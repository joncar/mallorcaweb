<?php if(empty($editor)): ?>
<script src="<?= base_url() ?>theme/theme/files/js/jquery-2.1.4.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.migrate/1.4.1/jquery-migrate.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/files/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/files/js/jquery.visible.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/files/revolution/js/jquery.themepunch.tools.min838f.js?rev=5.0"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/files/revolution/js/jquery.themepunch.revolution.min838f.js?rev=5.0"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/files/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/files/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/files/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/files/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/files/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/files/js/tweenMax.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/files/js/jquery.backgroundparallax.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/files/js/jquery.isotope.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/files/js/jquery.imagesloaded.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/files/js/jquery.owl.carousel.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/files/js/jquery.lightcase.min.js"></script>

<script type="text/javascript" src="<?= base_url() ?>theme/theme/files/js/jquery.min.bgvideo.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyPEFmiS1aSSx_fpoB5US78NBVI2pmRjc&libraries=places,geometry"></script>
<script type="text/javascript" src="<?= base_url() ?>js/gmap3.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script>
    

    $(".product-intro h2, .product-intro h3").on('click',function(){
        $(this).parent().find('p').toggle();
    });
</script>

<script>

    
    $(document).on('change',".table-cart .qty",function(){
        $.post('<?= base_url() ?>tt/addToCart/'+$(this).data('id')+'/'+$(this).val()+'/0/0',{},function(data){
            $.post('<?= base_url() ?>tt/getForm',{},function(data){
                $(".cartdetail").html(data);
            });
        });        
    });
    
    function cerrarCarro(){        
        $("#header-cart .header-cart-content").removeClass('showed');   
        $("#header-cart-mobile").removeClass('showed');
    }


    function enviarCarro(){
        var producto = $("input[name='producto']").val();
        var cantidad = $("input[name='cantidad']").val();
        addToCart(producto,cantidad);
        return false;
    }
    function addToCart(producto_id,cantidad){
        $.post('<?= base_url() ?>tt/addToCart/'+producto_id+'/'+cantidad,{},function(data){
            $(".menubar-cart").html(data);      
            $("#header-cart .header-cart-content").addClass('showed'); 
            $("#header-cart-mobile").addClass('showed');     
        });
    }
    function remToCart(producto_id){
        $.post('<?= base_url() ?>tt/delToCart/'+producto_id,{},function(data){
            $(".cartdetail").html(data);
        }); 
    }

    function remCart(producto_id){
        $.post('<?= base_url() ?>tt/delToCartNav/'+producto_id,{},function(data){
            $(".menubar-cart").html(data);
        });    
    }

    var captcha = '1';
    function contacto(f){
        $("#result").hide().html("Enviando solicitud").slideDown();

        if(captcha===''){
            $(".g-recaptcha").show();
            captcha = 1;
        }else{
            var post_data = new FormData(f);           
            //Ajax post data to server
            $("input[type='submit']").attr('disabled',true);
            $.ajax({
                url: URL+'paginas/frontend/contacto',
                data: post_data,
                context: document.body,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success:function(response){ 
                    response = JSON.parse(response);                                           
                    $("#result").hide().html(response.text).slideDown();
                    $("input[type='submit']").attr('disabled',false);
                }
            })
        }

        return false;
    }

    var captcha2 = '';
    function subscribir(f){
        $("#result2").show().html('Enviando solicitud').slideDown();
        if(captcha2===''){
            $(".g-recaptcha").show();
            captcha2 = 1;
        }else{
            var post_data = new FormData(f);           
            //Ajax post data to server
            $.ajax({
                url: URL+'paginas/frontend/subscribir',
                data: post_data,
                context: document.body,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success:function(response){ 
                    response = JSON.parse(response);                                           
                    $("#result2").hide().html(response.msg).slideDown();
                }
            })
        }

        return false;
    }
</script>




<script type="text/javascript" src="<?= base_url() ?>theme/theme/files/js/jquery.backgroundparallax.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/files/js/script.js"></script>
<?php else: ?>
	<script>
		$("a,button,label").on('click',function(e){
			e.preventDefault();
		});
	</script>
<?php endif ?>

<?php 
if(!empty($js_files)): ?>
<?php foreach($js_files as $file): ?>
<script src="<?= $file ?>"></script>
<?php endforeach; ?>  
<?php endif; ?>
<script src="<?= base_url() ?>js/frame.js?v=1.3"></script>
<script src="<?= base_url() ?>js/jquery.mask.min.js"></script>
<script>
    $('.mask').each(function(){
        $(this).mask($(this).data('format'),{placeholder: $(this).data('placeholder')});
    });    
</script>


<!---- Social Feed ---->
<!-- jQuery -->
<!-- Codebird.js - required for TWITTER -->
<script src="<?= base_url() ?>js/social/bower_components/codebird-js/codebird.js"></script>
<!-- doT.js for rendering templates -->
<script src="<?= base_url() ?>js/social/bower_components/doT/doT.min.js"></script>
<!-- Moment.js for showing "time ago" and/or "date"-->
<script src="<?= base_url() ?>js/social/bower_components/moment/min/moment.min.js"></script>
<!-- Moment Locale to format the date to your language (eg. italian lang)-->
<script src="<?= base_url() ?>js/social/bower_components/moment/locale/it.js"></script>
<!-- Social-feed js -->
<script src="<?= base_url() ?>js/social/js/jquery.socialfeed.js?v1.0"></script>
<script>
    $(document).ready(function(){
        $('.social-feed-container').socialfeed({            
            // INSTAGRAM
            instagram:{
                accounts: ['@mallorcaislandfestival'],  //Array: Specify a list of accounts from which to pull posts
                limit: 6,         
                type:'instagram',                         //Integer: max number of posts to load
                client_id: 'cb78ff6b321d47fb9f4ec59ffdf421f8',       //String: Instagram client id (option if using access token)
                access_token: '1736150833.cb78ff6.e7f17a31a5a34361997d3d7a81531583' //String: Instagram access token
            },

            facebook:{
                accounts: ['@mallorcaislandfestival'],  //Array: Specify a list of accounts from which to pull wall posts
                limit: 6,                                   //Integer: max number of posts to load
                access_token: 'EAAOL3DoVcQABAIh31PNOSsakf3b1lYKYP10TsB8RZBs6wsSdciiEwgdzxVk2tqqJ3bFiyZBa9JJBSzK0SwF7zmBBZC7FTrSHvWsgmPD0gK0RQNfqQd6eMJbYBHbcwyE4UiWtBhYnu2Jjrif2eeU1E6VqERYEb1VdROjzCKuRgZDZD'  //String: "APP_ID|APP_SECRET"
            },

            youtube:{
                accounts: ['UCIp1ihBnekNPNIbNQ_9PG4A'],  //Array: Specify a list of accounts from which to pull wall posts
                limit: 6,                                   //Integer: max number of posts to load
                avatar:'https://yt3.ggpht.com/-RhFrdqLtqTU/AAAAAAAAAAI/AAAAAAAAAAA/CO09m4_x86c/s288-mo-c-c0xffffffff-rj-k-no/photo.jpg',
                access_token: 'AIzaSyDEWIzmhAE16S6p9uALPu0I6IJtmXX5La8'  //String: "APP_ID|APP_SECRET"
            },
            
            // GENERAL SETTINGS
            length:400,                                     //Integer: For posts with text longer than this length, show an ellipsis.
            show_media:true,                                //Boolean: if false, doesn't display any post images
            media_min_width: 300,                           //Integer: Only get posts with images larger than this value
            update_period: 5000,                            //Integer: Number of seconds before social-feed will attempt to load new posts.
            template: "<?= base_url() ?>js/social/js/template.html",                         //String: Filename used to get the post template.            
            date_format: "ll",                              //String: Display format of the date attribute (see http://momentjs.com/docs/#/displaying/format/)
            date_locale: "en",                              //String: The locale of the date (see: http://momentjs.com/docs/#/i18n/changing-locale/)
            moderation: function(content) {                 //Function: if returns false, template will have class hidden
                return  (content.text) ? content.text.indexOf('fuck') == -1 : true;
            },
            callback: function() {                          //Function: This is a callback function which is evoked when all the posts are collected and displayed
                $(".isotope-grid").isotope('reloadItems');
                $(".isotope-grid").isotope('layout');
                setTimeout(function(){
                    $("#allSocial").trigger('click');
                    $("a[data-rel^=lightcase]").lightcase({showSequenceInfo:!1,swipe:!0,showCaption:!1,overlayOpacity:.95,maxWidth:1300,maxHeight:1100,shrinkFactor:1,video:{width:780,height:420}});
                },2000);
            }
        });
    });
</script>

<script>
    $("a[data-filter='.all']").parents('#page-body').find('.isotope-grid').isotope({
        filter:'.all'
    });

    //Mapa
    var gmMapDiv = $("#mapa");

    if (gmMapDiv.length) {        
        var lat = gmMapDiv.data('lat'),
            lng = gmMapDiv.data('lng'),
            icon = gmMapDiv.data('icon');            
        gmMapDiv.gmap3({
            action: "init",
            marker: {                    
                options: {
                    position: {lat: lat, lng: lng},
                    icon: icon
                }
            },
            map: {
                options: {
                    center: {lat: lat, lng: lng},
                    zoom: 17,
                    zoomControl: true,
                    zoomControlOptions: {
                        style: google.maps.ZoomControlStyle.SMALL
                    },
                    zoomControlOptions: {
                        position: google.maps.ControlPosition.LEFT_TOP
                    },
                    mapTypeControl: false,
                    scaleControl: false,
                    scrollwheel: false,
                    streetViewControl: false,
                    draggable: true,
                    styles: [{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#d3d3d3"}]},{"featureType":"transit","stylers":[{"color":"#808080"},{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#b3b3b3"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"weight":1.8}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"color":"#d7d7d7"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ebebeb"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"color":"#a7a7a7"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#efefef"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#696969"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"color":"#737373"}]},{"featureType":"poi","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#d6d6d6"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"color":"#dadada"}]}]
                }
            }
        });
    }

</script>

<div class="popup">
    <div>
        <a href="#" class="close"><i class="fa fa-remove"></i></a>
        ¿Quieres ganar <br/><br/> un viaje al <br/><br/>MIF 2019? <br/><br/>
        <a rel="canonical" class="sr-button button-4 circled button-icon-text button-medium" href="<?= base_url('concurso') ?>">
            Ir al concurso
        </a>
    </div>
    
</div>

<?php if($view=='main'): ?>
    <!--<script>
        $(document).on('ready',function(){
            setTimeout(function(){
                $(".popup").fadeIn(500);
            },10000);
        });

        $(".close").on('click',function(e){
            e.preventDefault();
            $(".popup").fadeOut(500);
        });
    </script>-->
<?php endif ?>
<script>
    $(".close2").on('click',function(e){
        e.preventDefault();
        $(".popup2").fadeOut(500);
        $("#formulariopagar").submit();
    }); 
</script>
<script>
    function validarcarrito(){
        if($(".politicas1").prop('checked') && $(".politicas2").prop('checked') && $(".politicas3").prop('checked')){
            $(".popup2").fadeIn(500);            
            $("#formulariopagar").removeAttr('onsubmit');
        }else{
            $("#result").html('<span class="colored">Debes aceptar nuestras políticas para realizar la compra</span>');
            
        }
        return false;
    }
</script>

<script>
    $(document).on('click','.minus',function(){
        var val = parseInt($(this).parent().find('.qty').val());
        val = val>1?(val-1):val;
        $(this).parent().find('.qty').val(val);
        $(this).parent().find('.qty').trigger('change');
    });

    $(document).on('click','.plus',function(){
        var val = parseInt($(this).parent().find('.qty').val());
        val = val+1;
        $(this).parent().find('.qty').val(val);
        $(this).parent().find('.qty').trigger('change');
    });
</script>