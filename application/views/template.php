<!doctype html>
<html lang="en">

	<!-- Google Web Fonts
	================================================== -->

	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i%7CFrank+Ruhl+Libre:300,400,500,700,900" rel="stylesheet">

	<!-- Basic Page Needs
	================================================== -->

	<title><?= empty($title) ? 'Monalco' : $title ?></title>
  	<meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
	<meta name="description" content="<?= empty($keywords) ?'': strip_tags($description) ?>" /> 
	<meta name="msvalidate.01" content="D261EA3DBCF2B4AC392FE13DF25E9F51" />	
	<link rel="apple-touch-icon" sizes="180x180" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" />
	<link rel="icon" type="image/png" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" sizes="192x192">
	<link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>	
	<link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>
	<link href="<?= base_url() ?>js/stocookie/stoCookie.css" rel="stylesheet">
	<link rel="canonical" href="<?= empty($url)?base_url():$url ?>">
	<script>var URL = '<?= base_url() ?>';</script>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    
    <!-- FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=cyrillic,cyrillic-ext,latin-ext,vietnamese" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet" type="text/css">
	<link href="//fonts.googleapis.com/css?family=Lora:400italic,700italic" rel="stylesheet" type="text/css">
       
    <!-- CSS -->

    <?php 
    if(!empty($css_files)):
    foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?= $file ?>" />
    <?php endforeach; ?>
    <?php endif; ?>
    <link rel="stylesheet" id="default-style-css"  href="<?= base_url() ?>theme/theme/files/css/style.css?v=1" type="text/css" media="all" />
    <link rel="stylesheet" id="fontawesome-style-css" href="<?= base_url() ?>theme/theme/files/css/font-awesome.min.css" type="text/css" media="all" />
    <link rel="stylesheet" id="ionic-icons-style-css" href="<?= base_url() ?>theme/theme/files/css/ionicons.css" type="text/css" media="all" />
    <link rel="stylesheet" id="revolution-slider-main-css" href="<?= base_url() ?>theme/theme/files/revolution/css/settings.css" type="text/css" media="all" />
    <link rel="stylesheet" id="revolution-slider-layer-css" href="<?= base_url() ?>theme/theme/files/revolution/css/layers.css" type="text/css" media="all" />
	<link rel="stylesheet" id="revolution-slider-nav-css" href="<?= base_url() ?>theme/theme/files/revolution/css/navigation.css" type="text/css" media="all" />
    <link rel="stylesheet" id="owlcarousel-css" href="<?= base_url() ?>theme/theme/files/css/owl.carousel.css" type="text/css" media="all" />
    <link rel="stylesheet" id="lightcase-css" href="<?= base_url() ?>theme/theme/files/css/lightcase.css" type="text/css" media="all" />
	<link rel="stylesheet" id="isotope-style-css"  href="<?= base_url() ?>theme/theme/files/css/isotope.css" type="text/css" media="all" />
    <link rel="stylesheet" id="mqueries-style-css"  href="<?= base_url() ?>theme/theme/files/css/mqueries.css" type="text/css" media="all" />	  
	<!-- Social-feed css -->
	<link href="<?= base_url() ?>css/social.css" rel="stylesheet" type="text/css">
	<!-- font-awesome for social network icons -->
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.7.3/css/font-awesome.css" rel="stylesheet">
	<link href="<?= base_url() ?>assets/grocery_crud/css/jquery_plugins/file_upload/jquery.fileupload-ui.css" type="text/css"  rel="stylesheet" />
	
</head>

<body>
	<a href="whatsapp://send?text=Hola+deseo+información+sobre+&phone=+34657941718" style="font-size:18px;padding:8px 8px;text-decoration:none;background-color:#189D0E;color:white;text-shadow:none;position:fixed;right:0;bottom:0px;z-index: 1000;"><i class="fa fa-whatsapp"></i> whatsapp</a>
	<?php if(empty($editor)): ?>
		<!-- PAGELOADER -->
		<div id="page-loader">
			<div class="page-loader-inner">
		     	<span class="loader-figure"></span>
		        <img class="loader-logo" title="Icono de loading" src="<?= base_url() ?>theme/theme/files/uploads/logo-mif.png" alt="Loader Logo" >
			</div>
		</div>
		<!-- PAGELOADER -->
	<?php endif ?>
	<div id="page-content">
    	<?php 
    		if(empty($editor)){
    			$this->load->view($view); 
    		}else{
    			echo $view;
    		}
		?>	
	</div>	
	<?php $this->load->view('includes/template/scripts') ?>

</body>
</html>