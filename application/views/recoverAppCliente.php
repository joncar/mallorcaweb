<body class="login" style="background-image:url(<?= base_url().'img/'.$this->db->get('ajustes')->row()->fondo ?>) !important; background-size: cover; background-attachment: fixed; background-repeat: no-repeat;">
    <!-- BEGIN LOGO -->
    <div class="logo">
        <a href="<?= base_url() ?>">
            <a href="<?= site_url() ?>" class="header-logo"><img src="<?= base_url().'img/'.$this->db->get('ajustes')->row()->logo ?>" style=" width: 500px" alt="" /></a>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN LOGIN -->
    <div class="content">
        <form action="registro/recuperar_app_cliente" method="post" onsubmit="return sendForm(this,'#Result')" role="form" class="form-horizontal">		    
		    <input type="email" name="email" id="email" data-val="required" class="form-control" value="<?= $user->email ?>" readonly><br/>
		    <input type="password" class="form-control" name="pass" id="pass" placeholder="Nueva Contraseña"><br/>
		    <input type="password" class="form-control" name="pass2" id="pass2" placeholder="Repetir Contraseña"><br/>		    
		    <button type="submit" class="btn btn-success">Recuperar Contraseña</button>
            <div id="Result"></div>
		</form>
    </div>
    <div class="copyright"> <?= date("Y") ?> © EVA Software. </div>
    <script>
        var URL = '<?= base_url() ?>';
    </script>
    <script src="<?= base_url() ?>js/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>js/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>js/additional-methods.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>js/select2.full.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>js/app.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>js/login.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>js/frame.js" type="text/javascript"></script>
</body>