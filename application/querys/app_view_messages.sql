DROP VIEW IF EXISTS app_view_messages;
CREATE VIEW app_view_messages AS SELECT 
app_consejos.id as app_consejos_id,
app_consejos.descripcion as descr, 
app_consejos.fecha_envio as fecha_tramite, 
DATE_FORMAT(app_consejos.fecha_envio,'%d/%m/%Y %H:%i') AS fecha_envio,
app_consejos.duracion,
app_push.app_usuarios_id,
app_push.fecha
FROM app_push
INNER JOIN app_consejos ON app_consejos.id = app_push.app_consejos_id
WHERE 
(SEC_TO_TIME(TIME_TO_SEC(app_push.fecha)+(app_consejos.duracion*3600))>TIME(NOW()) OR app_consejos.duracion IS NULL)
UNION 
SELECT 
app_push_programacion.id,
app_actividades.nombre,
app_programacion.notificar,
DATE_FORMAT(app_programacion.fecha,'%d/%m/%Y %H:%i'),
app_programacion.duracion,
app_push_programacion.app_usuarios_id,
app_programacion.fecha
FROM `app_push_programacion` 
INNER JOIN app_programacion ON app_programacion.id = app_push_programacion.app_programacion_id
INNER JOIN app_actividades ON app_actividades.id = app_programacion.app_actividades_id
INNER JOIN app_fechas_salida ON app_programacion.app_fechas_salida_id = app_fechas_salida.id
WHERE DATE(app_programacion.fecha) = DATE(NOW())