DROP VIEW IF EXISTS view_ventas;
CREATE VIEW view_ventas AS
SELECT 
UPPER(user.referencia_grupo) AS 'CODIGO GRUPO',
limpiar_acentos(UPPER(user.nombre)) AS Nombre,
limpiar_acentos(UPPER(user.apellido_paterno)) AS '1º APELLIDO',
limpiar_acentos(UPPER(user.apellido_2)) AS '2º APELLIDO',
UPPER(user.email) AS 'E-MAIL',
UPPER(user.telefono) AS 'TELEFONO',
UPPER(user.telefono_padre) AS 'TELÉFONO PADRES',
DATE_FORMAT(user.fecha_nacimiento,'%d/%m/%Y') AS 'F. Nacimiento',
UPPER(user.dni) AS 'DNI',
UPPER(user.Instituto) AS 'INSTITUTO',
UPPER(ventas.productos) AS 'PRODUCTOS',
FORMAT(ventas.precio,2,'de_DE') AS 'Importe de venta',
ventas.cantidad,
DATE_FORMAT(ventas.fecha_compra,'%d/%m/%Y %h:%i') AS 'F. COMPRA',
ventas.procesado,
user.id as nro_usuario,
ventas.id as nro_compra,
ventas.fecha_compra
FROM ventas
INNER JOIN user on user.id = ventas.user_id
ORDER BY ventas.id DESC