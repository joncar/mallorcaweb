<?php 
class Elements_app extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function app_programacion($where = array(),$limit = 0,$select = 0){
		if(!empty($limit)){
			$this->db->limit($limit);
		}
		if(empty($select)){
			$this->db->select('
				app_actividades.*,
				app_programacion.duracion,
				app_programacion.id as proid,
				app_programacion.fecha as _fecha,
				DATE(app_programacion.fecha) as fecha,
				TIME(app_programacion.fecha) as hora,
				app_programacion.app_lineup_id,
				app_programacion.destacado,
				app_programacion.color_pulsera
            ');
		}
		$this->db->order_by('app_programacion.fecha','ASC');        
        $this->db->join('app_actividades','app_actividades.id = app_programacion.app_actividades_id');
        $data = $this->db->get_where('app_programacion',$where);
        foreach($data->result() as $n=>$f){
            $data->row($n)->foto = base_url('img/app/'.$f->foto);
            $data->row($n)->fecha_format = date("d/m/Y H:i",strtotime($f->_fecha));
            $data->row($n)->miniatura = base_url('img/app/'.$f->miniatura);
            $data->row($n)->foto_horizontal = base_url('img/app/'.$f->foto_horizontal);
            $data->row($n)->hora = date('H:i',strtotime($f->_fecha));
            $data->row($n)->hasta = date('H:i',strtotime($f->_fecha.' +'.$f->duracion.' hours'));
            $data->row($n)->lugar = explode(',',str_replace(array(' ','(',')'),array('','',''),$f->lugar));
            $data->row($n)->mapa = base_url('img/app/'.$f->mapa);            
            if(!empty($_POST['user'])){
                if(date('d',strtotime($f->_fecha)) == date("d")){
                    $data->row($n)->recordatorio = $this->db->get_where('app_push_programacion',array('app_usuarios_id'=>$_POST['user'],'app_programacion_id'=>$f->proid))->row(); 
                }
            	$data->row($n)->avisame = $this->db->get_where('app_programacion_no_notif',array('app_usuarios_id'=>$_POST['user'],'app_programacion_id'=>$f->proid))->num_rows()>0?0:1;            
        	}

            if(!empty($f->app_lineup_id)){
                $dj = $this->db->get_where('app_lineup',array('id'=>$f->app_lineup_id));
                if($dj->num_rows()>0){
                    $data->row($n)->dj = $dj->row();
                }
            }

            if(!empty($f->fotos)){
                $fotos = explode(',',$f->fotos);
                $v = array();
                foreach($fotos as $vv){
                    if(!empty($vv)){
                        $v[] = base_url('img/app/'.$vv);
                    }
                }
                $data->row($n)->fotos = $v;
            }
        }
        return $data;
	}

	function app_lineup($where = array(),$limit = 0,$select = 0){
		if(!empty($limit)){
			$this->db->limit($limit);
		}
		if(empty($select)){
			$this->db->select('app_lineup.*');
		}		
        $data = $this->db->get_where('app_lineup',$where);
        foreach($data->result() as $n=>$f){
            $data->row($n)->foto = base_url('img/app/'.$f->foto);
            $fotos = explode(',',$f->fotos);
            $v = array();
            foreach($fotos as $vv){
                if(!empty($vv)){
                    $v[] = base_url('img/app/'.$vv);
                }
            }
            $data->row($n)->fotos = $v;
            $data->row($n)->miniatura = base_url('img/app/'.$f->miniatura);

            //Programas
            $data->row($n)->programas = array();
            $this->db->select('app_programacion.*,app_actividades.id as act,app_actividades.nombre as actividad, app_actividades.miniatura as fotoActividad');
            $this->db->join('app_actividades','app_actividades.id = app_programacion.app_actividades_id');
            $this->db->where('app_lineup_id',$f->id);
            $this->db->group_by('fecha');
            foreach($this->db->get_where('app_programacion')->result() as $p){
                $fecha = date("d/m/Y",strtotime($p->fecha));
                $desde = date("H:i",strtotime($p->fecha));
                $hasta = date('H:i',strtotime($p->fecha.' +'.$p->duracion.' hours'));
                $miniaturaActividad = base_url('img/app/'.$p->fotoActividad);
                $actividad = $this->app_actividad(array('id'=>$p->act))->row();                
                $pro = array('miniatura'=>$miniaturaActividad,'fecha'=>$fecha,'desde'=>$desde,'hasta'=>$hasta,'titulo'=>$p->actividad,'actividad'=>$actividad);
                $data->row($n)->programas[] = $pro;
            }
        }
        return $data;
	}

    function app_actividad($where = array()){
       $multimedia = $this->db->get_where('app_actividades',$where);
       foreach($multimedia->result() as $n=>$m){
            $multimedia->row($n)->foto = base_url('img/app/'.$m->foto);
            $multimedia->row($n)->miniatura = base_url('img/app/'.$m->miniatura);
            $multimedia->row($n)->foto_horizontal = base_url('img/app/'.$m->foto_horizontal);                
            $multimedia->row($n)->mapa = base_url('img/app/'.$m->mapa);                
            $multimedia->row($n)->lugar = explode(',',str_replace(array(' ','(',')'),array('','',''),$m->lugar));
            if(!empty($m->fotos)){
                $fotos = explode(',',$m->fotos);
                $v = array();
                foreach($fotos as $vv){
                    if(!empty($vv)){
                        $v[] = base_url('img/app/'.$vv);
                    }
                }
                $multimedia->row($n)->fotos = $v;
            }
       }
       return $multimedia;
    }

    function array_search_by_value($array,$field,$value){
        foreach($array as $n=>$a){
            if($a[$field]==$value){
                return true;
            }
        }
        return false;
    }

	function redes($limit = 0){
        //$this->db->delete('app_redes',array('fecha'=>date("Y-m-d")));
		$redes = $this->db->get_where('app_redes',array('fecha'=>date("Y-m-d")));
        if($redes->num_rows()==0/* || 1==1*/){
            $redes = array();
            //Instagram
            if(empty($_SESSION['instagram'])){
                $_SESSION['instagram'] = file_get_contents('https://api.instagram.com/v1/users/self/media/recent?access_token=1736150833.cb78ff6.e7f17a31a5a34361997d3d7a81531583');
            }
            $instagram = $_SESSION['instagram'];            
            $instagram = json_decode($instagram);
            $instagram = $instagram->data;
            foreach($instagram as $d){
                if(!empty($d->caption->text) && !$this->array_search_by_value($redes,'texto',$d->caption->text)){
                    $r = array();
                    $r['id'] = $d->id;
                    $r['texto'] = $d->caption->text;
                    $r['foto'] = $d->images->thumbnail->url;
                    $r['link'] = $d->link;
                    $r['tipo'] = 'instagram';
                    $r['fecha'] = date("d-m-Y H:i",(int)($d->created_time));
                    $r['_fecha'] = $d->created_time;
                    $r['color'] = 'bg-yellow-dark';
                    $redes[] = $r;
                }
            }
            //Facebook
            if(empty($_SESSION['facebook'])){
                $_SESSION['facebook'] = file_get_contents('https://graph.facebook.com/v3.0/700011170131401/posts?fields=id,from,name,message,created_time,story,description,link,picture,object_id&limit=6&access_token=EAAOL3DoVcQABAIh31PNOSsakf3b1lYKYP10TsB8RZBs6wsSdciiEwgdzxVk2tqqJ3bFiyZBa9JJBSzK0SwF7zmBBZC7FTrSHvWsgmPD0gK0RQNfqQd6eMJbYBHbcwyE4UiWtBhYnu2Jjrif2eeU1E6VqERYEb1VdROjzCKuRgZDZD&_=1553516317395');
            }
            $facebook = $_SESSION['facebook'];            
            $facebook = json_decode($facebook);            
            $facebook = $facebook->data;
            foreach($facebook as $d){
                if(!empty($d->message) && !$this->array_search_by_value($redes,'texto',$d->message)){
                    $r = array();
                    $r['id'] = $d->id;
                    $r['texto'] = $d->message;
                    $r['foto'] = $d->picture;
                    $r['link'] = $d->link;
                    $r['tipo'] = 'facebook';
                    $r['fecha'] = date("d-m-Y H:i",strtotime($d->created_time));
                    $r['_fecha'] = strtotime($d->created_time);
                    $r['color'] = 'bg-blue-dark';
                    $redes[] = $r;
                }
            }
            if(empty($_SESSION['youtube'])){
                $_SESSION['youtube'] = file_get_contents('https://www.googleapis.com/youtube/v3/search?part=snippet,id&order=date&maxResults=6&key=AIzaSyDEWIzmhAE16S6p9uALPu0I6IJtmXX5La8&channelId=UCIp1ihBnekNPNIbNQ_9PG4A');
            }
            $youtube = $_SESSION['youtube'];                        
            $youtube = json_decode($youtube);
            $youtube = $youtube->items;            
            foreach($youtube as $d){
                if(!empty($d->snippet->description) && !$this->array_search_by_value($redes,'texto',$d->snippet->description)){
                    $r = array();
                    $r['id'] = $d->id->videoId;
                    $r['texto'] = $d->snippet->description;
                    $r['foto'] = $d->snippet->thumbnails->default->url;
                    $r['link'] = 'https://www.youtube.com/watch?v='.$r['id'];
                    $r['tipo'] = 'youtube';
                    $r['fecha'] = date("d-m-Y H:i",strtotime($d->snippet->publishedAt));
                    $r['_fecha'] = strtotime($d->snippet->publishedAt);
                    $r['color'] = 'bg-red-dark';
                    $redes[] = $r;
                }
            }            
            $redes = msort($redes, '_fecha','DESC');            
            $this->db->insert('app_redes',array('text'=>json_encode($redes),'fecha'=>date("Y-m-d")));
        }else{
            $redes = json_decode($redes->row()->text);
        }

        if(!empty($limit)){
            $x = 0;
            $ar = array();
            foreach($redes as $r){
                if($x<$limit){
                    $x++;
                    $ar[] = $r;
                }
            }
            $redes = $ar;
        }

        return $redes;
	}

    function app_multimedia($where = array()){
       $multimedia = $this->db->get_where('app_multimedia',$where);
       foreach($multimedia->result() as $n=>$m){
        $multimedia->row($n)->portada = base_url('img/app/'.$m->portada);
        $multimedia->row($n)->fotos = $this->app_multimedia_files(array('app_multimedia_id'=>$m->id));
       }
       return $multimedia;
    }

    function app_multimedia_files($where = array()){
       $multimedia = $this->db->get_where('app_multimedia_files',$where);
       foreach($multimedia->result() as $n=>$m){
        $multimedia->row($n)->foto = $m->tipo==1?base_url('img/app/'.$m->foto):'';
        $multimedia->row($n)->video = $m->tipo==2?base_url('img/app/'.$m->video):'';
       }
       return $multimedia;
    }

    function app_consejos($where = array()){
       $this->db->join('app_consejos_salidas','app_consejos_salidas.app_consejos_id = app_consejos.id');
       $this->db->join('app_usuarios','app_usuarios.app_fechas_salida_id = app_consejos_salidas.app_fechas_salida_id');
       $consejos = $this->db->get_where('app_consejos',$where);       
       foreach($consejos->result() as $n=>$m){
        
       }
       return $consejos;
    }

    function app_push($where = array()){
        if(!empty($where)){
            $this->db->select("app_consejos.id as app_consejos_id,app_consejos.descripcion as descr, app_consejos.fecha_envio as fecha_tramite, DATE_FORMAT(app_consejos.fecha_envio,'%d/%m/%Y %H:%i') AS fecha_envio,app_consejos.duracion,app_push.app_usuarios_id,app_push.fecha");           $this->db->join('app_consejos','app_consejos.id = app_push.app_consejos_id');
            $this->db->where('app_usuarios_id = '.$where['user'],NULL,FALSE);
            $this->db->order_by('fecha','DESC');
            $consejos = $this->db->get_where('app_push');
            return $consejos;
        }else{
            return $this->db->get_where('app_push',array('id'=>-1));
        }
    }

    function app_push_programacion($where = array()){
        if(!empty($where)){    
            $this->db->select('app_push_programacion.*');
            $this->db->join('app_programacion','app_programacion.id = app_push_programacion.app_programacion_id');
            $this->db->where('app_usuarios_id = '.$where['user'],NULL,FALSE);            
            $this->db->where('DATE(app_programacion.fecha) = \''.date("Y-m-d").'\'',NULL,FALSE);            
            $consejos = $this->db->get_where('app_push_programacion');
            return $consejos;
        }else{
            return $this->db->get_where('app_push_programacion',array('id'=>-1));
        }
    }

    function app_influencers($where = array()){
       $this->db->order_by('orden','ASC');
       $multimedia = $this->db->get_where('app_influencers',$where);
       foreach($multimedia->result() as $n=>$m){
        $multimedia->row($n)->foto = base_url('img/app/'.$m->foto);
        $multimedia->row($n)->miniatura = base_url('img/app/'.$m->miniatura);
        $val = explode(',',$m->fotos);
        $v = array();
        foreach($val as $vv){
            if(!empty($vv)){
                $v[] = base_url('img/app/'.$vv);
            }
        }        
        $multimedia->row($n)->fotos = $v;
       }
       return $multimedia;
    }


    protected $apiKeyOneSignal = 'ZTM2ZDJhODQtMTU5MS00NmZlLTk0OTEtZDE1MzEwY2M3NTM1';
    protected $appIdOneSignal = 'bab567ad-c17c-49ae-a967-5e49be8aff2c';

    function sendPush($registrationIdsArray,$messageData){
        $headers = array("Content-Type:" . "application/json", "Authorization:" . "Basic " . $this->apiKeyOneSignal);
        $data = array(
        'app_id' => $this->appIdOneSignal,
        'headings'=>array('en'=>'Mensaje de recordatorio','es'=>'Mensaje de recordatorio'),
        'data'=>array('param1'=>'parametro'),        
        'contents' => array('en'=>$messageData['message'],'es'=>$messageData['message']),
        'android_background_layout'=>array("image"=>"https://domain.com/background_image.jpg", "headings_color"=>"FFFF0000", "contents_color"=>"FF00FF00"),
        'include_player_ids'=>$registrationIdsArray,
        'ios_badgeType'=>'Increase',
        'ios_badgeCount'=>'1',
        'large_icon'=>base_url('img/notifIcon.png')
        );

        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications" );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($data) );
        $response = curl_exec($ch);
        curl_close($ch);
        print_r($response);
        return $response;
    }
}