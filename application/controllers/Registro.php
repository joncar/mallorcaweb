<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('Main.php');
class Registro extends Main {
        const IDROLUSER = 2;
        public function __construct()
        {
            parent::__construct();         
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
        }
        
        public function loadView($param = array('view'=>'main'))
        {
                if(!empty($param->output)){
                    $panel = 'panel';
                    $param->view = empty($param->view)?$panel:$param->view;
                    $param->crud = empty($param->crud)?'user':$param->crud;
                    $param->title = empty($param->title)?ucfirst($this->router->fetch_method()):$param->title;
                }
                if(is_string($param)){
                    $param = array('view'=>$param);
                }
                $template = 'templateadmin';
                $this->load->view($template,$param);
        }

        public function index($url = 'main',$page = 0)
        {
            if($this->router->fetch_class()=='registro'){
                $crud = new ajax_grocery_CRUD();
                $crud->set_theme('bootstrap2');
                $crud->set_table('user');
                $crud->set_subject('<span style="font-size:18px">Nuevo usuario</span>');
                //Fields
                $crud->required_fields_array();
                //unsets
                $crud->unset_back_to_list()
                     ->unset_delete()
                     ->unset_read()
                     ->unset_edit()
                     ->unset_list()
                     ->unset_export()
                     ->unset_print()
                     ->field_type('fecha_registro','invisible')
                     ->field_type('fecha_actualizacion','invisible')
                     ->field_type('status','invisible')
                     ->field_type('foto','invisible')
                     ->field_type('admin','invisible')
                     ->field_type('password','password')
                     ->field_type('cedula','invisible')
                     ->field_type('apellido_materno','invisible')
                     ->field_type('tiene_android','hidden',1)
                     ->field_type('registrado_desde','invisible');

                if($crud->getParameters(false)=='add'){                     
                    $crud->field_type('fecha_nacimiento','mask',array('mask'=>'00/00/0000'));
                    $crud->field_type('fecha_caducidad','mask',array('mask'=>'00/00/0000'));
                }

                $crud->display_as('password','Contraseña nuevo usuario')
                     ->display_as('apellido_paterno','1º Apellido')
                     ->display_as('apellido_2','2º Apellido')
                     ->display_as('nombre','Nombre del pasajero')
                     ->display_as('email','Email de contacto')
                     ->display_as('provincias_id','Provincia')
                     ->display_as('referencia_grupo','Código del grupo')
                     ->display_as('telefono','Teléfono')
                     ->display_as('codigo_postal','Código Postal')
                     ->display_as('direccion','Dirección')
                     ->display_as('fecha_caducidad','Fecha de caducidad del DNI')
                     ->display_as('dni','DNI')
                     ->display_as('telefono','Teléfono Estudiante')
                     ->display_as('telefono_padre','Teléfono Padre/Madre/Tutor legal')
                     ->display_as('dni','DNI')
                     ->set_relation('plan','planes','{nombre} {costo}');

                $redirect = empty($_SESSION['carrito'])?'panel':'store/checkout';
                $crud->set_lang_string('insert_success_message','Les seves dades han estat guardats amb èxit <script>setTimeout(function(){document.location.href="'.base_url($redirect).'"; },2000)</script>');                
                $crud->set_lang_string('form_add','');
                //$crud->required_fields('password','email','nombre','apellido','centro','nro_grupo','curso');
                $crud->callback_field('referencia_grupo',function(){
                    return form_input('referencia_grupo','','class="form-control" id="field-referencia_grupo" placeholder="Ej. M20001 (Nº Documento Reserva)"');
                })->callback_field('Instituto',function(){
                    return form_input('Instituto','','class="form-control" id="field-instituto"').
                           '<div style="margin-top:10px;"><b>Importante</b> Recordar la contraseña siempre que quieras entrar al sistema</div>';
                });
                //Displays             
                $crud->set_lang_string('form_save','REGISTRAR');

                $crud->callback_before_insert(array($this,'binsertion'));
                $crud->callback_after_insert(array($this,'ainsertion'));
                $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]');
                //$crud->required_fields('password','email','nombre','apellido_paterno','direccion','codigo_postal','telefono','ciudad','provincias_id','referencia_grupo','Instituto');
                $output = $crud->render();
                $output->view = 'registro';
                $output->crud = 'user';
                $output->title = 'REGISTRAR';
                $output->output = $output->output;
                $output->scripts = get_header_crud($output->css_files,$output->js_files,TRUE);
                $this->loadView($output);   
            }
        }              
        
        function conectar()
        {
            $this->loadView('predesign/login');
        }
        /* Callbacks */
        function binsertion($post)
        {            
            $post['status'] = 1;
            $post['admin'] = 0;
            $post['fecha_registro'] = date("Y-m-d H:i:s");
            get_instance()->contrasena = $post['password'];
            $post['password'] = md5($post['password']);
            $post['registrado_desde'] = 'WEB';
            return $post;
        }
        
        function ainsertion($post,$primary)
        {              
            //Asignar rol
            get_instance()->db->insert('user_group',array('user'=>$primary,'grupo'=>self::IDROLUSER));            
            get_instance()->user->login_short($primary);    
            $post['contrasena'] = get_instance()->contrasena;
            get_instance()->enviarcorreo((object)$post,1);
            return true;
        }
        
       
        function forget($key = '',$ajax = '')
        {
            if(empty($_POST) && empty($key)){
                $this->loadView(array('view'=>'forget'));
            }
            else
            {                
                $_SESSION['email'] = empty($_SESSION['email']) && !empty($ajax)?base64_decode($ajax):$_SESSION['email'];
                $_SESSION['key'] = empty($_SESSION['key']) && !empty($ajax)?base64_decode($ajax):$_SESSION['key'];
                if(empty($key)){
                    if(empty($_SESSION['key'])){
                        $this->form_validation->set_rules('email','Email','required|valid_email');
                        if($this->form_validation->run())
                        {
                            $user = $this->db->get_where('user',array('email'=>$this->input->post('email')));
                            if($user->num_rows()>0){
                                $_SESSION['key'] = md5($this->input->post('email'));
                                $_SESSION['email'] = $this->input->post('email');
                                if($_SESSION['lang']=='es'){
                                    correo($this->input->post('email'),'Restablecimiento de contraseña',$this->load->view('email/forget',array('user'=>$user->row()),TRUE));
                                }else{
                                    correo($this->input->post('email'),'Recuperación de contraseña',$this->load->view('email/forget_en',array('user'=>$user->row()),TRUE));
                                }
                                $_SESSION['msj'] = $this->success('Se han enviado los pasos de restauración a su correo');
                                header("Location:".base_url('registro/index/add'));
                            }
                            else{
                                $this->loadView(array('view'=>'registro','msj'=>$this->error('El correo utilizado no esta registrado')));
                            }
                        }
                        else{
                            $this->loadView(array('view'=>'registro','msj'=>$this->error($this->form_validation->error_string())));
                        }
                    }
                    else
                    {
                        $this->form_validation->set_rules('email','Email','required|valid_email');
                        $this->form_validation->set_rules('pass','Password','required|min_length[8]');
                        $this->form_validation->set_rules('pass2','Password2','required|min_length[8]|matches[pass]');
                        //$this->form_validation->set_rules('key','Llave','required');
                        if($this->form_validation->run())
                        {
                            /*if($this->input->post('key') == $_SESSION['key'])
                            {*/
                                $this->db->update('user',array('password'=>md5($this->input->post('pass'))),array('email'=>$_SESSION['email']));
                                session_unset();
                                $this->loadView(array('view'=>'registro','msj'=>$this->success('Se ha restablecido su contraseña <a href="'.base_url().'">Volver al inicio</a>')));
                            /*}
                            else
                                $this->loadView(array('view'=>'recover','msj'=>$this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.')));*/
                        }
                        else{
                            if(empty($_POST['key'])){
                                $this->loadView(array('view'=>'registro','msj'=>$this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.')));    
                                session_unset();
                            }
                            else{
                                $this->loadView(array('view'=>'recover','key'=>$key,'msj'=>$this->error($this->form_validation->error_string())));
                            }
                        }
                    }
                }
                else
                {
                    $this->loadView(array('view'=>'recover','key'=>$key));
                    /*if(!empty($_SESSION['key']) && $key==$_SESSION['key'])
                    {
                        $this->loadView(array('view'=>'recover','key'=>$key));
                    }
                    else{
                        $enc = $this->db->get_where('user',array('MD5(email)'=>$key));
                        
                        if($enc->num_rows()==0){
                            if(empty($ajax)){
                                $this->loadView(array('view'=>'registro','msj'=>$this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.')));
                            }else{
                                echo $this->traduccion->traducir($this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.'),$_SESSION['lang']);
                            }
                        }else{
                            $_SESISON['key'] = md5($enc->row()->email);
                            $_SESSION['email'] = $enc->row()->email;
                            $this->loadView(array('view'=>'recover','key'=>$key));
                        }
                    }*/
                }
            }
        }        

        function recuperar_app($email = ''){
            if(!empty($email) && empty($_POST)){
                $email = base64_decode($email);
                $usuario = $this->db->get_where('app_usuarios',array('email'=>$email));
                if($usuario->num_rows()>0){
                    $this->loadView(array('view'=>'recoverApp','user'=>$usuario->row()));
                }else{
                    throw new exception('Error al consultar información',503);
                }
            }elseif(!empty($_POST)){
                $this->form_validation->set_rules('email','Email','required|valid_email')
                                      ->set_rules('pass','Contraseña','required')
                                      ->set_rules('pass2','Repetir contraseña','required|matches[pass]');
                if($this->form_validation->run()){
                    $this->db->update('app_usuarios',array('password'=>md5($_POST['pass'])),array('email'=>$_POST['email']));
                    echo $this->success('Contraseña cambiada con éxito');
                }else{
                    echo $this->error($this->form_validation->error_string());
                }
            }
        }

        function recuperar_app_cliente($email = ''){
            if(!empty($email) && empty($_POST)){
                $email = base64_decode($email);
                $usuario = $this->db->get_where('user',array('email'=>$email));
                if($usuario->num_rows()>0){
                    $this->loadView(array('view'=>'recoverAppCliente','user'=>$usuario->row()));
                }else{
                    throw new exception('Error al consultar información',503);
                }
            }elseif(!empty($_POST)){
                $this->form_validation->set_rules('email','Email','required|valid_email')
                                      ->set_rules('pass','Contraseña','required')
                                      ->set_rules('pass2','Repetir contraseña','required|matches[pass]');
                if($this->form_validation->run()){
                    $this->db->update('user',array('password'=>md5($_POST['pass'])),array('email'=>$_POST['email']));
                    echo $this->success('Contraseña cambiada con éxito');
                }else{
                    echo $this->error($this->form_validation->error_string());
                }
            }
        }
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
