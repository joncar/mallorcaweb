<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Tienda extends Panel{
        function __construct() {
            parent::__construct();
        } 
        
       	function categorias(){
       		$crud = $this->crud_function('','');
       		$crud->set_field_upload('foto','img/fotos_categorias');
          $crud->set_field_upload('icono','img/fotos_categorias');
          $crud->add_action('reembolsos','',base_url('t/reembolsos').'/');
          $crud->columns('nombre_categoria','subtitulo','foto','sistema_reembolso','disponible');
          $crud->field_type('condiciones_reembolso','string');
       		$crud = $crud->render();
       		$this->loadView($crud);
       	}

        function reembolsos($x = ''){
          $crud = $this->crud_function('','');
          $crud->where('categorias_id',$x)
               ->field_type('categorias_id','hidden',$x);
          $crud = $crud->render();
          $this->loadView($crud);
        }

       	function productos(){
       		$crud = $this->crud_function('','');
       		//$crud->set_field_upload('foto','img/fotos_productos');
          $crud->field_type('foto','image',array('path'=>'img/fotos_productos','width'=>'600px','height'=>'400px'));
          $crud->add_action('<i class="fa fa-image"></i> Adm.fotos','',base_url('tienda/productos_fotos').'/');
       		$crud = $crud->render();
       		$this->loadView($crud);
       	}

        function productos_fotos(){
          $this->load->library('image_crud');
          $crud = new image_crud();
          $crud->set_table('productos_fotos')
               ->set_image_path('img/fotos_productos')
               ->set_url_field('foto')
               ->set_relation_field('productos_id')
               ->set_ordering_field('orden');
          $crud->module = 'tienda';
          $crud = $crud->render();
          $this->loadView($crud);
        }

       	function ventas($action = '',$x = '',$y = ''){
          if($action=='print' || $action=='export'){
            $this->as['ventas'] = 'view_ventas';
          }
       		$crud = $this->crud_function('','');  
          $crud->callback_column('se8701ad4',function($val,$row){
              return '<a href="'.base_url('t/ventas/'.$row->user_id).'">'.$val.'</a>';
          });
          if(is_numeric($x)){
              $crud->where('user_id',$x);
          }
          $crud->callback_column('procesado',function($val){
              switch($val){
                  case '-1': return '<span class="label label-danger">No procesado</span>'; break;
                  case '1': return '<span class="label label-default">Por procesar</span>'; break;
                  case '2': return '<span class="label label-success">Procesado</span>'; break;
              }
          });
          $crud->callback_after_delete(function($primary){
              get_instance()->db->delete('ventas_detalles',array('ventas_id'=>$primary));
          });
          if($crud->getParameters()=='list'){
            $crud->columns('id','user_id','productos','precio','cantidad','fecha_compra','procesado');
          }
          if(!empty($_POST['desde']) && !empty($_POST['hasta'])){
              $desde = date("Y-m-d",strtotime(str_replace('/','-',$_POST['desde'])));
              $hasta = date("Y-m-d",strtotime(str_replace('/','-',$_POST['hasta'])));
              $crud->where('DATE(fecha_compra) >=',$desde);
              $crud->where('DATE(fecha_compra) <=',$hasta);
          }
          if($action=='export' && !empty($x) && !empty($y)){
            $desde = date("Y-m-d",strtotime(str_replace('/','-',$x)));
            $hasta = date("Y-m-d",strtotime(str_replace('/','-',$y)));
            $crud->where('DATE(fecha_compra) >=',$desde);
            $crud->where('DATE(fecha_compra) <=',$hasta);
            $crud->unset_columns('fecha_compra');
          }
          $crud->display_as('id','#Compra');
          $crud->field_type('procesado','dropdown',array(-1=>'No Procesado',1=>'Por procesar',2=>'Procesado'));                  
       		$crud = $crud->render();
          $crud->output = $this->load->view('_ventas_crud',array('output'=>$crud->output),TRUE);
       		$this->loadView($crud);
       	}
    }
?>
