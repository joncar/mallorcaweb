<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{        
        function __construct() {
            parent::__construct();
            $this->load->model('tienda_model');
            $this->load->library('form_validation');
        } 
        
        function tienda(){
            $this->loadView(array('view'=>'tienda','title'=>'Reserva','url'=>base_url('store')));
        }

        function listado($categoria){
            $ur = $categoria;
            $id = explode('-',$categoria);
            $id = $id[0];
            if(is_numeric($id)){
                $reserva = $this->db->get_where('categorias',array('id'=>$id));
                if($reserva->num_rows()>0){
                    $cat = 'Reserva en '.strtolower(strip_tags($reserva->row()->nombre_categoria));
                    $this->loadView(array('view'=>'listado','categoria'=>$categoria,'title'=>$cat,'url'=>base_url('store/'.$ur)));
                }else{
                    redirect('store');
                }
            }else{
                redirect('store');
            }
        }

        function listado_test($categoria){
            $ur = $categoria;
            $id = explode('-',$categoria);
            $id = $id[0];
            if(is_numeric($id)){
                $reserva = $this->db->get_where('categorias',array('id'=>$id));
                if($reserva->num_rows()>0){
                    $cat = 'Reserva en '.strtolower(strip_tags($reserva->row()->nombre_categoria));
                    $this->loadView(array('view'=>'listado_test','categoria'=>$categoria,'title'=>$cat,'url'=>base_url('store/'.$ur)));
                }else{
                    redirect('store');
                }
            }else{
                redirect('store');
            }
        }

        function producto($producto){
            $ur = $producto;
            $id = explode('-',$producto);
            $id = $id[0];
            if(is_numeric($id)){
                $reserva = $this->db->get_where('productos',array('id'=>$id));
                if($reserva->num_rows()>0){
                    $categoria = $this->db->get_where('categorias',array('id'=>$reserva->row()->categorias_id));
                    $categoria = strtolower(strip_tags($categoria->row()->nombre_categoria));
                    $cat = 'Reserva en '.$categoria.' '.strip_tags($reserva->row()->nombre_producto);
                    $this->loadView(array('view'=>'producto','producto'=>$producto,'title'=>$cat,'url'=>base_url('store/produto/'.$ur)));
                }else{
                    redirect('store');
                }                
                
            }else{
                redirect('store');
            }
        }


        function ver_carro(){
            $this->loadView('carrito');
        }

        function checkout(){
            $carrito = $this->carrito->getCarrito(); 
            if(count($carrito)==0){
                redirect('store/carrito');
            }else{                
                if($this->user->log){

                    //Guardamos en la bd
                    $this->tienda_model->cleanVentas();
                    $id = $this->tienda_model->saveVenta();
                    $this->pagar_articulo($id);
                }else{
                    $this->tienda_model->cleanVentas();
                    redirect('registro/index/add?redirect=store/checkout');
                }
            }

        }

        function pagar_articulo($id){
            $this->db->select('ventas_detalles.*, productos.categorias_id');
            $this->db->join('productos','productos.id = ventas_detalles.productos_id');
            $detalle = $this->db->get_where('ventas_detalles',array('ventas_id'=>$id));
            if($detalle->num_rows()>0){
                $total = 0;                     
                foreach($detalle->result() as $c){
                    /*$total+= ($c->cantidad*$c->precio)+($c->cantidad*3.50);*/
                    $total+= ($c->cantidad*$c->precio);
                }
                $total+=(3.50); 
                /*if($this->user->id==2){
                    $total = 0.01;
                } */                  
                $miObj = new RedsysAPI;
                // Valores de entrada                    
                if($detalle->row()->categorias_id=='1'){
                    $merchantCode   ="091995811";
                    $key = 'KBagCRmyFKbAAYqRu2FCfOOlpyezifpy';//Clave secreta del terminal
                }else{
                    $merchantCode   ="327977559";
                    $key = 'xqSeBn4Qp+EsCTtko9MwNEGqFnMoDj2d';
                }
                $terminal       ="1";
                $amount         =$total*100;
                $currency       ="978";
                $transactionType    ="0";
                $merchantURL    =base_url('tt/procesarPago');
                $urlOK      =base_url('store/pagoOk/'.$id);
                $urlKO      =base_url('store/pagoKo/'.$id);
                $order      = $id.'-'.date("s");
                $miObj->setParameter("DS_MERCHANT_AMOUNT",$amount);
                $miObj->setParameter("DS_MERCHANT_ORDER",strval($order));
                $miObj->setParameter("DS_MERCHANT_MERCHANTCODE",$merchantCode);
                $miObj->setParameter("DS_MERCHANT_CURRENCY",$currency);
                $miObj->setParameter("DS_MERCHANT_TRANSACTIONTYPE",$transactionType);
                $miObj->setParameter("DS_MERCHANT_TERMINAL",$terminal);
                $miObj->setParameter("DS_MERCHANT_MERCHANTURL",$merchantURL);
                $miObj->setParameter("DS_MERCHANT_URLOK",$urlOK);       
                $miObj->setParameter("DS_MERCHANT_URLKO",$urlKO);
                $version="HMAC_SHA256_V1";                    
                $request = "";
                $params = $miObj->createMerchantParameters();
                $signature = $miObj->createMerchantSignature($key);
                echo '<div style="text-align:center">Redireccionando al banco, por favor espere</div>';
                $this->load->view('checkout',array('version'=>$version,'params'=>$params,'signature'=>$signature));
                if(!empty($_SESSION['carrito'])){
                    unset($_SESSION['carrito']);
                }
            }else{
                redirect('carrito');
            }
        }

        

        public function refreshCartForm(){
            $this->load->view('includes/fragmentos/carritoForm');
        }
        
        public function carrito($venta_id = ''){
            $this->loadView('carrito');
        }

        public function addToCart($prod = '',$cantidad = '',$return = TRUE,$sumarcantidad = TRUE){
            if(!empty($prod) && is_numeric($prod) && $prod>0 && is_numeric($cantidad) && $cantidad>0){                
                $sumarcantidad = $sumarcantidad=='0'?FALSE:$sumarcantidad;
                $this->db->where('productos.id',$prod);
                $producto = $this->carrito->getProductos();
                if($producto->num_rows()>0){
                    $producto = $producto->row();
                    $producto->cantidad = $cantidad;
                    $this->carrito->setCarrito($producto,$sumarcantidad);
                }                
            }
            if($return){
                $this->load->view('_carrito');
            }
        }
        
        public function addToCartArray(){
            if(!empty($_POST)){
                $this->form_validation->set_rules('provincias_id','Provincia','required')
                                      ->set_rules('dia_entrega','Dia de entrega','required')
                                      ->set_rules('hora_entrega','Hora de entrega','required')
                                      ->set_rules('forma_pago','Forma de pago','required')
                                      ->set_rules('costo_envio','Costo de envio','required')
                                      ->set_rules('politicas','Costo de envio','required');
                if($this->form_validation->run()){
                    $response = 'success';
                    foreach($_POST['id'] as $n=>$v){
                        if(empty($_POST['gramaje'][$n]) && !empty($this->db->get_where('productos',array('id'=>$v))->row()->gramaje)){
                            $response = "Sis plau tria un gramatge";
                        }else{
                            $this->addToCart($v,$_POST['cantidad'][$n],$_POST['gramaje'][$n],FALSE,FALSE);
                        }
                    }
                    //Almacenar datos de envio
                    $_SESSION['envio']['provincias_id'] = $_POST['provincias_id'];
                    $_SESSION['envio']['dia_entrega'] = $_POST['dia_entrega'];
                    $_SESSION['envio']['hora_entrega'] = $_POST['hora_entrega'];
                    $_SESSION['envio']['forma_pago'] = $_POST['forma_pago'];
                    $_SESSION['envio']['observaciones'] = $_POST['observaciones'];
                    $_SESSION['envio']['costo_envio'] = $_POST['costo_envio'];
                    $total = 0;
                    foreach($_SESSION['carrito'] as $n=>$v){                        
                        $total+= ($v->precio*$v->cantidad);
                    }
                    $response = $total<45?'comanda minima de 45€':$response;
                    echo $response;
                }else{
                    echo "Sis plau completi totes les dades amb asterisc";
                }
            }else{
                echo "Sis plau completi totes les dades amb asterisc";
            }
        }
        
        public function delToCart($prod = ''){
            if(!empty($prod) && is_numeric($prod) && $prod>0){                
                $this->carrito->delCarrito($prod);                
            }
            $this->load->view('_carritoForm');
        }

        public function delToCartNav($prod = ''){
            if(!empty($prod) && is_numeric($prod) && $prod>0){                
                $this->carrito->delCarrito($prod);                
            }
            $this->load->view('_carrito');   
        }

        function getForm(){
            $this->load->view('_carritoForm');
        }

        function procesarPago(){             
            $this->load->library('redsysapi');
            $miObj = new RedsysAPI;

            //$this->tienda_model->pagoOk(47271);

            /*$_POST = array(
                'Ds_SignatureVersion' => 'HMAC_SHA256_V1',
                'Ds_Signature' => 'Vr71Chc4xojSd-ibNt3i_szcWsWAMMRL4yBBlUVRn2U=',
                'Ds_MerchantParameters' => 'eyJEc19EYXRlIjoiMDJcLzEwXC8yMDE4IiwiRHNfSG91ciI6IjExOjUzIiwiRHNfU2VjdXJlUGF5bWVudCI6IjEiLCJEc19DYXJkX1R5cGUiOiJDIiwiRHNfQ2FyZF9Db3VudHJ5IjoiNzI0IiwiRHNfQW1vdW50IjoiMSIsIkRzX0N1cnJlbmN5IjoiOTc4IiwiRHNfT3JkZXIiOiI0ODM2My0xMTUxNDEiLCJEc19NZXJjaGFudENvZGUiOiIzMjc5Nzc1NTkiLCJEc19UZXJtaW5hbCI6IjAwMSIsIkRzX1Jlc3BvbnNlIjoiMDAwMCIsIkRzX01lcmNoYW50RGF0YSI6IiIsIkRzX1RyYW5zYWN0aW9uVHlwZSI6IjAiLCJEc19Db25zdW1lckxhbmd1YWdlIjoiMSIsIkRzX0F1dGhvcmlzYXRpb25Db2RlIjoiOTA0MjMzIiwiRHNfQ2FyZF9CcmFuZCI6IjEifQ=='
            );*/
            if (!empty($_POST)){                
                $datos = $_POST["Ds_MerchantParameters"];
                $decodec = json_decode($miObj->decodeMerchantParameters($datos));                                
                //$decodec = $_POST["Ds_MerchantParameters"];  //Eliminar siempre que se pase a produccion                              
                if(!empty($decodec) && ($decodec->Ds_Response=='0000' || $decodec->Ds_Response=='0001' || $decodec->Ds_Response=='0002' || $decodec->Ds_Response=='0099') && empty($decodec->Ds_ErrorCode)){
                    //$id = substr($decodec->Ds_Order,3);    
                    $id = $decodec->Ds_Order;
                    $id = explode('-',$id)[0];                                    
                    $this->tienda_model->pagoOk($id,$decodec);
                }else{
                    //$id = substr($decodec->Ds_Order,3);  
                    $id = $decodec->Ds_Order;
                    $id = explode('-',$id)[0];                  
                    $this->tienda_model->pagoNoOk($id,$decodec);
                }
                
                
                //correo('joncar.c@gmail.com','POST',print_r($decodec,TRUE));
            }
        }

        function pagoOk($id = ''){
            $this->loadView(array('view'=>'pagoOk','title'=>'Resultado de compra'));
        }

        function pagoKo($id = ''){
            $this->loadView(array('view'=>'pagoKo','title'=>'Resultado de compra'));
        }

        function pagoOkMovil($id = ''){
            $this->load->view('pagoOkMovil');
        }

        function pagoKoMovil($id = ''){
            $this->load->view('pagoKoMovil');
        }

        function payInApp(){
            $this->load->view('pagoInApp');
        }


        /************** DESACTIVAR CUANDO ESTE EN PRODUCCIóN *****/

        function pagar_articulo_movil($id){
            $this->db->select('ventas_detalles.*, productos.categorias_id');
            $this->db->join('productos','productos.id = ventas_detalles.productos_id');
            $detalle = $this->db->get_where('ventas_detalles',array('ventas_id'=>$id));
            if($detalle->num_rows()>0){
                $total = 0;                     
                foreach($detalle->result() as $c){
                    /*$total+= ($c->cantidad*$c->precio)+($c->cantidad*3.50);*/
                    $total+= ($c->cantidad*$c->precio);
                }
                $total+=(3.50); 
                /*if($this->user->id==2){
                    $total = 0.01;
                } */                  
                $miObj = new RedsysAPI;
                // Valores de entrada                    
                if($detalle->row()->categorias_id=='1'){
                    $merchantCode   ="091995811";
                    $key = 'KBagCRmyFKbAAYqRu2FCfOOlpyezifpy';//Clave secreta del terminal
                }else{
                    $merchantCode   ="327977559";
                    $key = 'xqSeBn4Qp+EsCTtko9MwNEGqFnMoDj2d';
                }
                $terminal       ="1";
                $amount         =$total*100;
                $currency       ="978";
                $transactionType    ="0";
                $merchantURL    =base_url('tt/procesarPagoMovil');
                $urlOK      =base_url('store/pagoOkMovil/'.$id);
                $urlKO      =base_url('store/pagoKoMovil/'.$id);
                $order      = $id.'-'.date("s");
                $miObj->setParameter("DS_MERCHANT_AMOUNT",$amount);
                $miObj->setParameter("DS_MERCHANT_ORDER",strval($order));
                $miObj->setParameter("DS_MERCHANT_MERCHANTCODE",$merchantCode);
                $miObj->setParameter("DS_MERCHANT_CURRENCY",$currency);
                $miObj->setParameter("DS_MERCHANT_TRANSACTIONTYPE",$transactionType);
                $miObj->setParameter("DS_MERCHANT_TERMINAL",$terminal);
                $miObj->setParameter("DS_MERCHANT_MERCHANTURL",$merchantURL);
                $miObj->setParameter("DS_MERCHANT_URLOK",$urlOK);       
                $miObj->setParameter("DS_MERCHANT_URLKO",$urlKO);
                $version="HMAC_SHA256_V1";                    
                $request = "";
                $params = $miObj->createMerchantParameters();
                $signature = $miObj->createMerchantSignature($key);
                echo '<div style="text-align:center">Redireccionando al banco, por favor espere</div>';
                $this->load->view('checkout',array('version'=>$version,'params'=>$params,'signature'=>$signature));
                if(!empty($_SESSION['carrito'])){
                    unset($_SESSION['carrito']);
                }
            }else{
                redirect('carrito');
            }
        }

        
        function procesarPagoMovil(){             
            $this->load->library('redsysapi');
            $miObj = new RedsysAPI;

            //$this->tienda_model->pagoOkTest(48,array());

            /*$_POST = array(
                'Ds_SignatureVersion' => 'HMAC_SHA256_V1',
                'Ds_Signature' => 'Vr71Chc4xojSd-ibNt3i_szcWsWAMMRL4yBBlUVRn2U=',
                'Ds_MerchantParameters' => 'eyJEc19EYXRlIjoiMDJcLzEwXC8yMDE4IiwiRHNfSG91ciI6IjExOjUzIiwiRHNfU2VjdXJlUGF5bWVudCI6IjEiLCJEc19DYXJkX1R5cGUiOiJDIiwiRHNfQ2FyZF9Db3VudHJ5IjoiNzI0IiwiRHNfQW1vdW50IjoiMSIsIkRzX0N1cnJlbmN5IjoiOTc4IiwiRHNfT3JkZXIiOiI0ODM2My0xMTUxNDEiLCJEc19NZXJjaGFudENvZGUiOiIzMjc5Nzc1NTkiLCJEc19UZXJtaW5hbCI6IjAwMSIsIkRzX1Jlc3BvbnNlIjoiMDAwMCIsIkRzX01lcmNoYW50RGF0YSI6IiIsIkRzX1RyYW5zYWN0aW9uVHlwZSI6IjAiLCJEc19Db25zdW1lckxhbmd1YWdlIjoiMSIsIkRzX0F1dGhvcmlzYXRpb25Db2RlIjoiOTA0MjMzIiwiRHNfQ2FyZF9CcmFuZCI6IjEifQ=='
            );*/
            if (!empty($_POST)){                
                $datos = $_POST["Ds_MerchantParameters"];
                $decodec = json_decode($miObj->decodeMerchantParameters($datos));                                
                //$decodec = $_POST["Ds_MerchantParameters"];  //Eliminar siempre que se pase a produccion                              
                if(!empty($decodec) && ($decodec->Ds_Response=='0000' || $decodec->Ds_Response=='0001' || $decodec->Ds_Response=='0002' || $decodec->Ds_Response=='0099') && empty($decodec->Ds_ErrorCode)){
                    //$id = substr($decodec->Ds_Order,3);    
                    $id = $decodec->Ds_Order;
                    $id = explode('-',$id)[0];                                    
                    $this->tienda_model->pagoOkTest($id,$decodec);
                }else{
                    //$id = substr($decodec->Ds_Order,3);  
                    $id = $decodec->Ds_Order;
                    $id = explode('-',$id)[0];                  
                    $this->tienda_model->pagoNoOkTest($id,$decodec);
                }
                
                
                //correo('joncar.c@gmail.com','POST',print_r($decodec,TRUE));
            }
        }

        function pagarMovil($w,$id = 'W'){
            if(is_numeric($id)){
                $params = $this->db->get_where('app_carritoparams',array('id'=>$id));                
                if($params->num_rows()>0){                    
                    $user = json_decode($params->row()->user);
                    $params = json_decode($params->row()->params);
                    if(is_numeric($user->id)){
                        $user = $this->db->get_where('user',array('id'=>$user->id));
                        if($user->num_rows()>0){
                            $this->tienda_model->cleanVentasTest($user->row());
                            $id = $this->tienda_model->saveVentaApp($params,$user->row());
                            $this->pagar_articulo_movil($id);
                            $this->db->delete('app_carritoparams',array('id'=>$id));
                        }
                    }
                }
            }
        }

        function solicitar_reembolso(){
            $msj = $this->error('Por favor complete los datos solicitados');

            $table = $this->db->field_data('reembolsos');
            foreach($table as $t){
                if($t->null == 'NO' && $t->primary_key==0){                    
                    $this->form_validation->set_rules($t->name,$t->name,'required');
                }
            }
            $this->form_validation->set_rules('politicas','Politícas de privacidad','required');
            $this->form_validation->set_rules('politicas2','Condiciones generales','required');            
            if($this->form_validation->run()){
                //unset($_POST['politicas']);
                //unset($_POST['politicas2']);
                //unset($_POST['g-recaptcha-response']);
                //unset($_POST['form-check']);
                $categoria = $this->db->get_where('categorias',['id'=>$_POST['categorias_id']]);
                if($categoria->num_rows()>0){
                    $_POST['enviado_a'] = $categoria->row()->email_sistema_reembolso;
                    $data = [];
                    foreach($table as $t){
                        if($t->primary_key==0){                    
                            $data[$t->name] = $_POST[$t->name];
                        }
                    }                    
                    $this->db->insert('reembolsos',$data);
                    $this->enviarcorreo($_POST,$categoria->row()->notificacion_reembolso,$_POST['email']);
                    $this->enviarcorreo($_POST,$categoria->row()->notificacion_reembolso,$categoria->row()->email_sistema_reembolso);
                    $msj = $this->success('Ha enviado correctamente su formulario, en los próximos días procederemos a la devolución del importe correspondiente a la cuenta bancaria indicada. También recibirá al correo electrónico indicado su RECIBO DE VIAJE en los próximos días con todos los datos de su plaza para el MIF2021. Gracias.');
                }else{
                    $msj = $this->error('El destino elegido no se encuentra registrado');
                }
            }else{
                $msj = $this->error($this->form_validation->error_string());
            }

            echo $msj;
        }
    }
