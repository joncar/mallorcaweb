<?php /*if(empty($_SESSION['user']) || $_SESSION['admin']==0):*/ ?>
	<!--<h5 class="subtitle-2 align-center">Sistema en mantenimiento, por favor intente ingresar más tarde</h5>-->
<?php /*else:*/ ?>        
        



	<div class="column-section clearfix">

				<h3><strong>Recibo de viaje</strong></h3>
				<div class="spacer-medium"></div>
				<div class="column two-third">
                	
                
                    <form id="contact-form" class="" action="tt/solicitar_reembolso/" onsubmit="sendForm(this,'#result'); return false" method="post">
                    	<div class="form-row">
                            <label style="font-size: 21px;color: gray;">Datos de tu grupo</label>
                        </div>
                        <div class="form-row">
                            <label for="instituto">Nombre del instituto con el que viaja: <abbr title="required" class="required">*</abbr></label>
                            <input type="text" name="instituto" id="instituto" class="instituto req" placeholder="(para localizarle en su grupo)" value="" />
                        </div>

                        <div class="form-row">
                            <label for="localidad">Localidad del grupo <abbr title="required" class="required">*</abbr></label>
                            <input type="text" name="localidad" class="localidad" placeholder="(por si dos grupos tienen el mismo nombre)" value="" />
                        </div>
                        
                        <div class="form-row">
                            <label for="referencia">Nº Referencia del grupo:</label>
                            <input type="text" name="referencia" id="referencia" class="referencia req" placeholder="(no obligatorio, sólo si lo dispone <?= $categorias->row()->label_referencia_reembolso ?>)" value="" />
                        </div>

                        

                        <div class="form-row">
                            <label style="font-size: 21px;color: gray;">Datos del Pasajero/a:</label>
                        </div>

                        <div class="form-row">
                            <label for="nombre_apellido">Nombre y Apellidos: <abbr title="required" class="required">*</abbr></label>
                            <input type="text" name="nombre_apellido" id="nombre_apellido" class="nombre_apellido" placeholder="(nombre, 1r apellido y 2º apellido)" value="" />
                        </div>        
                        <div class="form-row">
                            <label for="dni">DNI/NIE/Pasaporte: <abbr title="required" class="required">*</abbr></label>
                            <input type="text" name="dni" class="dni" placeholder="(introduzca los digitos y letra)" value="" />
                        </div>
                        <div class="form-row">
                            <label for="fecha_nacimiento">Fecha de nacimiento: <abbr title="required" class="required">*</abbr></label>
                            <input type="text" name="fecha_nacimiento" class="fecha_nacimiento mask" data-format="00/00/0000" data-placeholder="__/__/____" value="" />
                        </div>         
                        <div class="form-row">
                            <label for="telefono">Teléfono <abbr title="required" class="required">*</abbr></label>
                            <input type="text" name="telefono" class="telefono" placeholder="(para recibir comunicados)" value="" />
                        </div>       
                        <div class="form-row">
                            <label for="email">E-MAIL: <abbr title="required" class="required">*</abbr></label>
                            <input type="email" name="email" class="email" value="" placeholder="(para recibir comunicados)" />
                        </div>




                        <div class="form-row">
                            <label style="font-size: 21px;color: gray;">Datos del Padre/Madre/Tutor <small style="font-size:13px">(completar sólo si el pasajero es menor de edad a 20/04/2020):</small></label>
                        </div>
                        <div class="form-row">
                            <label for="nombre_tutor">Nombre y Apellidos: </label>
                            <input type="text" name="nombre_tutor" id="nombre_tutor" class="nombre_tutor" placeholder="(nombre, 1r apellido y 2º apellido)" value="" />
                        </div>        
                        <div class="form-row">
                            <label for="dni_tutor">DNI/NIE/Pasaporte: </label>
                            <input type="text" name="dni_tutor" class="dni_tutor" placeholder="(introduzca los digitos y letra)" value="" />
                        </div>
                        <div class="form-row">
                            <label for="fecha_nacimiento_tutor">Fecha de nacimiento: </label>
                            <input type="text" name="fecha_nacimiento_tutor" class="fecha_nacimiento_tutor mask" data-format="00/00/0000" data-placeholder="__/__/____" value="" />
                        </div>         
                        <div class="form-row">
                            <label for="telefono_tutor">Teléfono </label>
                            <input type="text" name="telefono_tutor" class="telefono_tutor" placeholder="(para recibir comunicados)" value="" />
                        </div>       
                        <div class="form-row">
                            <label for="email_tutor ">E-MAIL: </label>
                            <input type="email" name="email_tutor" class="email_tutor" value="" placeholder="(para recibir comunicados)" />
                        </div>



                        <div class="form-row">
                            <label style="font-size: 21px;color: gray;">Nº Cuenta para la devolución:</label>
                        </div>

                        <div class="form-row">
                            <label for="titular_cuenta_bancaria">DNI del titular: <abbr title="required" class="required">*</abbr></label>
                            <input type="text" name="dni_cuenta_bancaria" class="dni_cuenta_bancaria" placeholder="(introduzca los digitos y letra)" value="" />
                        </div>

                        <div class="form-row">
                            <label for="titular_cuenta_bancaria">Titular cuenta bancaria: <abbr title="required" class="required">*</abbr></label>
                            <input type="text" name="titular_cuenta_bancaria" class="titular_cuenta_bancaria" placeholder="(nombre, 1r apellido y 2º apellido)" value="" />
                        </div>
                        <div class="form-row">
                            <label for="cuenta_bancaria">Cuenta Bancaria: <abbr title="required" class="required">*</abbr></label>
                            <input type="text" name="cuenta_bancaria" class="cuenta_bancaria" placeholder="ESXX XXXX XXXX XX XXXXXXXXXX" value="" />
                        </div>  
                        <div class="form-row">
                            <input type="checkbox" name="politicas" value="1"> Acepto las <a href="<?= base_url('politicas-de-privacidad.html') ?>" rel="canonical" target="_new">política de privacidad</a><br/>
                            <input type="checkbox" name="politicas2" value="1"> Acepto la propuesta de revisión con las mismas <a href="<?= base_url($categorias->row()->condiciones_reembolso) ?>" rel="canonical" target="_new">condiciones y servicios</a> incluidos que tenía contratados para realizar el MIF20 que  serán de misma aplicación para el MIF21.
                        </div>
                        
                        <div class="form-row">
                            <div class="alert" id="result">
                                
                            </div>
                        </div>
                        
                        <div class="form-row hidden">
                            <input type="text" id="form-check" name="form-check" value="" class="form-check" />
                        </div> <!-- Spam check field -->

                        <div class="form-row">
                            <div class="g-recaptcha" data-sitekey="6LciCHAUAAAAAM5R0xYoqlfiNcMmKwINyX2GHk2D"></div>
                        </div>
                        
                        <div class="form-row">
                        	<input type="hidden" name="fecha_solicitud" value="<?= date("Y-m-d H:i:s") ?>" />
                            <input type="hidden" name="categorias_id" value="<?= $categorias->row()->id ?>" />
                            <input type="submit" name="submit" class="submit" value="Enviar" />
                        </div>
                    </form>
                </div>
            
            	<div class="column one-third last-col">
                	<?php echo $categorias->row()->texto_sistema_reembolso; ?>
                </div>
            
            </div> <!-- END .column-section -->






	
<?php /*endif*/ ?>