<?php
    $carrito = $this->carrito->getCarrito();     
    $total = 0;
    $impuestos = 0;
?>
<a href="<?= base_url('store/carrito') ?>" class="show-cart">
    <i class="fa fa-shopping-cart"></i>
    <span>
    <?php if(count($carrito)>0): ?>
        <?= count($carrito) ?>
    <?php else: ?>
        0        
    <?php endif ?>
    </span>
</a>
<div class="header-cart-content">
    <table>
        <?php foreach($carrito as $c): ?>
        <tr>
            <td class="cart-item-image">
                <a href="<?= site_url('productos/'. toURL($c->id.'-'.$c->nombre_producto)) ?>">
                    <img src="<?= base_url().'img/fotos_productos/'.$c->foto ?>" alt="<?= $c->nombre_producto ?>" />
                </a>
            </td>
            <td class="cart-item-desc">
                <h6 class="product-name uppercase">
                    <a href="<?= site_url('productos/'. toURL($c->id.'-'.$c->nombre_producto)) ?>">
                        <?= $c->nombre_producto ?>
                    </a>
                </h6>
                <span class="cart-item-quantity"><?= $c->cantidad ?> x </span>
                <span class="cart-item-price"><?= moneda($c->precio) ?>€</span>
            </td>
            <td class="remCart" style="position: absolute;right: 30px;"><a href="javascript:remCart(<?= $c->id ?>)"><i class="fa fa-remove"></i></a></td>
        </tr>
        <?php /*$total+= ($c->cantidad*$c->precio); $impuestos+= $c->cantidad*3.50;*/ ?>
        <?php $total+= ($c->cantidad*$c->precio); ?>
        <?php endforeach ?>             
        <?php $impuestos+= 3.50; ?>
    </table>
    <?php if(count($carrito)==0): ?> 
        <span style="color:black" class="hidden-xs">Carrito Vacio</span>
    <?php endif ?>
    <?php if($total>0): ?>
    <div class="cart-total">
        <h6 class="cart-total-name">Subtotal</h6>
        <div class="cart-total-amount"><?= moneda($total) ?>€</div>
    </div>
    <div class="cart-total">
        <h6 class="cart-total-name">Gastos de Tramitación Bancaria</h6>
        <div class="cart-total-amount"><?= moneda($impuestos) ?>€</div>
    </div>
    <div class="cart-total">
        <h6 class="cart-total-name">Total</h6>
        <div class="cart-total-amount"><?= moneda($total+$impuestos) ?>€</div>
    </div>
    <div class="cart-action">
        <a href="<?= base_url('store/carrito') ?>" class="cart-action-cart sr-button button-mini button-3">Ver carrito</a>
        <a href="<?= base_url('store/carrito') ?>" class="cart-action-checkout sr-button button-mini button-2">Procesar pago</a>
    </div>
    <div style="text-align:right"><a href="javascript:cerrarCarro()"><i class="fa fa-remove"></i> <small>Cerrar</small></a></div>
    <?php endif ?>
</div>