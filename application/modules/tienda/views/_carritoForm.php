
<?php $carrito = $this->carrito->getCarrito(); $total = 0; $impuesto = 0;?>
<?php if(count($carrito)>0): ?>
<div style="text-align: right">
	<a class="sr-button button-1 button-mini" href="<?= base_url('store') ?>"> Seguir comprando</a>
</div>
<table class="table-cart">
	<thead>
		<tr>
			<th class="product-remove">&nbsp;</th>
			<th class="product-image">&nbsp;</th>
			<th class="product-name">Producto</th>
			<th class="product-unit-price">Precio unitario</th>
			<th class="product-quantity">Cantidad</th>
			<th class="product-subtotal">Total</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($carrito as $c): ?>
		<tr>
			<td class="product-remove">
				<a href="javascript:remToCart(<?= $c->id ?>)" class="remove-item"><i class="ion ion-close"></i></a>
			</td>
			<td class="product-image">
				<a href="<?= base_url('store/producto/'.toUrl($c->id.'-'.$c->nombre_producto)) ?>"><img src="<?= base_url('').'img/fotos_productos/'.$c->foto ?>" alt="<?= $c->nombre_producto ?>" /></a>
			</td>
			<td class="product-name">
				<h6 class="uppercase">
					<a href="<?= base_url('store/producto/'.toUrl($c->id.'-'.$c->nombre_producto)) ?>"><?= $c->nombre_producto ?>
					</a>
				</h6>
			</td>
			<td class="product-unit-price">
				<span class="product-price"><?= moneda($c->precio) ?></span>
			</td>
			<td class="product-quantity">
				<div class="quantity">
					<input type="button" class="minus" value="-">
					<input type="text" class="qty" value="<?= $c->cantidad ?>" data-id="<?= $c->id ?>">
					<input type="button" class="plus" value="+">
				</div>
			</td>
			<td class="product-subtotal">
				<span class="product-price"><?= moneda($c->cantidad*$c->precio) ?></span>
			</td>
		</tr>
		<?php $total+= ($c->cantidad*$c->precio); ?>
		<?php endforeach ?>
		<?php $impuesto += 3.50 ?>
		<tr>
				<td></td>
				<td></td>
				<td colspan="3">Subtotal</td>
				<td><?= moneda($total) ?></td>
			</tr>	
		<tr>	
		<tr>
				<td></td>
				<td></td>
				<td colspan="3">Gastos de Tramitación Bancaria</td>
				<td><?= moneda($impuesto) ?></td>
			</tr>	
		<tr>
			<td></td>
			<td></td>
			<td colspan="3">
				<b>Total</b>
			</td>
			<td>
				<b><?= moneda($total+$impuesto) ?></b>
			</td>
		</tr>
		<tr>
			<td colspan="6">
				<form id="formulariopagar" onsubmit="return validarcarrito()" action="<?= base_url('store/checkout') ?>">
					<div style="">
						<div style="text-align:left; font-size:16px"><input type="checkbox" id="politicas1" class="politicas1"> <label style="display:inline-block; vertical-align: top; width:90%; font-weight:300"  for="politicas1">Realizar la reserva implica automáticamente la aceptación de las condiciones generales del paquete de viaje indicadas en esta web.</label>  </div>
						<div style="text-align:left; font-size:16px"><input type="checkbox" id="politicas2" class="politicas2"> <label  style="display:inline-block; vertical-align: top; width:90%; font-weight:300" for="politicas2">Acepto las <a href="<?= base_url('politicas-de-privacidad.html') ?>" rel="canonical" target="_new">política de privacidad</a></label></div>
						<div style="text-align:left; font-size:16px"><input type="checkbox" id="politicas3" class="politicas3"> <label  style="display:inline-block; vertical-align: top; width:90%; font-weight:300" for="politicas3">Acepto las <a href="<?= base_url('condiciones-generales.html') ?>" rel="canonical" target="_new">condiciones generales de compra</a></label></div>
					</div>
					<div style="width:100%; float:right; height: 18px"></div>
					<div id="result"></div><br/>
					<?php
						$ajustes = $this->db->get('ajustes')->row();
						if($ajustes->habilitar_tpv==1 && strtotime($ajustes->suspender_ventas_hasta)<=time()):
					?>
					<input class="update-cart" value="Pagar" type="submit">
					<?php else: ?>
					<input class="update-cart" value="Agotado" type="button" disabled="true" style="opacity: .3">
					<?php endif ?>
				</form>	
				
			</td>
		</tr>
	</tbody>
</table>
<?php else: ?>
	Carrito Vacio
<?php endif ?>
<div class="popup2">
	<div class="backanimate" style="color:white; font-family: 'Montserrat'; font-size:1rem; padding:20px;">
		<a href="#" class="close2" style="right: 20px; top: 0.4em; font-size:2em; "><i class="fa fa-remove"></i></a>
		<h2 style="color:white">Ya estás a un paso menos de vivir el MALLORCA ISLAND FESTIVAL!</h2>
		<h4 style="font-family: 'Montserrat'; text-align: center; color:white; margin-top: 27px; line-height: 18px;">
			Para hacer el pago tienes que tener preparado:
		</h4>
		<ol style="text-align: center;">
			<li>Una tarjeta de Crédito / Débito para hacer el pago.</li>
			<li>El teléfono móvil del titular de la tarjeta, dónde os llegará un SMS con el código de confirmación de pago.</li>
		</ol>
		<h4 style="font-family: 'Montserrat'; text-align: center; color:white; margin-top: 27px; line-height: 18px;">
			¿Quieres que hacerlo aún más fácil?
		</h4>
		<p> ¡Prepara la tarjeta y llama al <a href="tel:+34902002068">902 002 068</a>, y nuestras operadoras te harán el pago On Line!</p>
	</div>

</div>