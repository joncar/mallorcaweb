<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Feedback extends Panel{
        function __construct() {
            parent::__construct();
        } 
        
        function loadView($view = ''){
            if(is_string($view)){
                $output = $this->load->view($view,array(),TRUE);
                $view = array('view'=>'panel','crud'=>'user','output'=>$output);
            }
            parent::loadView($view);
        }
        
        function feedback($action = '',$id = ''){
        	$crud = $this->admin_feedback();
        }

        function admin_feedback(){
        	$this->as['admin_feedback'] = 'feedback';
        	$crud = $this->crud_function('','');
        	$crud->set_url('paginas/feedback/admin_feedback/');
        	$crud->field_type('feedback','image',array('path'=>'img/feedback','width'=>'616px','height'=>'1020px'));        	
            $crud->field_type('tipo_red','dropdown',array('instagram'=>'Instagram','facebook'=>'Facebook'));
        	$crud = $crud->render();
        	$crud->output = $this->load->view('feedback_admin',array('output'=>$crud->output),TRUE);
        	$this->loadView($crud);        	
        }

        
    }
?>
