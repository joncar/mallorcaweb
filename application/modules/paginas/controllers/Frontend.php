<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{        
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
            $this->load->library('form_validation');
        }       

        public function loadView($param = array('view' => 'main')) {
            if($this->router->fetch_method()!='editor'){
                $param['page'] = $this->querys->fillFields($param['page']);
            }
            parent::loadView($param);
        }
        
        function read($url){
            $theme = $this->theme;
            $params = $this->uri->segments;
            $this->load->model('querys');
            $this->loadView(
                array(
                    'view'=>'read',
                    'page'=>$this->load->view($theme.$url,array(),TRUE),
                    'link'=>$url,
                    'title'=>ucfirst(str_replace('-',' ',$url)),
                    'url'=>base_url($url).'.html'
                )
            );
        }
        
        function getFormReg($x = '2'){                    
            return $this->querys->getFormReg($x);
        }
        
        function editor($url){            
            $this->load->helper('string');
            if(!empty($_SESSION['user']) && $this->user->admin==1){                
                //$page = file_get_contents('application/modules/paginas/views/'.$url.'.php');
                /*$page = str_replace('<?php','[?php',$page);
                $page = str_replace('<?=','[?=',$page);
                $page = str_replace('&gt;','>',$page);
                $page = str_replace('&lt;','<',$page);*/
                $page = $this->load->view($url,array(),TRUE);
                $this->loadView(array('view'=>'cms/edit','scripts'=>true,'name'=>$url,'edit'=>TRUE,'page'=>$page,'title'=>'Editar '.ucfirst(str_replace('-',' ',$url))));
            }else{
                redirect(base_url());
            }
        }
        
        function contacto(){             
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('nombre','Nombre','required');
            $this->form_validation->set_rules('telefono','Telefono','required');   
            $this->form_validation->set_rules('politicas','Politicas','required');   
            $this->form_validation->set_rules('subject','Motivo','required');         
            if($this->form_validation->run()){
                $this->load->library('recaptcha');
                //if(!$this->recaptcha->recaptcha_check_answer($_POST['g-recaptcha-response'])){
                    //$_SESSION['msj'] = $this->error('Captcha introducido incorrectamente');
                //}else{
                    if($_POST['subject']=='contacto'){
                        $this->form_validation->set_rules('fecha_nacimiento','Fecha de nacimiento','required');
                        $this->form_validation->set_rules('instituto','Instituto','required');
                        $this->form_validation->set_rules('curso','Curso','required');   
                        $this->form_validation->set_rules('poblacion','Población','required');   
                        $this->form_validation->set_rules('nro_estudiantes','Numero de estudiantes','required');  
                        $this->form_validation->set_rules('oferta','¿Cómo te ha llegado la oferta?','required');   
                        $this->form_validation->set_rules('asunto','Asunto','required');   
                        $this->form_validation->set_rules('message','Mensaje','required');   
                        if(!$this->form_validation->run()){
                            echo json_encode(array('text'=>$this->form_validation->error_string()));
                            unset($_SESSION['msj']);
                            return 0;
                        }
                    }
                    $this->enviarcorreo((object)$_POST,6,'info@mallorcaislandfestival.com');
                    unset($_POST['politicas']);
                    unset($_POST['form-check']);
                    unset($_POST['subject']);
                    unset($_POST['fields']);
                    unset($_POST['g-recaptcha-response']);
                    $_POST['fecha'] = date("Y-m-d H:i:s");
                    $this->db->insert('contacto',$_POST);
                    $_SESSION['msj'] = 'Gracias por contactarnos, en breve nos pondremos en contacto';
                //}
            }else{                
               $_SESSION['msj'] = $this->form_validation->error_string().'<script>$("#guardar").attr("disabled",false); </script>';
            }

            echo json_encode(array('text'=>$_SESSION['msj']));
            unset($_SESSION['msj']);
        }
        
        function enviarCurriculum(){
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('nombre','Nombre','required');
            $this->form_validation->set_rules('mensaje','Comentario','required');
            //$this->form_validation->set_rules('curriculum','Curriculum','required');            
            if($this->form_validation->run() && !empty($_FILES['curriculum'])){
                get_instance()->load->library('mailer');
                get_instance()->mailer->mail->AddAttachment($_FILES['curriculum']['tmp_name'],$_FILES['curriculum']['name']);
                $this->enviarcorreo((object)$_POST,2,'info@finques-sasi.com');
                //$_SESSION['msj'] = $this->success('Gracias por contactarnos, en breve le llamaremos');
                echo json_encode(array('success'=>TRUE,'message'=>'Gracias por contactarnos, en breve le contactaremos'));
            }else{
                //$_SESSION['msj'] = $this->error('Por favor complete los datos solicitados  <script>$("#guardar").attr("disabled",false); </script>');
               echo json_encode(array('success'=>FALSE,'message'=>'Por favor complete los datos solicitados  <script>$("#guardar").attr("disabled",false); </script>'));
            }
            /*if(!empty($_GET['redirect'])){
                redirect($_GET['redirect']);
            }else{
                redirect(base_url('p/contacto'));
            }*/
        }

        function confirmar_subscripcion($id){
            $id = base64_decode($id);
            if(is_numeric($id)){
                $email = $this->db->get_where('subscritos_solicitudes',array('id'=>$id));
                if($email->num_rows()>0){
                    $email = $email->row()->email;
                    $this->db->insert('subscritos',array('email'=>$email));  
                    $this->db->delete('subscritos_solicitudes',array('email'=>$email));
                    $this->enviarcorreo((object)array(),10,$email);  
                    $_SESSION['msj2'] = 'Subscripción exitosa';                    
                }else{
                    $_SESSION['msj2'] = 'Subscripción ya ha sido procesada';
                }
            }

            header('Location:'.base_url().'main#footer');
        }
        
        function subscribir(){
            $err = 'error';
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('politicas','Políticas','required');
            if($this->form_validation->run()){
                
                $emails = $this->db->get_where('subscritos',array('email'=>$_POST['email']));
                $success = $emails->num_rows()==0?TRUE:FALSE;
                if($success){
                    $this->db->insert('subscritos_solicitudes',array('email'=>$_POST['email']));    
                    $_SESSION['msj2'] = 'Subscripción exitosa';
                    $err = 'success';
                    $_POST['link'] = base_url('paginas/frontend/confirmar_subscripcion/'.base64_encode($this->db->insert_id()));
                    $this->enviarcorreo((object)$_POST,11);
                }else{
                    $_SESSION['msj2'] = 'Ya el correo esta registrado';
                }
            }else{
                $_SESSION['msj2'] = $this->error($this->form_validation->error_string());
            }
            echo json_encode(array('result'=>$err,'msg'=>$_SESSION['msj2']));
            unset($_SESSION['msj2']);
            //redirect(base_url().'p/contacte#subscribir');
        }
        
        function unsubscribe(){
            if(empty($_POST)){
                $this->loadView('includes/template/unsubscribe');
            }else{
                $emails = $this->db->get_where('subscritos',array('email'=>$_POST['email']));
                $success = $emails->num_rows()>0?TRUE:FALSE;
                if($success){
                    $this->db->delete('subscritos',array('email'=>$_POST['email']));
                    echo $this->success('Correo desafiliado al sistema de noticias');
                }            
                $this->loadView(array('view'=>'includes/template/unsubscribe','success'=>$success));
            }
        }
        
        function pdf(){
            require_once APPPATH.'libraries/html2pdf/html2pdf.php';
            $papel = 'L';
            $orientacion = 'P';
            $html2pdf = new HTML2PDF();
            $html2pdf->setDefaultFont('raleway');
            $html2pdf->addFont("ralewayb","B");
            $html2pdf->addFont("titanone","");
            $menu = $this->db->get('menu_del_dia')->row();
            $html = $menu->pdf;
            foreach($menu as $n=>$v){
                $html = str_replace('['.$n.']', str_replace('&bull;','<br/> &bull;',strip_tags($v)),$html);
            }
            $html = str_replace('[precio]',$this->db->get('ajustes')->row()->precio_menu_dia.'€',$html);
            $html = $this->load->view('pdf',array('html'=>$html),TRUE);
            $html2pdf->writeHTML($html); 
            //echo $this->db->get('menu_del_dia')->row()->pdf;
            ob_end_clean();         
            $html2pdf->Output('Menu-del-dia-'.date("dmY").'.pdf');
        }

        function galeria(){
            $this->loadView(array('view'=>'galeria','title'=>'Galeria','page'=>''));
        }

        function galeria_equipo(){
            $this->loadView(array('view'=>'galeria','title'=>'Galeria de nuestro equipo','page'=>''));
        }

        function validar_politicas($crud){

            if($crud->getParameters(FALSE)=='insert_validation'){                
                if(empty($_POST['politicas'])){
                    echo '<textarea>'.json_encode(array('success'=>false,'error_message'=>'Debe aceptar las politicas de privacidad')).'</textarea>';
                    die();
                }
            }else{
                unset($_POST['politicas']);
            }
        }

         function concurso($x = ''){
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();
            $this->validar_politicas($crud);
            $crud->set_table('concurso')
                 ->set_theme('bootstrap3')
                 ->set_subject('')                 
                 ->unset_back_to_list()
                 ->unset_delete()
                 ->unset_read()
                 ->unset_print()
                 ->required_fields_array()
                 ->unset_jquery()
                 ->unset_edit()
                 ->display_as('nombre','Nombre Completo')
                 ->display_as('email','Email')
                 ->display_as('ano_nacimiento','Año de nacimiento')
                 ->display_as('telefono','Teléfono para comunicarte que eres el/la ganador')
                 ->display_as('instituto','Instituto donde estudias')
                 ->display_as('curso','Curso que estudias')
                 ->display_as('propuesta','Tu mejor respuesta a por qué quieres ir al MIF2020 con un acompañante en condición de Very Important People (V.I.P.)')                 
                 ->set_rules('email','Email','required|is_unique[concurso.email]|valid_email')
                 ->field_type('fecha','invisible');
            $crud->set_url('paginas/frontend/concurso/');
            $crud->callback_field('curso',function($val){
                return form_dropdown('curso',array('1º Bachillerato'=>'1º Bachillerato','2º Bachillerato'=>'2º Bachillerato','ESO'=>'ESO','Otros'=>'Otros'),'id="field-curso"');
            })
            ->callback_before_insert(function($post){
                $post['fecha'] = date("Y-m-d H:i:s");
                return $post;
            })
            ->callback_after_insert(function($post,$primary){
                get_instance()->enviarcorreo((object)$post,7,'info@mallorcaislandfestival.com');
                get_instance()->enviarcorreo((object)$post,8);
            });
            if(empty($x)){
                $crud = $crud->render(2);            
            }else{
                $crud = $crud->render();            
            }
            $page = $this->load->view($this->theme.'concurso',array(),TRUE);
            $page = str_replace('[output]',$crud->output,$page);
            $this->loadView(
                array(
                'view'=>'read',
                'js_files'=>$crud->js_files,

                'page'=>$page,                
                'title'=>'Concurso MIF'
                )
            );
        }


        function contacta($x = ''){
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();
            $this->validar_politicas($crud);
            $crud->set_table('contacta')
                 ->set_theme('bootstrap3')
                 ->set_subject('')                 
                 ->field_type('comentario','editor',array('type'=>'textarea'))
                 ->unset_back_to_list()
                 ->unset_delete()
                 ->unset_read()
                 ->unset_print()
                 ->required_fields_array()
                 ->unset_jquery()
                 ->unset_edit()
                 ->display_as('nombre','Nombre Completo')
                 ->display_as('telefono','Teléfono')
                 ->display_as('estudiantes','Nº de Estudiantes')
                 ->display_as('oferta','¿Como te ha llegado la oferta?')
                 ->field_type('oferta','set',array(
                    'Flaix FM'=>'Flaix FM',
                    'Radio FlaixBack'=>'Radio FlaixBack',
                    'Prensa'=>'Prensa',
                    'TV'=>'TV',
                    'Recomendación de un amigo'=>'Recomendación de un amigo',
                    'Discoteca'=>'Discoteca',
                    'Profesor'=>'Profesor',
                    'AMPA'=>'AMPA',
                    'Redes sociales'=>'Redes sociales',
                    'Otros'=>'Otros'
                 ));
            
            $crud->set_url('paginas/frontend/contacta/');
            $crud->callback_before_insert(function($post,$primary){
                $post['fecha'] = date("Y-m-d H:i:s");
                return $post;
            });
            $crud->callback_after_insert(function($post,$primary){
                $post['oferta'] = implode(',',$post['oferta']);
                get_instance()->enviarcorreo((object)$post,9,'info@mallorcaislandfestival.com');                
            });
            if(empty($x)){
                $crud = $crud->render(2);            
            }else{
                $crud = $crud->render();            
            }
            $page = $this->load->view($this->theme.'mas_informacion',array(),TRUE);
            $page = str_replace('[output]',$crud->output,$page);
            $this->loadView(
                array(
                'view'=>'read',
                'js_files'=>$crud->js_files,

                'page'=>$page,                
                'title'=>'Pedir más información'
                )
            );
        }

        function curriculum($x = ''){
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();
            $this->validar_politicas($crud);
            $crud->set_table('trabaja_con_nosotros')
                 ->set_theme('bootstrap3')
                 ->set_subject('')
                 ->set_field_upload('curriculum','files/curriculum')
                 ->set_field_upload('titulaciones','files/curriculum')
                 ->field_type('comentarios','editor',array('type'=>'textarea'))                 
                 ->unset_back_to_list()
                 ->unset_delete()
                 ->unset_read()
                 ->unset_print()
                 ->required_fields_array()
                 ->unset_jquery()
                 ->unset_edit();
            $crud->set_url('paginas/frontend/curriculum/');
            if(empty($x)){
                $crud = $crud->render(2);            
            }else{
                $crud = $crud->render();            
            }
            $page = $this->load->view($this->theme.'trabaja-con-nosotros',array(),TRUE);
            $page = str_replace('[output]',$crud->output,$page);
            $this->loadView(
                array(
                'view'=>'read',
                'js_files'=>$crud->js_files,

                'page'=>$page,                
                'title'=>'Trabaja con nosotros'
                )
            );
        }

        function search(){
            if(!empty($_GET['q'])){
                if(empty($_SESSION[$_GET['q']])){
                    $_SESSION[$_GET['q']] = file_get_contents('http://www.google.es/search?ei=meclXLnwCNma1fAPgbCYCA&q=site%3Amallorcaislandfestival.com+'.urlencode($_GET['q']).'&oq=site%3Amallorcaislandfestival.com+'.urlencode($_GET['q']).'&gs_l=psy-ab.3...10613.16478..16743...0.0..0.108.1753.20j3......0....1..gws-wiz.0XRgmCZL0TA');                
                }
                $result = $_SESSION[$_GET['q']];
                preg_match_all('@<div id=\"ires\">(.*)</div>@si',$result,$result);
                $resultado = $result[0][0];
                $resultado = fragmentar($resultado,'<ol>','</ol>');
                $resultado = !empty($resultado[0])?$resultado[0]:array();

                if(!empty($resultado)){
                    $resultado = str_replace('http://www.mallorcaislandfestival.com/url?q=','',$resultado);
                    $resultado = str_replace('/url?q=','',$resultado);
                    $resultado = explode('<div class="g">',$resultado);                
                    foreach($resultado as $n=>$r){
                        if(strpos($r,'/search?q=site:')){
                            unset($resultado[$n]);
                            continue;
                        }
                        $resultado[$n] = '<div class="g">'.$r;                    
                        $resultado[$n] = substr($r,0,strpos($r,'&'));
                        $pos = strpos($r,'">')+2;
                        $rr = substr($r,$pos);
                        $pos = strpos($rr,'">');
                        $rr = substr($rr,$pos);                    
                        $resultado[$n].= $rr;
                        $resultado[$n] = '<div class="g">'.$resultado[$n];
                        $resultado[$n] = utf8_encode($resultado[$n]);
                    }
                }
                
                $this->loadView(array(
                    'view'=>'read',
                    'page'=>$this->load->view($this->theme.'search',array('resultado'=>$resultado),TRUE,'paginas'),
                    'result'=>$resultado,
                    'title'=>'Resultado de busqueda'
                ));

            }
        }

        function sitemap(){
            $pages = array(
                base_url(),
                base_url().'contacto.html',
                base_url().'contacte.html',
                base_url().'historia.html',
                base_url().'equip',
                base_url().'equipo',
                base_url().'serveis',
                base_url().'servicios',
                base_url().'faq.html',
                base_url().'avis-legal.html',
                base_url().'aviso-legal.html',
                base_url().'galeria.html',
                base_url().'blog',
            );

            //Blogs
            foreach($this->db->get_where('blog',array('blog.blog_categorias_id'=>10))->result() as $b){
                $pages[] = site_url('blog/'.toURL($b->id.'-'.$b->titulo));
            }
            //Serveis
            foreach($this->db->get_where('blog',array('blog_categorias_id'=>9))->result() as $b){
                $link = $b->idioma=='es'?'servicios':'serveis';
                $pages[] = base_url($link.'/'.toUrl($b->id.'-'.$b->titulo));
            }
            //equip
            foreach($this->db->get_where('blog',array('blog_categorias_id'=>11))->result() as $b){
                $link = $b->idioma=='es'?'equipo':'equip';
                $pages[] = base_url($link.'/'.toUrl($b->id.'-'.$b->titulo));
            }
            $site = '<?xml version="1.0" encoding="UTF-8"?>
            <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
            foreach($pages as $p){
                $site.= '
                <url>
                      <loc>'.trim($p).'</loc>
                      <lastmod>'.date("Y-m-d").'T11:43:00+00:00</lastmod>
                      <priority>1.00</priority>
                </url>';
            }
            $site.= '</urlset>';
            ob_end_clean();
            ob_end_flush();
            header('Content-Type: application/xml');
            echo $site;
        }
    }
