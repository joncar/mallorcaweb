<!-- HEADER -->
<header id="header" class="header-transparent transparent-light">
	[menu]
</header>
<!-- HERO  -->
<!-- HERO  -->
<section id="hero" class="hero-full parallax-section text-light" data-parallax-image="[base_url]theme/theme/files/uploads/hero-about-44.jpg">
	
	<div id="page-title" class="wrapper align-center">
		<h4 class="subtitle-2">Preguntas frecuentes</h4>
		<hr class="small fat colored">
		<h1>FAQ's</h1>
		</div> <!-- END #page-title -->
		<a href="#" id="scroll-down"></a>
		
	</section>
	<!-- HERO -->
	
	
	<!-- PAGEBODY -->
	<section id="page-body" class="notoppadding">
		
		<div class="column-section boxed-sticky adapt-height clearfix">
			<div class="column one-half has-animation" data-delay="200" style="background:url([base_url]theme/theme/files/uploads/about-team-faq.jpg) center center;background-size:cover;"></div>
			<div class="column one-half bigpadding last-col has-animation" style="background:#ffffff;">
				<h3><span>Resolvemos tus dudas</span> <hr class="small fat colored"></h3>
				<div class="spacer-medium"></div>
				
				<!-- ACCORDION -->
				<div class="accordion">
					<div class="toggle-item">
						<div class="toggle-title">
							<h5 class="toggle-name">¿Qué es el Fin de Curso Mallorca Island Festival?</h5>
						</div>
						<div class="toggle-inner"><p>El Fin de Curso Mallorca Island Festival es un evento creado y registrado hace 10 años por el GRUPO MIF, para dar la oportunidad a los estudiantes de bachillerato de poder celebrar el fin de una etapa de forma totalmente organizada.</p></div>
					</div>
					<div class="toggle-item">
						<div class="toggle-title">
							<h5 class="toggle-name">¿Por qué viajar con MIF?</h5>
						</div>
						<div class="toggle-inner"><p>Como agencia de viajes especializada, contamos con una alta experiencia en la gestión y preparación de viajes para estudiantes.<br>
						Líderes en este sector, movilizamos año tras año miles de estudiantes, por lo que conseguimos grandes descuentos tanto en el transporte cómo en el hospedaje, por ello, tenemos un precio tan competitivo y la seguridad de qué todos nuestros estudiantes han disfrutado de la mejor experiencia en su viaje de despedida.</p></div>
					</div>
					<div class="toggle-item">
						<div class="toggle-title">
							<h5 class="toggle-name">¿De qué se tienen que encargar los estudiantes para realizar el viaje?</h5>
						</div>
						<div class="toggle-inner"><p>Con MIF, los estudiantes únicamente se deben de preocupar de estudiar para los exámenes, ya que nuestro equipo de profesionales ya lo tiene todo preparado para el viaje: autobuses, barcos, traslados, hospedaje, actividades, excursiones, seguros, etc.<br>
						Nuestras salidas están programadas después de la selectividad, para que no impacte en el curso académico, por lo que nuestra insignia es “tú encárgate de aprobar y luego disfruta de la recompensa”.</p></div>
					</div>
					<div class="toggle-item">
						<div class="toggle-title">
							<h5 class="toggle-name">¿Qué son los “Staff MIF”?</h5>
						</div>
						<div class="toggle-inner"><p>Los Staff MIF es el staff técnico que se encarga de la organización y de las gestiones administrativas durante el viaje, para que los estudiantes no se tengan que preocupar de nada. Los Staff MIF, asesoraran a los estudiantes en todas las consultas que deseen plantearle durante el viaje.</p></div>
					</div>
					<div class="toggle-item">
						<div class="toggle-title">
							<h5 class="toggle-name">¿Tengo los traslados incluidos?</h5>
						</div>
						<div class="toggle-inner"><p>Si, con MIF está incluido el traslado de inicio desde vuestro Instituto hasta el Puerto de Barcelona con motivo qué podáis realizar el embarque todos juntos.<br>
						También están incluidos los traslados desde el Puerto de Mallorca hasta el Resort y viceversa y la vuelta en barco hasta el Puerto de Barcelona.</p></div>
					</div>
					<div class="toggle-item">
						<div class="toggle-title">
							<h5 class="toggle-name">¿Qué podemos hacer durante nuestra estancia en Mallorca?</h5>
						</div>
						<div class="toggle-inner"><p>MIF, como cada año, ya lo tiene todo preparado y a la llegada al Resort, les daremos la bienvenida a los grupos con nuestro “Cóctel de Bienvenida” en la sala Welcome Meeting del Resort dónde se realizará una breve presentación del programa y horarios de las actividades, excursiones y discotecas del Viaje Fin de Curso Mallorca Island Festival.</p></div>
					</div>
					<div class="toggle-item">
						<div class="toggle-title">
							<h5 class="toggle-name">¿Dónde nos encontraremos con los “Staff MIF”?</h5>
						</div>
						<div class="toggle-inner"><p>En el autobús que recogerá a los estudiantes en su instituto les dará la bienvenida los Staff MIF quiénes acompañarán a los grupos hasta el Puerto de Barcelona y les indicará cuál es la zona de embarque.<br>
							En el Puerto de Palma de Mallorca se encontrarán los Staff MIF de Mallorca quiénes indicarán a los estudiantes los autobuses para desplazarnos hacia el Resort.<br>
						Nuestros Staff MIF también se encargan de forma dinámica de ayudar a los grupos a realizar el check-in en el Resort y la entrega de llaves, de guiar en las excursiones, organizar las actividades y atender a las consultar que pueda tener el grupo.</p></div>
					</div>
					<div class="toggle-item">
						<div class="toggle-title">
							<h5 class="toggle-name">¿Cómo reservamos los tickets de actividades?</h5>
						</div>
						<div class="toggle-inner"><p>En el bus de ida en dirección al Mallorca Island Festival podréis contratar las actividades que vais a querer hacer durante vuestro Viaje de Fin de Curso!</p></div>
					</div>
					<div class="toggle-item">
						<div class="toggle-title">
							<h5 class="toggle-name">¿Es obligatorio que todo nuestro grupo hagamos las mismas excursiones o actividades?</h5>
						</div>
						<div class="toggle-inner"><p>Con el Fin de Curso Mallorca Island Festival los estudiantes pueden elegir que quieren hacer individualmente, es decir, no es necesario que todos hagáis las mismas actividades. Unos cuantos de clase pueden hacer la excursión en catamarán, otros pueden ir al parque acuático mientras otros toman el sol en la playa o piscina, etc..</p></div>
					</div>
										<div class="toggle-item">
						<div class="toggle-title">
							<h5 class="toggle-name">¿Puede contratar mi seguro de viaje?</h5>
						</div>
						<div class="toggle-inner"><p>Le informamos que puede contratar un seguro adicional de cancelación por motivos académicos y/o hospitalario, puede realizar la consulta de las condiciones a través de nuestro correo electrónico.</p></div>
					</div>
					<div class="toggle-item">
						<div class="toggle-title">
							<h5 class="toggle-name">¿Qué documentación necesito para viajar?</h5>
						</div>
						<div class="toggle-inner"><p>Todos los pasajeros, deberán llevar en regla su documentación personal correspondiente (con el D.N.I. o N.I.E. es suficiente, importante comprobar que no esté caducado) o en caso de no disponer de él será necesario el pasaporte.</p></div>
					</div>
					<div class="toggle-item">
						<div class="toggle-title">
							<h5 class="toggle-name">¿Cuántas maletas puedo llevar en el viaje?</h5>
						</div>
						<div class="toggle-inner"><p>Con MIF, para los pasajeros con salida desde el Puerto de Barcelona no hay restricciones de equipaje. En el caso de tener que realizar traslados aéreos desde otras comunidades autónomas, se deberá consultar política de equipajes de la compañía aérea.</p></div>
					</div>
					<div class="toggle-item">
						<div class="toggle-title">
							<h5 class="toggle-name">¿Están incluidas las comidas en el viaje?</h5>
						</div>
						<div class="toggle-inner"><p>En el Viaje Fin de Curso Mallorca Island Festival está incluida la Pensión Completa +Bebidas (PC). Los apartamentos también disponen de cocina completamente equipada con nevera y en el mismo resort hay varios supermercados.</p></div>
					</div>
					<div class="toggle-item">
						<div class="toggle-title">
							<h5 class="toggle-name">¿Puede variar el precio del Viaje Fin de Curso Mallorca Island Festival?</h5>
						</div>
						<div class="toggle-inner"><p>No, el precio del viaje es definitivo y no depende del número de pasajeros que viajéis ya al movilizar a miles de estudiantes año tras año podemos ofrecer el mejor precio a todos los participantes, seáis 10 o 100.</p></div>
					</div>
					<div class="toggle-item">
						<div class="toggle-title">
							<h5 class="toggle-name">¿Puede viajar con nuestro grupo compañeros/as que no estén estudiando con nosotros?</h5>
						</div>
						<div class="toggle-inner"><p>Sí, aunque el viaje es sólo para estudiantes de bachillerato, si vuestro grupo no tiene inconveniente, pueden acompañaros al viaje pasajeros que no estén estudiando con vosotros siempre que tengan más de 16 años.</p></div>
					</div>
					<div class="toggle-item">
						<div class="toggle-title">
							<h5 class="toggle-name">¿Cómo puedo obtener un descuento en mi viaje?</h5>
						</div>
						<div class="toggle-inner"><p>Una vez efectuada la reserva del viaje, os entregaremos a todos los participantes de vuestro grupo, de forma gratuita, el TALONARIO “VIAJA GRATIS” para entrar en sorteo múltiples regalos con los que podréis obtener importantes descuentos en el precio de vuestro viaje.</p></div>
					</div>
					<div class="toggle-item">
						<div class="toggle-title">
							<h5 class="toggle-name">¿Cuáles son los métodos de pago?</h5>
						</div>
						<div class="toggle-inner"><p>Una vez hayáis decidido vivir la experiencia, os facilitaremos el documento de reserva dónde se detalla el procedimiento. Las reservas las podéis efectuar en efectivo en cualquier entidad bancaria, mediante transferencia, a través de nuestra web “apartado reserva on-line” o incluso llamando a nuestra operadora con tarjeta de crédito/debito al 902 002 068.</p></div>
					</div>
					<div class="toggle-item">
						<div class="toggle-title">
							<h5 class="toggle-name">¿Una vez hecha la reserva, ¿cómo puedo informarme de todo lo relativo al viaje?</h5>
						</div>
						<div class="toggle-inner"><p>Una de nuestras señas de identidad, es la asistencia, y el servicio a todos y cada uno de los viajeros. Cada grupo tendrá asignado un miembro de nuestro Staff al que podrá consultarle cualquier duda relativa al viaje, pagos, u otros aspectos. Además, gracias a nuestro novedoso servicio, cada grupo tendrá un apartado exclusivo y personal en las redes sociales, en el cuál, además de interactuar con el resto de compañeros que acudan al viaje, podréis encontrar toda la información relativa al mismo, como número de personas asistentes, calendario de pagos, calendario de fiestas, incentivos elegidos por el grupo, fecha del viaje y muchas más opciones.</p></div>
					</div>
					<div class="toggle-item">
						<div class="toggle-title">
							<h5 class="toggle-name">¿Puedo  viajar siendo menor, podré acceder a las discotecas?</h5>
						</div>
						<div class="toggle-inner"><p>Sí, todos los pasajeros tendrán que firmar una autorización y siendo menor de edad deberá autorizar el viaje el padre, madre o tutor legal del pasajero. Todas las actividades y discotecas están adaptadas para garantizar el acceso tanto a menores como mayores de edad.</p></div>
					</div>
					<div class="toggle-item">
						<div class="toggle-title">
							<h5 class="toggle-name">¿Queremos hacer unas camisetas para llevarlas al viaje, y otras tantas para venderlas. ¿Podéis hacérnoslas vosotros?</h5>
						</div>
						<div class="toggle-inner"><p>La respuesta también es afirmativa. Tenemos acuerdos con empresas colaboradoras en la creación de merchandising de cualquier tipo, y podemos adaptar cualquier diseño que nos pidáis. No obstante, en todos los viajes, nosotros siempre os damos un detalle a la salida del grupo.</p></div>
					</div>
					<div class="toggle-item">
						<div class="toggle-title">
							<h5 class="toggle-name">¿Cuándo llegamos a destino, ¿hay alguien que pueda ayudarnos si surge algún imprevisto?</h5>
						</div>
						<div class="toggle-inner"><p>Sí. Cada edición del Mallorca Island Festival participan más de 8.000 estudiantes de bachillerato, por eso somos todos una gran familia y tanto en origen como en destino trabajamos un equipo de más de 80 miembros de la organización. Todo el staff técnico somos gente joven y muy preparada, por lo que aparte de convertirnos en vuestros mejores amigos de viaje, nos podréis realizar cuántas consultas necesitéis. ¡Nosotros estaremos encantados de acompañaros en esta aventura!</p></div>
					</div>
				</div>
				
				
				<div style="margin:20px 0; "><a href="[base_url]condiciones-generales.html">
					<h5 class="subtitle-2 colored">+ INFORMACIÓN SOBRE CONDICIONES GENERALES</h5></a>
				</div>
			</div>
			</div> <!-- END .column-section -->
			
			<div class="spacer-big"></div>
			<div>[contacto]</div>
			<div class="spacer-big"></div>
			<div>[footer]</div>
		</section>
		<!-- PAGEBODY -->