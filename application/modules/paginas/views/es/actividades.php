<!-- HEADER -->
	<header id="header" class="header-transparent transparent-light">        		
    	[menu]    			        
	</header>
	<!-- HERO  -->
    <section id="hero" class="hero-full parallax-section text-light" data-parallax-image="[base_url]theme/theme/files/uploads/hero-about-39.jpg">
    
        <div id="page-title" class="wrapper align-center">
            <h4 class="subtitle-2">No te pierdas ni una</h4>
            <hr class="small fat colored">
            <h1>Actividades</h1>
        </div> <!-- END #page-title -->
        <a href="#" id="scroll-down"></a>
            
    </section>
    <!-- HERO -->
    <section id="page-body" class="notoppadding">
    	<div class="column-section boxed-sticky adapt-height clearfix">
            <div class="column one-half has-animation empty-content animated" data-delay="200" style="background: rgba(0, 0, 0, 0) url([base_url]theme/theme/files/uploads/about-team-14.jpg) repeat scroll center center / cover; min-height: 661px;"></div>
        	<div class="column one-half bigpadding last-col has-animation tallest animated" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 661px;"><div class="col-content" style="margin-top: 0px;">
            	<h5 class="subtitle-2">Gratis para todo el grupo</h5>
                <hr class="small fat colored">
                <h2><strong>Paintball <br> Pool Parties <br> Barra Libre de Cócteles <br> Toro Mecánico e Hinchables <br> Futbolín Humano <br> Gladiadores <br> Crazy Games <br> Conciertos <br> 1 hora gratis Pista Tenis, Fútbol, Pádel y Minigolf <br> Torneo 4x4 <br> Vóley/Futbol Playa <br> Pool Sports: Water-Voley, Water Polo, Basket-Pool</strong></h2>
                <p></p>
            </div></div>
        </div>		
        <div class="fullwidth-section text-light parallax-section" data-parallax-image="[base_url]theme/theme/files/uploads/parallax5.jpg">
			<div class="fullwidth-content">
		    
		        <div class="wrapper-small align-center">
		            <h5 class="subtitle-2">Actividades Opcionales</h5>
		            <hr class="small fat colored">
		            <h2><strong>Banana Ride, Phantom, Rings, Pedal Boat, SofaWater Ski, Parasiling, Crazy Ball, etc.</strong></h2>
		        </div>		                    
		    </div>
		</div>

		<div class="fullwidth-section text-light" style="background: #1a1a1a;">
            <div class="fullwidth-content">
                
                <div class="wrapper-small align-center">
                    <!-- <h5 class="subtitle-2">Our Clients</h5> -->
                    <hr class="small fat colored">
                    <h2><strong>¿QUÉ MÁS SE PUEDE PEDIR?</strong></h2>
                    <h3><span class="texthyper" style="font-size:100%">Somos como vosotros, pensamos como vosotros, vivimos como vosotros</span></h3>
                </div>
                
                <div class="spacer-big"></div>
                
                <div class="isotope-grid gallery-container style-column-4 clearfix">
                    [foreach:galeria_actividades]
                    <div class="isotope-item">
                        <a href="[foto]" class="thumb-overlay" data-rel="lightcase:gallery1">
                            <img src="[foto]" alt="Galeria [foto]"/>
                        </a>
                    </div>
                    [/foreach]
                </div>
            </div>
        </div>
        <div>[contacto]</div>
        <div class="spacer-big"></div>
        <div>[footer]</div>
    </section>