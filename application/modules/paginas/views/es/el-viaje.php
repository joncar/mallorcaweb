<!-- HEADER -->
    <header id="header" class="header-transparent transparent-light">               
        [menu]                      
    </header>
	<!-- HERO  -->
	<section id="hero" class="hero-big parallax-section" data-parallax-image="[base_url]theme/theme/files/uploads/hero-about-3.jpg">
    	
        <div id="page-title" class="wrapper title-bottom align-center">
        	<h4>Hello, I'm Jhon Florence</h4>
            <h5>Designer and founder of Sudo</h5>
        </div> <!-- END #page-title -->
            
    </section>
    <!-- HERO -->

    <!-- PAGEBODY -->
	<section id="page-body" class="notoppadding">
        
        <div class="column-section boxed-sticky adapt-height clearfix">
        	<div class="column one-half bigpadding has-animation" style="background:#ffffff;">
            	<h5 class="subtitle-2">We are Sudo</h5>
                <hr class="zigzag mini colored">
                <h2><strong>Creativity & reliability are our strongest power.</strong></h2>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth. Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life.</p>
                <p>It wasn't a dream. His room, a proper human room although a little too small, lay peacefully between its four familiar walls. A collection of textile samples lay spread out on the table - Samsa was a travelling salesman - and above it there hung a picture that he had recently cut out of an illustrated magazine and housed in a nice, gilded frame.
                </p>
            </div>
            <div class="column one-half last-col has-animation" data-delay="200" style="background:url([base_url]theme/theme/files/uploads/about-office.jpg) center center;background-size:cover;"></div>
        </div> <!-- END .column-section -->
        
        
        <div class="column-section boxed-sticky adapt-height clearfix">
            <div class="column one-half has-animation" data-delay="200" style="background:url([base_url]theme/theme/files/uploads/about-team-1.jpg) center center;background-size:cover;"></div>
        	<div class="column one-half bigpadding last-col has-animation" style="background:#ffffff;">
            	<h5 class="subtitle-2">Our Team</h5>
                <hr class="zigzag mini colored">
                <h2><strong>We think that design is not just a tool, it is a way of life.</strong></h2>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth. Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life.</p>
                <p>It wasn't a dream. His room, a proper human room although a little too small, lay peacefully between its four familiar walls. A collection of textile samples lay spread out on the table - Samsa was a travelling salesman - and above it there hung a picture that he had recently cut out of an illustrated magazine and housed in a nice, gilded frame. Samsa was a travelling salesman - and above it there hung a picture that he had recently cut out of an illustrated magazine and housed in a nice, gilded frame.
                </p>
            </div>
        </div> <!-- END .column-section -->
        <div>[contacto]</div>
        <div class="spacer-big"></div>
        <div>[footer]</div>
 	</section>
	<!-- PAGEBODY -->