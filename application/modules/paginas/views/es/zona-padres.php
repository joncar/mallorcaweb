<!-- HEADER -->
<header id="header" class="header-transparent transparent-light">
    [menu]
</header>
<!-- HERO  -->
<section id="hero" class="hero-full parallax-section text-light" data-parallax-image="[base_url]theme/theme/files/uploads/hero-about-42.jpg">
    
    <div id="page-title" class="wrapper align-center">
        <h4 class="subtitle-2">Información importante</h4>
        <hr class="small fat colored">
        <h1>Zona Padres</h1>
        </div> <!-- END #page-title -->
        <a href="#" id="scroll-down"></a>
        
    </section>
    <!-- HERO -->
    <section id="page-body" class="notoppadding">
        <div class="fullwidth-section text-light" style="background: #1a1a1a;">
            <div class="fullwidth-content">
                
                <div class="wrapper-small align-center">
                    <!-- <h5 class="subtitle-2"></h5> -->
                    <hr class="small fat colored">
                    <h4>El viaje fin de curso a Mallorca para los estudiantes de Bachiller es un producto registrado por el Grupo Finalia.<br>
                    Grupo Finalia desarrolla su actividad en el sector turístico con presencia en diferentes y variadas líneas de negocio. Nuestro amplio abanico de actividades dentro del sector nos permite no sólo estar diversificados sino alcanzar importantes sinergias y la posibilidad de ofrecer a nuestros clientes una gestión global de sus necesidades.<br>
                    El Grupo Finalia está formado por un conjunto de empresas independientes en sus respectivos sectores: estudiantes, estadas lingüísticas, eventos deportivos, turismo on-line, receptivos, aéreos, business travel y continental.<br>
                    Lo que hace diferente al Grupo Finalia, lo que le da sentido, músculo y estabilidad, es el hecho de que cada empresa respalda a las otras y es, a su vez, respaldada por las demás, haciendo un gran puzzle en el que todas las piezas encajan.<br>
                    Ahora que nos conocen un poco mejor, queremos comunicarles que estar más y mejor informados sobre las actividades que realizarán sus hijos, nos importa.<br>
                    A continuación os detallamos los servicios incluidos y que sus hijos disfrutarán durante el viaje:
                    </h4>
                </div>
            </div>
        </div>
        <div class="column-section boxed-sticky adapt-height clearfix">
            <div class="column bigpadding text-light one-half" style="background: #e6007e;">
                <!-- <h6 class="subtitle-2">Our Story</h6> -->
                <h3 class="uppercase">SERVICIOS INCLUIDOS</h3>
                <h4 class="subtitle-1">TRANSPORTE IDA Y VUELTA</h4>
                <p>BARCO ACCIONA TRASMEDITERRANEA/BALEARIA<br>
                    • Fiesta bienvenida en medio del mar.<br>
                    • Transporte de equipaje ilimitado sin restricciones de peso ni volumen.<br>
                • Mejor acomodación disponible.</p>
                <h4 class="subtitle-1">TRASLADOS</h4>
                <p>AUTOBUSES<br>
                    • Traslado autobús - Localidad / Puerto.<br>
                    • Traslado autobús - Puerto Mallorca / Resort, ida y regreso.
                    <h4 class="subtitle-1">ALOJAMIENTO</h4>
                    <p>RESORT BELLEVUE CLUB ***<br>
                        • Apartamentos en distribución múltiple.<br>
                        • Cóctel de presentación a la llegada al resort.<br>
                        • Monólogo presentación + cóctel de bienvenida a la llegada al resort.
                        <h4 class="subtitle-1">RÉGIMEN</h4>
                        <p>PENSIÓN COMPLETA + BEBIDA<br>
                            • Desayuno, comida y cena en restaurante del buffet libre del resort.
                            <h4 class="subtitle-1">SEGUROS</h4>
                            <p> El seguro que mejor se adapta al colectivo de estudiantes.<br>
                                • Asistencia médica y sanitaria en viaje. Seguro estándar.
                                <h4 class="subtitle-1">ACTIVIDADES INCLUIDAS</h4>
                                <p>Organizadas por nuestro equipo técnico en Mallorca.<br>
                                    • Gratis partida de Paintball para todo el grupo.<br>
                                    • Inscripción gratuita a los torneos deportivos en la playa.<br>
                                    • Inscripción gratuita a los torneos acuáticos (Pool Sports) en las piscinas del resort.<br>
                                    • 1 hora gratis de las pistas de tenis, fútbol, pádel y minigolf para cada pasajero en el resort.<br>
                                    • Pool parties con barra libre de cócteles en las piscinas del resort.<br>
                                    • Crazy Games (Miss/Míster MIF) en las piscinas del resort.<br>
                                    • Toro mecánico e hinchables (futbolín humano, gladiadores, etc.) en las piscinas resort.<br>
                                    • Videos aftermovie & fotógrafo del evento.<br>
                                    • Regalos sorpresa durante el evento.<br>
                                    • Conciertos FLAIX FM y FLAIXBAC en el resort.
                                    <h4 class="subtitle-1">STAFF EN ORIGEN</h4>
                                    <p>Un equipo formado por 12 miembros de la organización.<br>
                                        • Les darán la bienvenida a sus hijos en el instituto con el autobús para acompañarles al Puerto.
                                        <h4 class="subtitle-1">STAFF EN MALLORCA</h4>
                                        <p>Un equipo formado por más de 50 miembros de la organización.<br>
                                            • Les darán la bienvenida a sus hijos a la llegada a Mallorca y se encargarán que toda la organización del viaje se cumpla a la perfección.
                                            <h4 class="subtitle-1">AYUDA CON EL VIAJE</h4>
                                            <p>Material “VIAJA GRATIS”.<br>
                                                • Somos conscientes que la época que nos toca vivir implica grandes esfuerzos por parte de todos. Por ello queremos ayudar a los estudiantes y familias a que su viaje de fin de estudios no se convierta en un contratiempo con diferentes alternativas para que su viaje les resulte lo más económico posible.
                                                <h4 class="subtitle-1">OTROS GASTOS</h4>
                                                <p>EQUIPAJE, IMPUESTOS Y TASAS<br>
                                                    • El equipaje necesario para el viaje (sin restricciones), los impuestos y las tasas portuarias están incluidos en el precio.
                                                    
                                                </p>
                                                <h3 class="uppercase">SEGUROS</h3>
                                                
                                                <p>Tener un buen seguro es síntoma de tranquilidad. Por ello trabajamos con las compañías aseguradoras líderes en el sector colectivo de estudiantes.<br><br>
                                                    
                                                    En este punto le confirmamos que en el viaje está incluido el seguro obligatorio básico/ESTÁNDAR de asistencia médica y sanitaria, con el que el pasajero tiene incluido:<br><br>
                                                    ● Asistencia obligatoria médica en España.<br>
● Seguro de Responsabilidad Civil 3.000 €<br>
● Teléfono de incidencias: 24 HORAS<br><br>
No obstante, si desea ampliar las coberturas aseguradas le informamos que tenemos un acuerdo de colaboración con el que podrá contratar el seguro PLUS ESTUDIANTES dónde se amplían las coberturas en asistencia médica y sanitaria durante el viaje, con el que dispondrá de:<br>
                                                  
                                                    ● Asistencia médica y sanitaria en España: 601,01€<br>
                                                    ● Repatriación o transporte de heridos y/o enfermos: ILIMITADO<br>
                                                    ● Repatriación de acompañante INCLUIDO<br>
                                                    ● Desplazamiento de un familiar en caso de hospitalización superior a cinco días: ILIMITADO<br>
                                                    ● Convalecencia en hotel. Máximo 10 días. 30€ por día<br>
                                                    ● Repatriación o transporte del asegurado fallecido. ILIMITADO<br>
                                                    ● Búsqueda, localización y envío de equipaje extraviado ILIMITADO<br>
                                                    ● Robo y daños materiales del equipaje 150,25 €<br>
                                                    ● Transmisión de mensajes urgentes ILIMITADO<br>
                                                    ● Seguro de Equipajes AXA 150 €<br>
                                                    ● Seguro de Responsabilidad Civil AXA 6.000 €<br>
                                                    ● Regreso anticipado por fallecimiento u hospitalización superior a dos días de un familiar de hasta 2º grado. ILIMITADO<br>
                                                    ● Teléfono de incidencias: 24 HORAS<br><br>
                                                    
                                                    Coberturas incluidas en:     (E) Seguro Básico Estándar (incluido en el viaje)      (P) Seguro Plus Estudiantes (opcional)
                                                    
                                                    
                                                </div>
                                                <div class="column bigpadding text-light one-half last-col" style="background: rgba(119,119,119,1);">
                                                    <h3 class="uppercase">TRASLADOS</h3>
                                                    <p>Sabemos que lo que más cuesta a los estudiantes cuando intentan organizarse entre ellos a la hora de buscar su viaje de fin de estudios es cómo llegar a los destinos (horarios, puntos de encuentro, autobuses, aviones, barcos, equipajes, bonos de reserva, documentación, traslados, etc.).
                                                        Es en este punto dónde hacemos especial relevancia al trabajo de nuestro equipo técnico.<br><br>
                                                        El viaje se iniciará como punto de encuentro en la localidad de su hijo, dónde nuestro autobús, acompañado de nuestro staff, recogerá a todo su grupo el día de salida a la hora establecida.
                                                        <br><br>Al subir al autobús nuestro equipo le dará la bienvenida al grupo entregándole la documentación del viaje, pulsera y dosier de información general durante el trayecto hasta el Puerto.
                                                        <br><br>Una vez estacione el autobús en el puerto, todos los grupos que inicien la salida ese mismo día únicamente tendrán que seguir las indicaciones de nuestro staff procediendo al embarque, así de sencillo, para los estudiantes y para la organización.
                                                        <br><br>En el trayecto en barco, sus hijos podrán disfrutar de todas las instalaciones y servicios que dispone el buque hasta llegar al Puerto de Palma de Mallorca, lugar dónde nuevamente nuestro equipo les estará esperando con los autobuses para guiarles hasta el Resort.
                                                        <br><br>Cómo decíamos al principio, los traslados son así de sencillos. Los estudiantes y sus padres no deben preocuparse de nada durante los trayectos ya que en todo momento sus hijos podrán realizar directamente cualquier consulta que puedan tener a nuestros miembros de la organización.
                                                        <br><br>
                                                        <h3 class="uppercase">ACTIVIDADES EN DESTINO</h3>
                                                        <p>Durante la estancia de sus hijos nuestra organización tiene preparadas un sinfín de actividades para todos los grupos con el fin de hacer de su viaje de fin de estudios una experiencia inolvidable.
                                                            <br><br>El primer día a la llegada al resort, nuestro staff realizará una presentación general a los grupos en el Show Garden del Resort para poderles situar en el mapa, explicarles las actividades, excursiones y ocio que podrán disfrutar durante sus estancias y facilitarles consejos e información práctica a tener en cuenta, como listados telefónicos, puntos de encuentro, etc.
                                                            <br><br>Todas las actividades son voluntarias y nuestro equipo técnico y staff se encargará de que todos los estudiantes puedan disfrutar de la combinación de eventos deportivos, conciertos, excursiones, playas y sol.
                                                            <h3 class="uppercase">ALOJAMIENTO</h3>
                                                            <p>Todos los estudiantes se alojaran en apartamentos completamente equipados con salón tv, cocina completa, baño privado, terraza y sus habitaciones correspondientes. Todos los apartamentos tienen más de 35 m2 y están situados dentro del mismo Resort.
                                                                <br><br>Como en el viaje está incluida la Pensión Completa, no se tendrán que preocupar de nada, ya que el resort dispone de un restaurante buffet libre para que todos los estudiantes puedan desayunar, comer y cenar. A parte, en el viaje también están incluidos los refrescos en el mismo restaurante por lo que no tendrán que tener un gasto adicional.
                                                                <h3 class="uppercase">EL RESORT</h3>
                                                                <p>El Resort Bellevue Club se encuentra ubicado a escasos metros de la playa de Alcudia, una de las más paradisíacas de Mallorca.
                                                                    <br><br>El Resort, siendo el más grande de Europa, está construido para garantizar el entretenimiento de todos sus visitantes. Tiene un lago natural que conecta con el mar y nueve piscinas de diferentes tamaños en una zona deportiva con pistas de tenis, minigolf, squash y circuito de karts. La diversión no termina ahí; el Pueblo Español es un área recreativa cuya zona central imita a un pueblo andaluz, en el que hay de todo: cafetería, pizzería, restaurante-buffet, salón de juegos, supermercados, joyerías y salón de espectáculos para 2500 personas.
                                                                    <h3 class="uppercase">ENCANTADOS DE ATENDERLE</h3>
                                                                    <p>Si le ha quedado alguna consulta al respecto o desea que le ampliemos la información de algún punto en concreto respecto al viaje de su hijo, no dude en llamar a nuestras operadoras quiénes estarán encantadas de atenderle:
                                                                        
                                                                        <br><br> <strong>TELÉFONO EXCLUSIVO PADRES Y MADRES:  902 002 068 </strong>
                                                                        <br>Horario de Atención al Cliente:  Lunes a Viernes de 08:00 a 21:00h.
                                                                        
                                                                        
                                                                        <br><br> <strong>CORREO ELECTRÓNICO</strong>
                                                                        <br>info@mallorcaislandfestival.com
                                                                        <br>reservas@mallorcaislandfestival.com
                                                                        
                                                                        <br><br> <strong>CHAT WEB</strong>
                                                                        <br>www.mallorcaislandfestival.com
                                                                        <br>Lunes a Viernes de 08:00 a 21:00h.
                                                                        
                                                                        <br><br> <strong>DIRECCIÓN</strong>
                                                                        <br>C/ Girona, 34. 08700 Igualada.
                                                                        <img src="[base_url]theme/theme/files/uploads/medical-team-3.jpg">
                                                                        
                                                                    </div>
                                                                    </div> <!-- END .column-section -->
                                                                    <div>[contacto]</div>
                                                                    <div class="spacer-big"></div>
                                                                    <div>[footer]</div>
                                                                </section>