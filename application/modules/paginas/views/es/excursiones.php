<!-- HEADER -->
    <header id="header" class="header-transparent transparent-light">               
        [menu]                      
    </header>
	<!-- HERO  -->
     <section id="hero" class="hero-full parallax-section text-light" data-parallax-image="[base_url]theme/theme/files/uploads/hero-about-38.jpg">
    
        <div id="page-title" class="wrapper align-center">
            <h4 class="subtitle-2">Disfruta de las mejores visitas a la isla</h4>
            <hr class="small fat colored">
            <h1>Excursiones</h1>
        </div> <!-- END #page-title -->
        <a href="#" id="scroll-down"></a>
            
    </section>

    <!-- PAGEBODY -->
	<section id="page-body" class="notoppadding">
		<div class="column-section boxed-sticky adapt-height clearfix">
        	<div class="column one-half bigpadding has-animation tallest animated" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 609px;"><div class="col-content" style="margin-top: 0px;">
            	<h5 class="subtitle-2">Beach. Boat Party</h5>
                <hr class="small fat colored">
                <h1 class="texthyper">Formentor</h1>
                <p>Disfruta con la mejor música hasta la famosa playa de Formentor y sus aguas cristalinas dónde se rodó el anuncio de "Estrella Damm". <br>Incluye 1 consumición
                </p>
            </div></div>
            <div class="column one-half last-col has-animation empty-content animated" data-delay="200" style="background: rgba(0, 0, 0, 0) url([base_url]theme/theme/files/uploads/about-office.jpg) repeat scroll center center / cover; min-height: 609px;"></div>
        </div>

        <div class="column-section boxed-sticky adapt-height clearfix">
            <div class="hidden-xs column one-half has-animation empty-content animated" data-delay="200" style="background: rgba(0, 0, 0, 0) url([base_url]theme/theme/files/uploads/about-team-12.jpg) repeat scroll center center / cover; min-height: 661px;"></div>
        	<div class="column one-half bigpadding last-col has-animation tallest animated" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 661px;"><div class="col-content" style="margin-top: 0px;">
            	<h5 class="subtitle-2">Parque acuático</h5>
                <hr class="small fat colored">
                <h1 class="texthyper">Sunland</h2>
                <p>Fiesta en la piscina de olas en un parque acuático con los mejores Dj's. <br>Incluye 50% Dto en bebidas en el parque.</p>
            </div></div>
            <div class="visible-xs column one-half has-animation empty-content animated" data-delay="200" style="background: rgba(0, 0, 0, 0) url([base_url]theme/theme/files/uploads/about-team-12.jpg) repeat scroll center center / cover; min-height: 661px;"></div>
        </div>
        
        <div class="column-section boxed-sticky adapt-height clearfix">
        	<div class="column one-half bigpadding has-animation tallest animated" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 609px;"><div class="col-content" style="margin-top: 0px;">
            	<h5 class="subtitle-2">Visita</h5>
                <hr class="small fat colored">
                <h1 class="texthyper">Visita Palma</h2>
                <p>Coves Drach & Playa, panorámica de la ciudad visitando las cuevas más famosas de Mallorca. <br> Incluye parada en playa con aguas cristalinas.</p>
            </div></div>
            <div class="column one-half last-col has-animation empty-content animated" data-delay="200" style="background: rgba(0, 0, 0, 0) url([base_url]theme/theme/files/uploads/about-office-22.jpg) repeat scroll center center / cover; min-height: 609px;"></div>
        </div>

        <div class="column-section boxed-sticky adapt-height clearfix">
            <div class="hidden-xs column one-half has-animation empty-content animated" data-delay="200" style="background: rgba(0, 0, 0, 0) url([base_url]theme/theme/files/uploads/about-team-13.jpg) repeat scroll center center / cover; min-height: 661px;"></div>
        	<div class="column one-half bigpadding last-col has-animation tallest animated" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 661px;"><div class="col-content" style="margin-top: 0px;">
            	<h5 class="subtitle-2">Excursión de 1 día</h5>
                <hr class="small fat colored">
                <h1 class="texthyper">Menorca o Ibiza</h2>
                <p>Salida por la mañana en barco para pasar un magnífico día y regreso al atardecer.</p>
            </div></div>
            <div class="visible-xs column one-half has-animation empty-content animated" data-delay="200" style="background: rgba(0, 0, 0, 0) url([base_url]theme/theme/files/uploads/about-team-13.jpg) repeat scroll center center / cover; min-height: 661px;"></div>
        </div>

        <div class="fullwidth-section text-light" style="background: #1a1a1a;">
            <div class="fullwidth-content">
                
                <div class="wrapper-small align-center">
                    <!-- <h5 class="subtitle-2">Our Clients</h5> -->
                    <hr class="small fat colored">
                    <h2><strong>¿QUÉ MÁS SE PUEDE PEDIR?</strong></h2>
                    <h3><span class="texthyper" style="font-size:100%">Somos como vosotros, pensamos como vosotros, vivimos como vosotros</span></h3>
                </div>
                
                <div class="spacer-big"></div>
                
                <div class="isotope-grid gallery-container style-column-4 clearfix">
                    [foreach:galeria_excursiones]
                    <div class="isotope-item">
                        <a href="[foto]" class="thumb-overlay" data-rel="lightcase:gallery1">
                            <img src="[foto]" alt="Galeria [foto]"/>
                        </a>
                    </div>
                    [/foreach]
                </div>
            </div>
        </div>
        <div>[contacto]</div>
        <div class="spacer-big"></div>
        <div>[footer]</div>
	</section>