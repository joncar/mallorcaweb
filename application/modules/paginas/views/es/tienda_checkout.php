<!-- HEADER -->
    <header id="header" class="header-transparent transparent-light">               
        [menu]                      
    </header>
<!-- HERO  -->
<!-- HERO  -->
	<section id="hero" class="hero-auto" style="background:#f5f6f7;">
    	
        <div id="page-title" class="wrapper align-center">
            <h2 class="uppercase"><strong>Checkout</strong></h2>
        </div> <!-- END #page-title -->
            
    </section>
    <!-- HERO -->
<!-- PAGEBODY -->
	<section id="page-body">
    
        <div id="shop-checkout" class="wrapper-small">
            <form id="checkout" name="checkout" action="#" method="post">
            
                <h4>Billing Details</h4>
                <div class="billing-details">
                    <div class="form-row column one-half">
                        <label for="billing-firstname">First Name <abbr title="required" class="required">*</abbr></label>
                        <input type="text" id="billing-firstname" name="billing-firstname" value="" />
                    </div>
                    
                    <div class="form-row column one-half last-col">
                        <label for="billing-lastname">Last Name <abbr title="required" class="required">*</abbr></label>
                        <input type="text" id="billing-lastname" name="billing-lastname" value="" />
                    </div>
                    
                    <div class="clear"></div>
                    
                    <div class="form-row">
                        <label for="billing-company">Company Name</label>
                        <input type="text" id="billing-company" name="billing-company" value="" />
                    </div>
                    
                    <div class="form-row">
                        <label for="billing-adress">Adress <abbr title="required" class="required">*</abbr></label>
                        <input type="text" id="billing-adress" name="billing-adress" value="" placeholder="Street address" />
                    </div>
                    
                    <div class="form-row">
                        <input type="text" id="billing-adress-2" name="billing-adress-2" value="" placeholder="Apartment, suite, unit etc. (optional)" />
                    </div>
                                    
                    <div class="form-row column one-half">
                        <label for="billing-postcode">Postcode / Zip <abbr title="required" class="required">*</abbr></label>
                        <input type="text" id="billing-postcode" name="billing-postcode" value="" />
                    </div>
                    
                    <div class="form-row column one-half last-col">
                        <label for="billing-city">City / Town <abbr title="required" class="required">*</abbr></label>
                        <input type="text" id="billing-city" name="billing-city" value="" />
                    </div>
                    
                    <div class="clear"></div>
                    
                    <div class="form-row">
                        <label for="billing-country">Country <abbr title="required" class="required">*</abbr></label>
                        <select id="billing-country" name="billing-country" class="full-width">
                            <option>Unidet States (US)</option>
                        </select>
                    </div>
                    
                    <div class="form-row column one-half">
                        <label for="billing-email">Email Adress <abbr title="required" class="required">*</abbr></label>
                        <input type="text" id="billing-email" name="billing-email" value="" />
                    </div>
                    
                    <div class="form-row column one-half last-col">
                        <label for="billing-phone">Phone <abbr title="required" class="required">*</abbr></label>
                        <input type="text" id="billing-phone" name="billing-phone" value="" />
                    </div>
                    
                    <div class="clear"></div>
                </div> <!-- END .billing-details -->	
                
                <div class="spacer-big"></div>
                
                <h4>Your Order</h4>
                <div class="cart-total">
                    <table class="table-cart">
                        <tbody>
                            <tr>
                                <td><h6 class="uppercase">Cart Subtotal</h6></td>
                                <td><span class="product-price">$243.97</span></td>
                            </tr>
                            <tr>
                                <td><h6 class="uppercase">Shipping</h6></td>
                                <td><span class="product-price">Free Delivery</span></td>
                            </tr>
                            <tr class="total">
                                <td><h5 class="uppercase">Total</h5></td>
                                <td><span class="product-price">$243.97</span></td>
                            </tr>
                        </tbody>
                    </table>
                </div> <!-- END .cart-total -->	
                
                <div class="spacer-medium"></div>
                
                <h4>Payment Option</h4>
                <div class="payment-option">
                    <div class="form-row">
						<input type="radio" value="banktransfer" name="payment_method" id="payment_method_banktransfer">
                        <label>Direct Bank Transfer</label>
                    </div>
                    <div class="form-row">
						<input type="radio" value="cheque" name="payment_method" id="payment_method_cheque">
                        <label>Cheque Payment </label>
                    </div>
                    <div class="form-row">
						<input type="radio" value="paypal" name="payment_method" id="payment_method_paypal">
                        <label>Paypal</label>
                    </div>
                </div>
                
                <div class="form-row">
                    <input type="submit" value="Place order" id="place_order" name="place_order">
                </div>	
            
           	</form>
        
        </div> <!-- END .wrapper -->
        
        <div class="spacer-big"></div>
       <div>[contacto]</div>
        <div class="spacer-big"></div>
        <div>[footer]</div>         
 	</section>
	<!-- PAGEBODY -->