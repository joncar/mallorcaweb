<!-- HEADER -->
<header id="header" class="header-transparent transparent-light">
	[menu]
</header>
<!-- HERO  -->
<section id="hero" class="hero-full parallax-section text-light" data-parallax-image="[base_url]theme/theme/files/uploads/hero-about-33.jpg">
	
	<div id="page-title" class="wrapper align-center">
		<h4 class="subtitle-2">Nuestro Equipo</h4>
		<hr class="small fat colored">
		<h1>Todo un grupo de profesionales a tu lado</h1>
		</div> <!-- END #page-title -->
		<a href="#" id="scroll-down"></a>
		
	</section>
	<!-- HERO -->
	<section id="page-body" class="notoppadding">
		<div class="column-section boxed-sticky adapt-height clearfix">
			<div class="column one-half has-animation empty-content animated" data-delay="200" style="background: rgba(0, 0, 0, 0) url([base_url]theme/theme/files/uploads/about-team-11.jpg) repeat scroll center center / cover; min-height: 661px;"></div>
			<div class="column one-half bigpadding last-col has-animation tallest animated" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 661px;"><div class="col-content" style="margin-top: 0px;">
				<!-- <h5 class="subtitle-2">Our Team</h5> -->
				<hr class="small fat colored">
				<h4><strong>¿Quién te acompaña en tu viaje de fin de curso?</strong></h4>
				<p>Nuestra división MALLORCA ISLAND FESTIVAL® (MIF), la compone un amplio grupo de profesionales que harán de vuestro viaje de fin de curso la mayor experiencia de vuestras vidas.</p>
				<p>Todo nuestro equipo, conoce el destino que os proponemos a la perfección, lo que hará que podamos resolver todas vuestras dudas o inquietudes acerca de cualquiera tema.
				</p>
				<p>Nuestros agentes irán directamente a vuestro instituto para conoceros si lo necesitáis, y ofreceros todas nuestras opciones para que podáis resolver todas vuestras dudas acerca de vuestro viaje de fin de curso.</p>
				<p>Ese agente será vuestro mejor amigo hasta la vuelta de la aventura, pues estará con vosotros desde el primer momento, asesorando, gestionando, y resolviendo las dudas que le puedan surgir al grupo.</p>
				<p>Queremos ponéroslo muy fácil.<br>
					
					Tanto es así, que en MALLORCA ISLAND FESTIVAL® (MIF), nos ocuparemos de todo para que vuestro viaje de fin de curso salga a la perfección:
					<br>• Información de reserva de vuestro viaje.
					<br>• Gestión de Rooming-List / Check-in & Check-out.
					<br>• Traslados y embarques.
					<br>• Alojamientos y actividades.
					<br>• Organización de fiestas y excursiones.
					<br>• Asistencia en origen y destino.
					<br>• Gestión y producción de merchandising y sorteos.
				<br>Y un largo etc, que hará de vuestro viaje una experiencia inolvidable.</p>
			</div></div>
		</div>
		<div class="fullwidth-section text-light" style="background: #1a1a1a;">
			<div class="fullwidth-content">
				
				<div class="wrapper-small align-center">
					<!-- <h5 class="subtitle-2">Our Clients</h5> -->
					<hr class="small fat colored">
					<h2><strong>¿QUÉ MÁS SE PUEDE PEDIR?</strong></h2>
					<h3><span class="texthyper" style="font-size:100%">Somos como vosotros, pensamos como vosotros, vivimos como vosotros</span></h3>
				</div>
				
				<div class="spacer-big"></div>
				
				<div class="isotope-grid gallery-container style-column-4 clearfix">
					[foreach:galeria_equipo]
					<div class="isotope-item">
						<a href="[foto]" class="thumb-overlay" data-rel="lightcase:gallery1">
							<img src="[foto]" alt="Galeria [foto]"/>
						</a>
					</div>
					[/foreach]
				</div>
			</div>
		</div>
		<div>[contacto]</div>
		<div class="spacer-big"></div>
		<div>[footer]</div>
	</section>