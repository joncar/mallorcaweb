<!-- HEADER -->
<header id="header" class="header-transparent transparent-light">
    [menu]
</header>
<!-- HERO  -->
<section id="hero" class="hero-full parallax-section text-light" data-parallax-image="[base_url]theme/theme/files/uploads/hero-about-37.jpg">
    
    <div id="page-title" class="wrapper align-center">
        <h4 class="subtitle-2">El Resort más grande de europa</h4>
        <hr class="small fat colored">
        <h1>Bellevue Club***</h1>
        </div> <!-- END #page-title -->
        <a href="#" id="scroll-down"></a>
        
    </section>
    <!-- HERO -->
    <section id="page-body" class="notoppadding">
        <div class="fullwidth-section text-light parallax-section" data-parallax-image="[base_url]theme/theme/files/uploads/parallax3.jpg">
            <div class="fullwidth-content">
                
                <div class="wrapper-small align-center">
                    <!-- <h5 class="subtitle-2">Our Clients</h5> -->
                    
                    <h2><strong>250.000 M2 • PLAYAS PARADISÍACAS • 9 PISCINAS  TENIS • FÚTBOL •
                    MINIGOLF SQUASH •
                    CIRCUITO DE KARTS
                    ALQUILER DE BICICLETAS
                    SUPERMERCADOS
                    ESCENARIO DE CONCIERTOS
                    AL AIRE LIBRE PARA MÁS
                    DE 2000 PERSONAS
                    KARAOKE • CLUB CHILL-OUT</strong></h2>
                </div>
            </div>
        </div>
        <div class="spacer-big"></div>
        <div class="wrapper">
            
            <div class="isotope-grid gallery-container style-column-4 clearfix">
                [foreach:galeria_resort]
                <div class="isotope-item">
                    <a href="[foto]" class="thumb-overlay" data-rel="lightcase:gallery1">
                        <img src="[foto]" alt="SEO Name"/>
                    </a>
                </div>
                [/foreach]
            </div>
            
        </div>
        <div class="spacer-big"></div>
        <div class="fullwidth-section text-light parallax-section" data-parallax-image="[base_url]theme/theme/files/uploads/parallax4.jpg">
            <div class="fullwidth-content">
                
                <div class="wrapper-small align-center">
                    <!--   <h5 class="subtitle-2">Our Clients</h5> -->
                    <hr class="small fat colored">
                    <h2><strong>Todos los apartamentos tienen: Salón TV • Cocina • Baño privado • Habitaciones y Terraza</strong></h2>
                </div>
            </div>
        </div>
        <div class="fullwidth-section text-light videobg-section"
        data-videotype="youtube" 
        data-videoyoutubeid="kI01D7rgLzc"
        data-videoratio="16/9"
        data-videoloop="true"                        
        data-videoposter="<?= base_url() ?>theme/theme/files/uploads/900x600-dark.jpg"
        data-videooverlaycolor="#000000"
        data-videooverlayopacity="0.7">
            <div class="fullwidth-content wrapper align-center">
                
                <h2><strong>El resort Bellevue Club***</strong></h2>
                <hr class="small fat colored">
                <h5 class="subtitle-1">VISITA UN APARTAMENTO</h5>
                <div class="spacer-medium"></div>
                <a class="sr-button small-button button-4 rounded" href="https://www.youtube.com/embed/kI01D7rgLzc?rel=0" data-rel="lightcase">Ver Video</a>
                
            </div>
        </div>
        <div>[contacto]</div>
        <div class="spacer-big"></div>
        <div>[footer]</div>
    </section>