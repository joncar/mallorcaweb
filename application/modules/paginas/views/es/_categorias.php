<?php     
    foreach($this->db->get_where('categorias')->result() as $c): 
    $enlace = base_url().'store/categorias/'.toUrl($c->id.'-'.$c->nombre_categoria);
    if($c->reembolso_admin==1 && (empty($_SESSION['user']) || $_SESSION['admin']==0)){
        $enlace = 'javascript:;';
    }
    if($c->disponible==1):        
?>
    	<div class="column one-third nopadding">

    		<a rel="canonical" href="<?= $enlace ?>" class="thumb-overlay overlay-effect-2 text-light">
                <img src="<?= base_url('img/fotos_categorias/'.$c->foto) ?>" alt="<?= $c->nombre_categoria ?>">
                <img src="<?= base_url('img/fotos_categorias/'.$c->foto) ?>" class="hover-pic" alt="<?= $c->nombre_categoria ?>">
                <div class="overlay-caption">
                    <h6 class="caption-sub portfolio-category subtitle-2">
                        <img src="<?= base_url('img/fotos_categorias/'.$c->icono) ?>">
                    </h6>
                    <hr class="zigzag">
                    <h4 class="caption-name portfolio-name uppercase textshadow"><?= $c->nombre_categoria ?></h4>
                </div>
            </a>
    	</div>
    <?php endif ?>
<?php endforeach ?>