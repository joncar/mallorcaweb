<!-- HEADER -->
    <header id="header" class="header-transparent transparent-light">               
        [menu]                      
    </header>
    <!-- HERO  -->
<!-- HERO  -->
<!-- HERO  -->
	<section id="hero" class="hero-auto">
    	
        <div class="revolution-slider-wrapper">
            <div id="revolutionslider1" class="revolution-slider"  data-version="5.0">
                <ul>
                        
                    <!--<li class="text-light" data-transition="fade" data-masterspeed="700">			
                        <!-- MAIN IMAGE -->
                        <!--<img src="[base_url]theme/theme/files/uploads/hero-shop-2.jpg"  alt=""  width="1920" height="1280">                        
                                            
                    </li> <!-- END first slide -->
                    
                    <li class="text-light" data-transition="fade" data-masterspeed="700">			
                        <!-- MAIN IMAGE -->
                        <img src="[base_url]theme/theme/files/uploads/transparent.jpg" style="background-color:#e03936" alt=""  width="1920" height="1280">
                     
                        <!-- LAYER NR. 3 -->
                        <!--<div class="tp-caption align-center" 						
                            data-x="center" data-hoffset="0" 
                            data-y="center" data-voffset="200"
                            data-width="full" 							
                            data-whitespace="normal"
                            data-transform_idle="o:1;" 
                            data-transform_in="y:50px;opacity:0;s:1000;e:Power3.easeOut;" 
                            data-transform_out="opacity:0" 							 
                            data-start="600">
                            <h2 class="uppercase"><strong>up to 80%</strong></h2>
                        </div>
                        
                        <!-- LAYER NR. 3 -->
                        <!--<div class="tp-caption align-center" 						
                            data-x="center" data-hoffset="0" 
                            data-y="center" data-voffset="0"
                            data-width="auto" 							
                            data-whitespace="normal"
                            data-transform_idle="o:1;" 
                            data-transform_in="y:50px;opacity:0;s:1000;e:Power3.easeOut;" 
                            data-transform_out="opacity:0" 							 
                            data-start="400">                            
                        </div>
                                            
                    </li> <!-- END first slide -->
                    
              	</ul>				
             </div><!-- END .revolution-slider -->
         </div> <!-- END .revolution-slider-wrapper -->
        <a href="#" id="scroll-down" style="color:white; border:2px solid white"></a>
    </section>
    <!-- HERO -->
    
            
	<!-- PAGEBODY -->
	<section id="page-body" class="notoppadding">
    
        <div class="column-section boxed-sticky clearfix">
            [categorias]                        
        </div>
        
        <div>[contacto]</div>
        <div class="spacer-big"></div>
        <div>[footer]</div>
        
 	</section>
	<!-- PAGEBODY -->