<!-- HEADER -->
    <header id="header" class="header-transparent transparent-light">               
        [menu]                      
    </header>
	<!-- HERO  -->
<!-- HERO  -->

<!-- HERO  -->
	<section id="hero" class="hero-auto parallax-section text-light" data-parallax-image="[base_url]theme/theme/files/uploads/parallax2.jpg">
        
        <div id="page-title" class="wrapper align-center">
            <h2 class="uppercase"><strong>Cuenta</strong></h2>
        </div> <!-- END #page-title -->
            
    </section>
    
            
	<!-- PAGEBODY -->
	<section id="page-body">
    
    	<div class="wrapper">
        	

			<div class="tabs vertical-tabs">
                <ul class="tab-nav clearfix">
                    <li data-tab="cuenta">
                    	<h6 class="tab-name uppercase">
                    	<a rel="canonical" href="[base_url]cuenta">Mis compras</a></h6>
                    </li>
                    <li data-tab="perfil">
                    	<h6 class="tab-name uppercase">
                    	<a rel="canonical" href="[base_url]t/admin/perfil/edit/[user_id]">Datos de usuario</a></h6>
                    </li>
                </ul>
                <div class="tab-container clearfix">
                    <div class="tab-content tab1" style="display: block">
                        [output]
                    </div> <!-- END .tab-content -->
                </div> <!-- END .tab-container -->  
            </div>
            
        </div> <!-- END .wrapper-small -->
                
        <div class="spacer-big"></div>		
		<div>[footer]</div>
 	</section>
	<!-- PAGEBODY -->