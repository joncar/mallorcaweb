<!-- HEADER -->
    <header id="header" class="header-transparent transparent-light">               
        [menu]                      
    </header>
	<!-- HERO  -->
<!-- HERO  -->
    <section id="hero" class="hero-full parallax-section text-light" data-parallax-image="[base_url]theme/theme/files/uploads/hero-about-galeria.jpg">
    
    <div id="page-title" class="wrapper align-center">
        <h4 class="subtitle-2">Famosos, Artistas, DJ'S e invitados de MIF</h4>
        <hr class="small fat colored">
        <h1>Feedback</h1>
        </div> <!-- END #page-title -->
        <a href="#" id="scroll-down"></a>
        
    </section>
    <!-- HERO -->
	
    <!-- PAGEBODY -->
	<section id="page-body" class="notoppadding">
    <div class="fullwidth-section text-light" style="background: #1a1a1a;">
        <div class="fullwidth-content">
            <!--<ul id="portfolio-filter" class="filter text-light" data-related-grid="portfolio-grid">
                <li class="active" ><a href="#" data-filter=".all">Todas</a></li>
                <li><a href="#" data-filter=".facebook">Facebook</a></li>                
                <li><a href="#" data-filter=".instagram">Instagram</a></li>                
            </ul>-->
            <div class="spacer-small"></div>

            <div id="portfolio-grid" class="isotope-grid gallery-container style-column-4 clearfix">
                <?php foreach($this->db->get_where('feedback')->result() as $r): ?>
                <div class="isotope-item dj all <?= $r->tipo_red ?>" style="text-align: center; height:40vh">
                    <a href="<?= base_url('img/feedback/'.$r->feedback) ?>" class="thumb-overlay" data-rel="lightcase:gallery1">
                        <img src="<?= base_url('img/feedback/'.$r->feedback) ?>" alt="Feedback" style="width:100%"/>
                    </a>
                </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>

        
    <div>[contacto]</div>
    <div class="spacer-big"></div>
    <div>[footer]</div>
	</section>
<!-- PAGEBODY -->