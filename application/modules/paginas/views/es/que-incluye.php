<!-- HEADER -->
<header id="header" class="header-transparent transparent-light">
    [menu]
</header>
<!-- HERO  -->
<section id="hero" class="hero-full parallax-section text-light" data-parallax-image="[base_url]theme/theme/files/uploads/hero-about-36.jpg">
    
    <div id="page-title" class="wrapper align-center">
        <h4 class="subtitle-2">¿Qué incluye?</h4>
        <hr class="small fat colored">
        <h1>Consigue tu viaje por tan solo 0 euros</h1>
        <h3>7 días y 6 noches</h3>
        </div> <!-- END #page-title -->
        <a href="#" id="scroll-down"></a>
        
    </section>
    <!-- HERO -->
    <section id="page-body" class="notoppadding">
        
        
        <div class="column-section boxed-sticky adapt-height clearfix notoppading">
            <div class="column one-half bigpadding last-col has-animation" style="background:#ffffff;">
                <h3><span>¿Que incluye?</span> <hr class="small fat colored"></h3>
                <div class="spacer-medium"></div>
                
                <!-- ACCORDION -->
                
                <div class="">
                    <div class="icon-box">
                        <div class="icon-box-icon">
                            <i class="fa-4x fa fa-ship colored"></i>
                        </div>
                        <div class="icon-box-content">
                            <h5 class="uppercase">Barco ida + regreso</h5>
                            <p>• Party Boat: fiesta bienvenida en medio del mar.</p>
                            <p>• Transporte de equipaje ilimitado sin restricciones.</p>
                            <p>• Mejor acomodación disponible.</p>
                        </div>
                    </div>
                    <div class="spacer-medium"></div>
                    <div class="icon-box">
                        <div class="icon-box-icon"><i class="fa-4x fa fa-bus colored"></i></div>
                        <div class="icon-box-content">
                            <h5 class="uppercase">Transfers</h5>
                            <p>• Traslado autobús - Localidad / Puerto.</p>
                            <p>• Traslado autobús - Puerto Mallorca / Resort, ida y regreso.</p>
                        </div>
                    </div>
                    <div class="spacer-medium"></div>
                    <div class="icon-box">
                        <div class="icon-box-icon"><i class="fa-4x fa fa-bed colored"></i></div>
                        <div class="icon-box-content">
                            <h5 class="uppercase">Alojamiento Resort Bellevue Club***</h5>
                            <p>• Apartamentos en distribución múltiple.</p>
                            <p>• Pensión completa + bebida en restaurant buffet libre.</p>
                            <p>• Monólogo presentación + cóctel de bienvenida.</p>
                        </div>
                    </div>
                    <div class="spacer-medium"></div>
                    <div class="icon-box">
                        <div class="icon-box-icon"><i class="fa-4x fa fa-bullhorn colored"></i></div>
                        <div class="icon-box-content">
                            <h5 class="uppercase">Organización excursiones y discotecas (opcionales)</h5>
                            <p>• Excursión Panorámica Palma + Coves Drach.</p>
                            <p>• Excursión 1 día en Menorca.</p>
                            <p>• Excursión Sunland Festival (parque acuático).</p>
                            <p>• Excursión Catamarán Beach Party.</p>
                            <p>• Pack Discotecas con los mejores DJ’s.</p>
                        </div>
                    </div>
                    <div class="spacer-medium"></div>
                    <div class="icon-box">
                        <div class="icon-box-icon"><i class="fa-4x fa fa-life-ring colored"></i></div>
                        <div class="icon-box-content">
                            <h5 class="uppercase">Seguros</h5>
                            <p>• Asistencia médica y sanitaria en viaje. Seguro básico.</p>
                        </div>
                    </div>
                    <div class="spacer-medium"></div>
                    <div class="icon-box">
                        <div class="icon-box-icon"><i class="fa fa-4x fa-smile-o colored"></i></div>
                        <div class="icon-box-content">
                            <h5 class="uppercase">Material "Viaja Gratis"</h5>
                            <p>• Consigue tu viaje por tan sólo 0€.</p>
                        </div>
                    </div>
                    <div class="spacer-medium"></div>
                    <div class="icon-box">
                        <div class="icon-box-icon"><i class="fa-4x fa fa-thumbs-up colored"></i></div>
                        <div class="icon-box-content">
                            <h5 class="uppercase">ASESORAMIENTO STAFF EN ORIGEN Y DESTINO</h5>
                            <p>• Disfruta con tu grupo, sin preocupaciones.</p>
                        </div>
                    </div>
                    <div class="spacer-medium"></div>
                    <div class="icon-box">
                        <div class="icon-box-icon"><i class="fa fa-4x fa-anchor colored"></i></div>
                        <div class="icon-box-content">
                            <h5 class="uppercase">IMPUESTOS Y TASAS PORTUARIAS INCLUIDAS</h5>
                            <p>• Tu viaje, con la garantía de GRUPO FINALIA y sin sorpresas.</p>
                        </div>
                    </div>
                    <div class="spacer-medium"></div>
                    <div class="icon-box">
                        <div class="icon-box-icon"><i class="fa fa-4x fa-trophy colored"></i></div>
                        <div class="icon-box-content">
                            <h5 class="uppercase">Actividades incluidas</h5>
                            <p>• Gratis partida de Paintball para todo el grupo.</p>
                            <p>• Inscripción gratuita a los torneos deportivos en la playa.</p>
                            <p>• Inscripción gratuita a los torneos acuáticos (Pool Sports).*</p>
                            
                            <p>• 1 hora gratis de las pistas de tenis, fútbol, pádel y minigolf.</p>
                            <p>• Pool parties con barra libre de cócteles.*</p>
                            <p>• Crazy Games (Miss/Míster MIF).*</p>
                           
                            <p>• Toro mecánico e inchables (futbolín humano, gladiadores...).*</p>
                            <p>• Video aftermovie & fotógrafo del evento.</p>
                            <p>• Regalos sorpresa durante el evento.</p>
                            <p>* En las instalaciones del Resort.</p>
                        </div>
                    </div>
                    
                    </div> <!-- END .wrapper -->
                    
                </div>
                <div class="column one-half has-animation" data-delay="200" style="background:url([base_url]theme/theme/files/uploads/about-team-incluye.jpg) center center;background-size:cover;"></div>
                </div> <!-- END .column-section -->
                <div class="fullwidth-section text-light parallax-section" data-parallax-image="[base_url]theme/theme/files/uploads/parallax2.jpg">
                    <div class="fullwidth-content">
                        
                        <div class="wrapper-small align-center">
                            <!-- <h5 class="subtitle-2">Our Clients</h5> -->
                            <hr class="small fat colored">
                            <h2><strong> + de 50.000 estudiantes de BACHILLERATO ya han vivido la experiencia, celebra el FIN DE LA SELE en el resort más grande de EUROPA (miles de estudiantes no pueden estar equivocados…</strong></h2>
                        </div>
                    </div>
                </div>
                <div class="spacer-big"></div>
                <div class="wrapper">
                    
                    <div class="isotope-grid gallery-container style-column-4 clearfix">
                        [foreach:galeria_que_incluye]
                        <div class="isotope-item">
                            <a href="[foto]" class="thumb-overlay" data-rel="lightcase:gallery1">
                                <img src="[foto]" alt="Galeria [foto]"/>
                            </a>
                        </div>
                        [/foreach]
                    </div>
                    
                </div>
                <div>[contacto]</div>
                <div class="spacer-big"></div>
                <div>[footer]</div>
                
            </section>