<!-- HEADER -->
<header id="header" class="header-transparent transparent-light">
    [menu]
</header>
<!-- HERO  -->
<section id="hero" class="hero-full parallax-section text-light" data-parallax-image="[base_url]theme/theme/files/uploads/hero-about-40.jpg">
    
    <div id="page-title" class="wrapper align-center">
        <h4 class="subtitle-2">En Mallorca todas las noches son una fiesta</h4>
        <hr class="small fat colored">
        <h1>Os tenemos preparados diferentes <br>packs Non Stop!</h1>
        </div> <!-- END #page-title -->
        <a href="#" id="scroll-down"></a>
        
    </section>
    <!-- HERO -->
    <section id="page-body" class="notoppadding discoteca ">
        <!-- HERO  -->
        <section id="hero" class="hero-auto parallax-section text-light" data-parallax-image="files/uploads/hero-portfolio-1.jpg">
            
            <div id="page-title" class="wrapper align-center">
                <h4 class="subtitle-2">Conciertos y mucha fiesta con los Dj's Flaix FM</h4>
                <h6 class="subtitle-1" style="line-height: 20px;">Transporte ida y vuelta en autobús. Resort - Discotecas - Resort <br>Consumiciones y regalos<br>Entradas a discotecas<br>Todas las noches garantía acceso -18</h6>
                <hr class="small fat colored">
                <h3><span>Line Up DJ's MIF</span></h3>
                </div> <!-- END #page-title -->
                <?php
                $this->db->order_by('orden','ASC');
                $dj = $this->db->get('galeria_dj');
                ?>
                <ul id="portfolio-filter" class="filter text-light" data-related-grid="portfolio-grid">
                    <li class="active" ><a href="#" data-filter=".all">Todos los DJ's</a></li>
                    <?php foreach($dj->result() as $n=>$g): ?>
                    <li><a href="#" data-filter=".dj<?= $g->id ?>"><?= $g->nombre ?></a></li>
                    <?php endforeach ?>
                </ul>
                
            </section>
            <!-- HERO -->
            <div id="portfolio-grid" class="isotope-grid portfolio-container style-column-3 fitrows">
                
                <?php foreach($dj->result() as $n=>$g): ?>
                <div class="isotope-item portfolio-item dj<?= $g->id ?>">
                    <div class="portfolio-media">
                        <a href="<?= base_url('img/galeria_dj/'.$g->foto1) ?>" class="thumb-overlay overlay-effect-1 text-light" data-rel="lightcase:gallerydj<?= $g->id ?>">
                            <img src="<?= base_url('img/galeria_dj/'.$g->foto1) ?>" alt="<?= $g->nombre ?>">
                            <div class="overlay-caption">
                                <!--<h6 class="caption-sub portfolio-category subtitle-2">Marsal Ventura</h6>-->
                                <!--<h4 class="caption-name portfolio-name uppercase"><?= $g->nombre ?></h4>-->
                            </div>
                        </a>
                    </div>
                </div>
                <div class="isotope-item portfolio-item all dj<?= $g->id ?>">
                    <div class="portfolio-media">
                        <?php if(!$g->ir_a_contacto): ?>
                        <a href="<?= base_url('img/galeria_dj/'.$g->foto2) ?>" class="thumb-overlay overlay-effect-1 text-light" data-rel="lightcase:gallerydj<?= $g->id ?>">
                            <?php else: ?>
                            <a href="<?= base_url('contacto.html') ?>" class="thumb-overlay overlay-effect-1 text-light">
                                <?php endif ?>
                                <img src="<?= base_url('img/galeria_dj/'.$g->foto2) ?>" alt="<?= $g->nombre ?>">
                                <div class="overlay-caption">
                                    <!--<h6 class="caption-sub portfolio-category subtitle-2">Marsal Ventura</h6>-->
                                    <h4 class="caption-name portfolio-name uppercase"><?= $g->nombre ?></h4>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="isotope-item portfolio-item dj<?= $g->id ?>">
                        <div class="portfolio-media">
                            <a href="<?= base_url('img/galeria_dj/'.$g->foto3) ?>" class="thumb-overlay overlay-effect-1 text-light" data-rel="lightcase:gallerydj<?= $g->id ?>">
                                <img src="<?= base_url('img/galeria_dj/'.$g->foto3) ?>" alt="<?= $g->nombre ?>">
                                <div class="overlay-caption">
                                    <!--<h6 class="caption-sub portfolio-category subtitle-2">Marsal Ventura</h6>-->
                                    <!--<h4 class="caption-name portfolio-name uppercase"><?= $g->nombre ?></h4>-->
                                </div>
                            </a>
                        </div>
                    </div>
                    <?php endforeach ?>
                    
                    
                    </div> <!-- END #portfolio-grid .isotope-grid -->
                    <!-- HERO  -->
                    <section id="hero" class="hero-auto parallax-section text-light" data-parallax-image="files/uploads/hero-portfolio-1.jpg">
                        <div id="influencers"></div>
                        <div id="page-title" class="wrapper align-center">
                            <h4 class="subtitle-2">Conoce nuestros influencers</h4>
                            <hr class="small fat colored">
                            <h3><span>Line Up Influencers</span></h3>
                            </div> <!-- END #page-title -->
                            
                            <?php
                            $this->db->order_by('orden','ASC');
                            $dj = $this->db->get('galeria_influencers');
                            ?>
                            <ul id="portfolio-filter" class="filter text-light" data-related-grid="portfolio-influencer">
                                <li class="active" ><a href="#" data-filter=".all">Todos los influencers</a></li>
                                <?php foreach($dj->result() as $n=>$g): ?>
                                <li><a href="#" data-filter=".inf<?= $g->id ?>"><?= $g->nombre ?></a></li>
                                <?php endforeach ?>
                            </ul>
                            
                        </section>
                        <!-- HERO -->
                        <div id="portfolio-influencer" class="isotope-grid portfolio-container style-column-3 fitrows">
                            
                            <?php foreach($dj->result() as $n=>$g): ?>
                            <div class="isotope-item portfolio-item inf<?= $g->id ?>">
                                <div class="portfolio-media">
                                    <a href="<?= base_url('img/galeria_influencers/'.$g->foto1) ?>" class="thumb-overlay overlay-effect-1 text-light" data-rel="lightcase:galleryinf<?= $g->id ?>">
                                        <img src="<?= base_url('img/galeria_influencers/'.$g->foto1) ?>" alt="<?= $g->nombre ?>">
                                        <div class="overlay-caption">
                                            <!--<h6 class="caption-sub portfolio-category subtitle-2">Marsal Ventura</h6>-->
                                            <!--<h4 class="caption-name portfolio-name uppercase"><?= $g->nombre ?></h4>-->
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="isotope-item portfolio-item all inf<?= $g->id ?>">
                                <div class="portfolio-media">
                                    <a href="<?= base_url('img/galeria_influencers/'.$g->foto2) ?>" class="thumb-overlay overlay-effect-1 text-light" data-rel="lightcase:galleryinf<?= $g->id ?>">
                                        <img src="<?= base_url('img/galeria_influencers/'.$g->foto2) ?>" alt="<?= $g->nombre ?>">
                                        <div class="overlay-caption">
                                            <!--<h6 class="caption-sub portfolio-category subtitle-2">Marsal Ventura</h6>-->
                                            <h4 class="caption-name portfolio-name uppercase"><?= $g->nombre ?></h4>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="isotope-item portfolio-item inf<?= $g->id ?>">
                                <div class="portfolio-media">
                                    <a href="<?= base_url('img/galeria_influencers/'.$g->foto3) ?>" class="thumb-overlay overlay-effect-1 text-light" data-rel="lightcase:galleryinf<?= $g->id ?>">
                                        <img src="<?= base_url('img/galeria_influencers/'.$g->foto3) ?>" alt="<?= $g->nombre ?>">
                                        <div class="overlay-caption">
                                            <!--<h6 class="caption-sub portfolio-category subtitle-2">Marsal Ventura</h6>-->
                                            <!--<h4 class="caption-name portfolio-name uppercase"><?= $g->nombre ?></h4>-->
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <?php endforeach ?>
                            
                            </div> <!-- END #portfolio-grid .isotope-grid -->
                            <!-- HERO  -->
                            <section id="hero" class="hero-auto parallax-section text-light" data-parallax-image="files/uploads/hero-portfolio-1.jpg">
                                
                                <div id="page-title" class="wrapper align-center">
                                    <h4 class="subtitle-2">Cada noche una fiesta diferente</h4>
                                    <hr class="small fat colored">
                                    <h3><span>Fiestas Temáticas</span></h3>
                                    </div> <!-- END #page-title -->
                                    
                                    <?php
                                    $dj = $this->db->get('galeria_fiestas');
                                    ?>
                                    <ul id="portfolio-filter" class="filter text-light" data-related-grid="portfolio-fiestas">
                                        <li class="active" ><a href="#" data-filter=".all">Todos las fiestas temáticas</a></li>
                                        <?php foreach($dj->result() as $n=>$g): ?>
                                        <li><a href="#" data-filter=".fis<?= $g->id ?>"><?= $g->nombre ?></a></li>
                                        <?php endforeach ?>
                                    </ul>
                                    
                                </section>
                                <!-- HERO -->
                                <div id="portfolio-fiestas" class="isotope-grid portfolio-container style-column-3 fitrows">
                                    
                                    <?php foreach($dj->result() as $n=>$g): ?>
                                    <div class="isotope-item portfolio-item fis<?= $g->id ?>">
                                        <div class="portfolio-media">
                                            <a href="<?= base_url('img/galeria_fiestas/'.$g->foto1) ?>" class="thumb-overlay overlay-effect-1 text-light" data-rel="lightcase:galleryfies<?= $g->id ?>">
                                                <img src="<?= base_url('img/galeria_fiestas/'.$g->foto1) ?>" alt="<?= $g->nombre ?>">
                                                <div class="overlay-caption">
                                                    <!--<h6 class="caption-sub portfolio-category subtitle-2">Marsal Ventura</h6>-->
                                                    <!--<h4 class="caption-name portfolio-name uppercase"><?= $g->nombre ?></h4>-->
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="isotope-item portfolio-item all fis<?= $g->id ?>">
                                        <div class="portfolio-media">
                                            <a href="<?= base_url('img/galeria_fiestas/'.$g->foto2) ?>" class="thumb-overlay overlay-effect-1 text-light" data-rel="lightcase:galleryfies<?= $g->id ?>">
                                                <img src="<?= base_url('img/galeria_fiestas/'.$g->foto2) ?>" alt="<?= $g->nombre ?>">
                                                <div class="overlay-caption">
                                                    <!--<h6 class="caption-sub portfolio-category subtitle-2">Marsal Ventura</h6>-->
                                                    <h4 class="caption-name portfolio-name uppercase"><?= $g->nombre ?></h4>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="isotope-item portfolio-item fis<?= $g->id ?>">
                                        <div class="portfolio-media">
                                            <a href="<?= base_url('img/galeria_fiestas/'.$g->foto3) ?>" class="thumb-overlay overlay-effect-1 text-light" data-rel="lightcase:galleryfies<?= $g->id ?>">
                                                <img src="<?= base_url('img/galeria_fiestas/'.$g->foto3) ?>" alt="<?= $g->nombre ?>">
                                                <div class="overlay-caption">
                                                    <!--<h6 class="caption-sub portfolio-category subtitle-2">Marsal Ventura</h6>-->
                                                    <!--<h4 class="caption-name portfolio-name uppercase"><?= $g->nombre ?></h4>-->
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <?php endforeach ?>
                                    
                                    </div> <!-- END #portfolio-grid .isotope-grid -->
                                    <div>[contacto]</div>
                                    <div class="spacer-big"></div>
                                    <div>[footer]</div>
                                </section>