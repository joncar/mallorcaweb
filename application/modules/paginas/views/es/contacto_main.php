

        <div class="spacer-big"></div>
    
        <div class="wrapper-small">
            
            <div class="align-center">
            <hr class="small fat colored">
                <div class="spacer-mini"></div>
                <h5 class="subtitle-2 colored">Redes Sociales</h5>
                <ul class="socialmedia-widget style-circled size-small hover-slide-1">
                    <li class="facebook"><a href="https://facebook.com/mallorcaislandfestival" target="_new"></a></li>                    
                    <li class="youtube"><a href="https://www.youtube.com/channel/UCIp1ihBnekNPNIbNQ_9PG4A " target="_new"></a></li>
                    <li class="soundcloud"><a href="https://www.soundcloud.com/finalia" target="_new"></a></li>
                    <li class="instagram"><a href="https://www.instagram.com/mallorca_island_festival/" target="_new"></a></li>
                </ul>
                <div class="spacer-mini"></div>
            <hr class="small fat colored">
            </div>
            
            <div class="spacer-medium"></div>
            
            <h3 class="align-center"><span>Tienes alguna duda?</h3>
            <h5 class="subtitle-2 align-center">Contacta con nosotros<br>Responderemos a todas tus preguntas</h5>
            
            
            <div class="spacer-medium"></div>
            <form id="contact-form" class="checkform sendemail" onsubmit="return contacto(this)" method="post">
                        <div class="form-row">
                            <label for="name">Nombre <abbr title="required" class="required">*</abbr></label>
                            <input type="text" name="nombre" id="name" class="name req" value="" />
                        </div>
                        
                        <div class="form-row">
                            <label for="email">Email <abbr title="required" class="required">*</abbr></label>
                            <input type="text" name="email" id="email" class="email req" value="" />
                        </div>

                        <div class="form-row">
                            <label for="email">Teléfono <abbr title="required" class="required">*</abbr></label>
                            <input type="text" name="telefono" id="email" class="req" value="" />
                        </div>                        
                                        
                        <div class="form-row">
                            <label for="message">Mensaje <abbr title="required" class="required">*</abbr></label>
                            <textarea name="message" id="message" class="req" rows="15" cols="50"></textarea>
                        </div>

                        <div class="form-row">
                            <label for="email"> <input type="checkbox" name="politicas" value="1"> Acepto las <a href="<?= base_url('politicas-de-privacidad.html') ?>" rel="canonical" target="_new">política de privacidad</a></label>                            
                        </div>
                        
                        <div class="form-row">
                            <div class="alert" id="result">
                                
                            </div>
                        </div>
                        
                        <div class="form-row hidden">
                            <input type="text" id="form-check" name="form-check" value="" class="form-check" />
                        </div> <!-- Spam check field -->
                        
                        <div class="form-row">
                            <input type="submit" name="submit" class="submit" value="Enviar" />
                        </div>
                        
                        <input type="hidden" name="subject" value="Contact Subject Sudo html" />
                        <input type="hidden" name="fields" value="name,email,message" />
                    </form>
            
        </div>