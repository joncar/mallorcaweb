<!-- HEADER -->
    <header id="header" class="header-transparent transparent-light">               
        [menu]                      
    </header>
<!-- HERO  -->
    <section id="hero" class="hero-full parallax-section text-light" data-parallax-image="[base_url]theme/theme/files/uploads/hero-about-43.jpg">
    
        <div id="page-title" class="wrapper align-center">
            <h4 class="subtitle-2">Conoce a mallorca island festival</h4>
            <hr class="small fat colored">
            <h1>Experiencias MIF</h1>
        </div> <!-- END #page-title -->
        <a href="#" id="scroll-down"></a>
            
    </section>
<!-- HERO -->

<!-- PAGEBODY -->
	<section id="page-body">
                       
        <div id="blog-grid" class="isotope-grid blog-container style-column-3 isotope-spaced">
            [foreach:blog]
                <div class="isotope-item blog-item">
                    <div class="blog-media">
                        <a rel="canonical"  href="[link]" class="thumb-overlay">
                            <img src="[foto]" alt="[titulo]">
                            <img src="[foto]" class="hover-pic" alt="[titulo]">
                        </a>
                    </div>
                    
                    <div class="blog-desc">
                        <div class="blog-headline">
                            <h6 class="post-category uppercase">[user]</h6>
                            <h5 class="post-name"><a href="[link]"><strong>[titulo]</strong></a></h5>
                        </div>
                        <p>[texto]... <a href="[link]">Leer más</a></p>
                        <ul class="blog-meta">
                            <li class="post-date">[fecha]</li>                            
                        </ul>
                    </div>
                </div>
            [/foreach]
            
                    
        </div> <!-- END #blog-grid .isotope-grid -->
    
        <div class="spacer-small"></div>
                
        <div>[contacto]</div>
        <div class="spacer-big"></div>
        <div>[footer]</div>
 	</section>
	<!-- PAGEBODY -->