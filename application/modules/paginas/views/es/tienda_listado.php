<!-- HEADER -->
<header id="header" class="header-transparent transparent-light">               
    [menu]                      
</header>
<!-- HERO  -->
<!-- HERO  -->
<section id="hero" class="hero-auto parallax-section text-light" data-parallax-image="<?= $categorias->row()->categoria_foto ?>">
    <div id="page-title" class="wrapper align-center">
        <img src="<?= base_url() ?>img/fotos_categorias/<?= $categorias->row()->icono ?>">
        <h2 class="textshadow"><strong><?= $categorias->row()->nombre_categoria ?></strong></h2>
        <h5 class="subtitle-2"><?= $categorias->row()->subtitulo ?></h5>
    </div> <!-- END #page-title -->
</section>
<!-- HERO -->

<!-- PAGEBODY -->
<section id="page-body">
    
    <div class="wrapper">
        <?php if($categorias->row()->sistema_reembolso==0 && $categorias->row()->disponible==1): ?>
            <div id="shop-grid" class="isotope-grid shop-container style-column-3 fitrows isotope-spaced">            
                <?php foreach($productos->result() as $p): ?>
                    <div class="isotope-item shop-item">
                        <div class="product-media">
                            <a href="<?= $p->link ?>" class="thumb-overlay overlay-effect-2 text-light">
                                <img src="<?= base_url() ?>img/fotos_productos/<?= $p->foto ?>" alt="<?= $p->nombre_producto ?>">
                                <img src="<?= base_url() ?>img/fotos_productos/<?= $p->foto ?>" class="hover-pic" alt="<?= $p->nombre_producto ?>">
                                <div class="overlay-caption">
                                    <div class="product-desc align-center">
                                        <h4 class="product-name uppercase textshadow"><?= $p->nombre_producto ?></h4>
                                        <div class="product-price textshadow">
                                            <?= $p->precio ?>
                                            <small><?= $p->precio_tachado ?></small>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <div class="add-to-cart-overlay">
                                <?php if($p->habilitar_venta==1): ?>
                                <a href="javascript:addToCart(<?= $p->id ?>,1)" class="add_to_cart_button">Añadir al carro</a>
                                <?php else: ?>
                                <a href="javascript:void(0)" class="add_to_cart_button">Agotado</a>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>                        
            </div> <!-- END #blog-grid .isotope-grid -->
        <?php elseif($categorias->row()->disponible==0): ?>
            <h5 class="subtitle-2 align-center">Sistema en mantenimiento, por favor intente ingresar más tarde</h5>
        <?php else: ?>
            <?php $this->load->view('sistema_reembolso',['categorias'=>$categorias],FALSE,'tienda'); ?>
        <?php endif ?>
        
        <div class="spacer-small"></div>
        
    </div> <!-- END .wrapper -->
   <div>[contacto]</div>
    <div class="spacer-big"></div>
    <div>[footer]</div>
</section>
<!-- PAGEBODY -->