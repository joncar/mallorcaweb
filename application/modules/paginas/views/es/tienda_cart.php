<!-- HEADER -->
    <header id="header" class="header-transparent transparent-light">               
        [menu]                      
    </header>
<!-- HERO  -->
	<section id="hero" class="hero-auto parallax-section text-light" data-parallax-image="[base_url]theme/theme/files/uploads/parallax2.jpg">
    	
        <div id="page-title" class="wrapper align-center">
            <h2 class="uppercase"><strong>Carrito de compra</strong></h2>
        </div> <!-- END #page-title -->
            
    </section>
    <!-- HERO -->

	<!-- PAGEBODY -->
	<section id="page-body">
    
        <div id="shop-cart" class="wrapper cartdetail">        
        	[carro]        
        </div> <!-- END .wrapper -->
        
        <div class="spacer-big"></div>
        <div>[contacto]</div>
        <div class="spacer-big"></div>
        <div>[footer]</div> 
 	</section>
	<!-- PAGEBODY -->