<!-- HEADER -->
    <header id="header" class="header-transparent transparent-light">               
        [menu]                      
    </header>
    <!-- HERO  -->

    <!-- HERO  -->
<section id="hero" class="hero-full parallax-section text-light" data-parallax-image="[base_url]theme/theme/files/uploads/concurso.jpg">
    
    <div id="page-title" class="wrapper align-center">
        <h4 class="subtitle-2">¿QUIERES GANAR UN VIAJE PARA MIF 2020?</h4>
        <hr class="small fat colored">
        <h1>CONCURSO MIF</h1>
    </div> <!-- END #page-title -->
    <a href="#" id="scroll-down"></a>
        
</section>
    <!-- HERO -->

    <!-- PAGEBODY -->
    <section id="page-body" class="notoppadding">
        
        <div class="column-section boxed-sticky adapt-height clearfix">
            <div class="column one-half bigpadding has-animation" data-delay="200" style="
                background-image:url(<?= base_url('img/tia.jpg') ?>);
                background-size: 140%;
                background-position: center bottom;
                background-color: black;
                color: white;
                min-height: 1128px;
                background-repeat: no-repeat;
                padding-bottom: 50vh !important;">
            
                    <h3 style=" color:white"><strong>BASES LEGALES DEL CONCURSO:</strong></h3>
                    <ul style="color:white">
                        <li class="uppercase">El presente concurso es organizado por FINALIA VIAJES, S.L.</li>
                        <li class="uppercase">El ganador/a del concurso será el formulario más original recibido valorado por nuestro departamento de marketing.</li>
                        <li class="uppercase">El/la ganador/a del concurso será comunicado por el organizador que se pondrá en contacto con el/la ganador/a para informarle que ha sido premiado y explicarle las fechas disponibles para viajar a efectos de bloquear su plazas de forma gratuita.</li>
                    </ul>
            </div>
            <div class="column one-half bigpadding last-col has-animation" style="background:#ffffff;">            
                   <h3><strong>Mándanos tu Propuesta</strong></h3>
                    <p>
                        Completa el siguiente formulario indicando por qué crees que deberías ir tú y un acompañante
                        al MIF 2020 totalmente gratis y no los otros 10.000 estudiantes que nos enviaran su participación!
                    </p>
                    <div class="spacer-small"></div>
                    [output]
            </div>
        </div> <!-- END .column-section -->
        <div>[footer]</div>
    </section>
    <!-- PAGEBODY -->