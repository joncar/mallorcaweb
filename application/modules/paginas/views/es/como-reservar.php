<!-- HEADER -->
<header id="header" class="header-transparent transparent-light">
    [menu]
</header>
<!-- HERO  -->
<section id="hero" class="hero-full parallax-section text-light" data-parallax-image="[base_url]theme/theme/files/uploads/hero-about-41.jpg">
    
    <div id="page-title" class="wrapper align-center">
        <h4 class="subtitle-2">Paso a paso</h4>
        <hr class="small fat colored">
        <h1>¿Cómo reservar?</h1>
        </div> <!-- END #page-title -->
        <a href="#" id="scroll-down"></a>
        
    </section>
    <section id="page-body" class="notoppadding">
        <div class="fullwidth-section text-light parallax-section" data-parallax-image="[base_url]theme/theme/files/uploads/fitness-9.jpg">
            <div class="fullwidth-content wrapper">
                
                <h2 class="align-center"><strong>Puedes hacer el pago por los siguientes medios</strong></h2>
                <small class="visible-xs" style="text-align:center; font-size:12px">Desliza el dedo hacia la izquierda para ver todos los tipos de pago</small>
                <div class="spacer-medium"></div>
                
                <div class="owl-carousel pagos" data-nav="true" data-dots="false" data-loop="true" data-items="4">
                    <div class="owl-item team-item team-member">
                        <div class="thumb-overlay overlay-effect-3 text-light">
                            <img src="[base_url]theme/theme/files/uploads/efectivo.jpg" alt="Pago en efectivo">
                            <div class="overlay-caption">
                                <h4 class="team-name uppercase"><strong>EN EFECTIVO</strong></h4>
                                <h6 style="min-height: 100px;" class="subtitle-2">en cualquier entidad bancaria.</h6>
                            </div>
                        </div>
                    </div>
                    <div class="owl-item team-item team-member">
                        <div class="thumb-overlay overlay-effect-3 text-light">
                            <img src="[base_url]theme/theme/files/uploads/transferencia.jpg" alt="Pago por transferencia">
                            <div class="overlay-caption">
                                <h4 class="team-name uppercase"><strong>TRANSFERENCIA</strong></h4>
                                <h6 style="min-height: 100px;" class="subtitle-2">en nuestra cuenta bancaria. Anotar en concepto de la transferencia:
                                <br>Nº grupo + nombre y apellidos pasajero.</h6>
                            </div>
                        </div>
                    </div>
                    <div class="owl-item team-item team-member">
                        <div class="thumb-overlay overlay-effect-3 text-light">
                            <a href="[base_url]store">
                                <img src="[base_url]theme/theme/files/uploads/online.jpg" alt="Pago on-line">
                                <div class="overlay-caption">
                                    <h4 class="team-name uppercase"><strong>PAGO ON-LINE</strong></h4>
                                    <h6 style="min-height: 100px;" class="subtitle-2">fácilmente siguiendo las instrucciones para formalizar
                                    tu reserva</h6>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="owl-item team-item team-member">
                        <div class="thumb-overlay overlay-effect-3 text-light">
                            <a href="tel:+34902002068">
                                <img src="[base_url]theme/theme/files/uploads/targeta.jpg" alt="Pago con targeta">
                                <div class="overlay-caption">
                                    <h4 class="team-name uppercase"><strong>OPERADORA</strong></h4>
                                    <h6 style="min-height: 100px;" class="subtitle-2">mediante tarjeta de
                                    crédito/débito llamando al
                                    902 002 068</h6>
                                </div>
                            </a>
                        </div>
                    </div>
                    
                </div>
                
            </div>
            </div> <!-- END .fullwidth-section -->
            <div class="column-section boxed-sticky adapt-height clearfix">
                <div class="column one-third empty-content" style="background: rgba(0, 0, 0, 0) url([base_url]theme/theme/files/uploads/agencybusiness-1.jpg) repeat scroll center center / cover; min-height: 583px;"></div>
                <div class="column one-third tallest" style="background: rgba(0, 0, 0, 0) none repeat scroll 0% 0%; min-height: 583px;"><div class="col-content" style="margin-top: 0px;">
                    <h3 style="margin-left: 6px"><span>¿Qué</strong><br>incluye?</span></h3>
                    <hr class="mini thick colored">
                    <p></p>
                    <!-- <div class="spacer-medium"></div> -->
                    <!--
                    <h3><span>Art</strong><br>direction</span></h3>
                    <hr class="mini thick colored">
                    <p>One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back.</p>
                    <div class="spacer-medium"></div>
                    <h3><span>Art</strong><br>direction</span></h3>
                    <hr class="mini thick colored">
                    -->
                    <h5><strong>7 DIAS / 6 NOCHES</strong></h5>
                    
                    <br><strong>BARCO IDA + REGRESO</strong><br>
                    • Party Boat: fiesta bienvenida en medio del mar.<br>
                    • Transporte de equipaje ilimitado sin restricciones.<br>
                    • Mejor acomodación disponible.<br><br>
                    
                    <strong>TRANSFERS</strong><br>
                    • Traslado autobús - Localidad / Puerto.<br>
                    • Traslado autobús - Puerto Mallorca / Resort, ida y regreso.<br><br>
                    
                    <strong>ALOJAMIENTO RESORT BELLEVUE CLUB ***</strong><br>
                    • Apartamentos en distribución múltiple.<br>
                    • Pensión completa + bebidas en restaurante buffet libre.<br>
                    • Monólogo presentación + cóctel de bienvenida.<br><br>
                    
                    <strong>ACTIVIDADES INCLUIDAS</strong><br>
                    • Gratis partida de Paintball para todo el grupo.<br>
                    • Inscripción gratuita a los torneos deportivos en la playa.<br>
                    • Inscripción gratuita a los torneos acuáticos (Pool Sports).*<br>
                    • 1 hora gratis de las pistas de tenis, fútbol, pádel y minigolf.*<br>
                    • Pool parties con barra libre de cócteles.*<br>
                    • Crazy Games (Miss/Míster MIF”).*<br>
                    • Toro mecánico e hinchables (futbolín humano, gladiadores...)*<br>
                    • Videos aftermovie & fotógrafo del evento.<br>
                    • Regalos sorpresa durante el evento.<br>
                    • Conciertos FLAIX FM y FLAIXBAC.*<br><br>
                    
                    * En las instalaciones del Resort.<br><br>
                    
                    <strong>ORGANIZACIÓN EXCURSIONES Y DISCOTECAS (opcionales)</strong><br>
                    • Excursión Panorámica Palma + Coves Drach<br>
                    • Excursión 1 día en Menorca<br>
                    • Excursión Sunland Festival (parque acuático).<br>
                    • Excursión Catamarán Beach Party.<br>
                    • Pack Discotecas con los mejores DJ’s.<br><br>
                    
                    <strong>SEGUROS</strong><br>
                    • Asistencia médica y sanitaria en viaje. Seguro básico.<br><br>
                    
                    <strong>MATERIAL “VIAJA GRATIS”</strong><br>
                    • Consigue tu viaje por tan sólo 0€.<br><br>
                    
                    <strong>ASESORAMIENTO STAFF EN ORIGEN Y DESTINO</strong><br>
                    • Disfruta con tu grupo, sin preocupaciones.<br><br>
                    
                    <strong>IMPUESTOS Y TASAS PORTUARIAS INCLUIDAS</strong><br>
                    • Tu viaje, con la garantía de GRUPO FINALIA y sin sorpresas.<br><br>
                    
                    ​
                <strong>NO INCLUYE:</strong> Todo lo NO especificado en el apartado INCLUYE</p>
            </div></div>
            <div class="column one-third last-col" style="background: rgba(0, 0, 0, 0) none repeat scroll 0% 0%; min-height: 583px;"><div class="col-content" style="margin-top: 0px;">
                <h3 style="margin-left: 6px"><span>Haz tu seguro</strong><br>a lo grande</span></h3>
                <hr class="mini thick colored">
                <p><strong>RESERVA DE PLAZA ESTÁNDAR</strong><br>
                    SERVICIOS INCLUIDOS + SEGUROS BÁSICOS<br>
                    <strong>Seguro BÁSICO.</strong> Seguro obligatorio en asistencia médica y sanitaria durante el viaje.<br><br>
                    
                    <strong>RESERVA DE PLAZA PLUS ESTUDIANTES</strong><br>
                    SERVICIOS INCLUIDOS + SEGURO PLUS ESTUDIANTES<br>
                    <strong>Seguro PLUS ESTUDIANTES.</strong> Se amplían las coberturas en asistencia médica y sanitaria durante el viaje:<br><br>
                    
                    ● Asistencia médica y sanitaria en España: 601,01€<br>
                    ● Repatriación o transporte de heridos y/o enfermos: ILIMITADO<br>
                    ● Repatriación de acompañante INCLUIDO<br>
                    ● Desplazamiento de un familiar en caso de hospitalización superior a cinco días: ILIMITADO<br>
                    ● Convalecencia en hotel. Máximo 10 días. 30 € por día<br>
                    ● Repatriación o transporte del asegurado fallecido. ILIMITADO<br>
                    ● Búsqueda, localización y envío de equipaje extraviado ILIMITADO<br>
                    ● Robo y daños materiales del equipaje 150,25 €<br>
                    ● Transmisión de mensajes urgentes ILIMITADO<br>
                    ● Seguro de Equipajes AXA 150 €<br>
                    ● Seguro de Responsabilidad Civil AXA 60.000 €<br>
                    ● Regreso anticipado por fallecimiento u hospitalización superior a dos días de un familiar de hasta 2º grado. ILIMITADO<br>
                    ● Teléfono de incidencias: 24 HORAS
                </p>
                <div class="spacer-medium"></div>
                <!--
                <h3><span>SEO</strong><br>optimization</span></h3>
                <hr class="mini thick colored">
                <p>One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back.</p>
                <div class="spacer-medium"></div>
                <h3><span>Art</strong><br>direction</span></h3>
                <hr class="mini thick colored">
                <p>One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back.</p>
                
                -->
            </div></div>
        </div>
        <!--<div class="fullwidth-section text-light parallax-section" data-parallax-image="[base_url]theme/theme/files/uploads/parallax6.jpg">
            <div class="fullwidth-content">
                
                <div class="wrapper-small align-center">
                    <h5 class="subtitle-2">Tu puedes!</h5>
                    <hr class="small fat colored">
                    <h2><strong>Consigue tu viaje por tan sólo <br>0 euros</strong></h2>
                </div>
            </div>
        </div>
        <div class="column-section boxed-sticky adapt-height clearfix">
            <div class="column one-half bigpadding has-animation tallest animated" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 609px;"><div class="col-content" style="margin-top: 0px;">
                <h5 class="subtitle-2">We are Sudo</h5>
                <hr class="small fat colored">
                <h2><strong>Creativity &amp; reliability are our strongest power.</strong></h2>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth. Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life.</p>
                <p>It wasn't a dream. His room, a proper human room although a little too small, lay peacefully between its four familiar walls. A collection of textile samples lay spread out on the table - Samsa was a travelling salesman - and above it there hung a picture that he had recently cut out of an illustrated magazine and housed in a nice, gilded frame.
                </p>
            </div></div>
            <div class="column one-half last-col has-animation empty-content animated" data-delay="200" style="background: rgba(0, 0, 0, 0) url([base_url]theme/theme/files/uploads/about-office-2.jpg) repeat scroll center center / cover; min-height: 609px;"></div>
        </div>-->
        <div>[contacto]</div>
        <div class="spacer-big"></div>
        <div>[footer]</div>
    </section>