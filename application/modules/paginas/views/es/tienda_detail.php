<!-- HEADER -->
    <header id="header" class="header-transparent transparent-light">               
        [menu]                      
    </header>
    <!-- HERO  -->

    <!-- HERO  -->
    <section id="hero" class="hero-auto parallax-section text-light" data-parallax-image="<?= $categorias->categoria_foto ?>">
        
        <div id="page-title" class="wrapper align-center">
            <img src="<?= base_url() ?>img/fotos_categorias/<?= $categorias->icono ?>">
            <h2 class="textshadow"><?= $categorias->nombre_categoria ?></h2>
            <h5 class="subtitle-2"><?= $categorias->subtitulo ?></h5>
            
        </div> <!-- END #page-title -->
            
    </section>
    <!-- HERO -->

	<!-- PAGEBODY -->
	<section id="page-body">
       	
        <div id="shop-single">         
            
            <div class="wrapper">
            	<div class="column-section clearfix">
                	<div class="column one-half">
                    	<div class="product-media">

                            <div class="product-main-image">
                                <img src="<?= base_url() ?>img/fotos_productos/<?= $detail->foto ?>" alt="Reserva <?= strip_tags($categorias->nombre_categoria) ?> <?= $detail->nombre_producto ?>">
                            </div>
                            <div class="product-thumbs">
                                <?php foreach($fotos->result() as $f): ?>
                                    <a href="<?= base_url() ?>img/fotos_productos/<?= $f->fote ?>" data-rel="lightcase:gal1"><img src="<?= base_url() ?>img/fotos_productos/<?= $f->fote ?>" alt="Reserva <?= $f->nombre_categoria ?> <?= $f->nombre_producto ?>"></a>
                                <?php endforeach ?>
                            </div>
                     	</div>


                    </div>
                	<div class="column one-half last-col">
                    	<div class="product-desc">
                            <h2 class="product-name"><strong><?= $detail->nombre_producto ?></strong></h2>
                            
                            <div class="product-price">
                                <?= $detail->precio ?>
                                <small><?= $detail->precio_tachado ?></small>
                            </div>
                            <form class="add-to-cart" method="post" onsubmit="return enviarCarro(this)">
                                <div class="form-row">
                                    <div class="quantity">
                                        <input type="button" class="minus" value="-">
                                        <input type="text" name="cantidad" class="qty" value="1">
                                        <input type="hidden" name="producto" value="<?= $detail->id ?>">
                                        <input type="button" class="plus" value="+">
                                    </div>
                                    <?php if($detail->habilitar_venta==1): ?>
                                    <button type="submit" class="add_to_cart_button">Añadir al carro</button>
                                    <?php else: ?>
                                    <button type="button" class="add_to_cart_button">Agotado</button>
                                    <?php endif ?>
                                </div>
                            </form>
                            <div id="share" class="align-left">
                                <ul>
                                    <li class="facebook"><a href="#"></a></li>                                    
                                    <li class="googleplus"><a href="#"></a></li>
                                    <li class="pinterest"><a href="#"></a></li>
                                </ul>
                            </div>
                            <div class="product-intro">
                                <p><?= $detail->descripcion ?></p>
                            </div>
                            
                            
                            
                            
                            
                     	</div> <!-- END .product-desc -->     
                    </div> <!-- END .column -->  
                </div> <!-- END .column-section -->  
            </div> <!-- END .wrapper -->
                       
           
            
		</div> <!-- END #shop-single --> 
                    
        <div id="related-products" class="related-items wrapper">
            <h6 class="uppercase align-center">Productos relacionados</h6>
            
            <div id="related-grid" class="owl-carousel owl-spaced shop-container" data-margin="30" data-nav="false" data-items="3">
                

                <?php foreach($relacionados->result() as $r): ?>
                    <div class="owl-item shop-item">
                        <div class="product-media">
                            
                            <a href="<?= $r->link ?>" class="thumb-overlay overlay-effect-2 text-light">
                                <img src="<?= base_url() ?>img/fotos_productos/<?= $r->foto ?>" alt="<?= $r->nombre_producto ?>">
                                <img src="<?= base_url() ?>img/fotos_productos/<?= $r->foto ?>" class="hover-pic" alt="<?= $r->nombre_producto ?>">
                                <div class="overlay-caption">
                                    <div class="product-desc align-center">
                                        <h6 class="product-name uppercase textshadow"><?= $r->nombre_producto ?></h6>
                                        <div class="product-price textshadow"><?= $r->precio ?></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php endforeach ?>
                
                
            </div> <!-- END .owl-carousel -->
        </div> <!-- END .related-items -->
        
        <div class="spacer-big"></div>
        <div>[contacto]</div>
        <div class="spacer-big"></div>
        <div>[footer]</div>           
 	</section>
	<!-- PAGEBODY -->