<!-- HEADER -->
    <header id="header" class="header-transparent transparent-light">               
        [menu]                      
    </header>
	<!-- HERO  -->
<!-- HERO  -->
    <section id="hero" class="hero-full parallax-section text-light" data-parallax-image="[base_url]theme/theme/files/uploads/hero-about-galeria.jpg">
    
    <div id="page-title" class="wrapper align-center">
        <h4 class="subtitle-2">En Mallorca todas las noches son una fiesta</h4>
        <hr class="small fat colored">
        <h1>Galeria</h1>
        </div> <!-- END #page-title -->
        <a href="#" id="scroll-down"></a>
        
    </section>
    <!-- HERO -->
	
    <!-- PAGEBODY -->
	<section id="page-body" class="notoppadding">
    <div class="fullwidth-section text-light" style="background: #1a1a1a;">
            <div class="fullwidth-content">
                
                <div class="wrapper-small align-center">
                    <!-- <h5 class="subtitle-2">Our Clients</h5> -->
                    <hr class="small fat colored">
                    <h2><strong>¿QUÉ MÁS SE PUEDE PEDIR?</strong></h2>
                    <h3><span class="texthyper" style="font-size:100%">Somos como vosotros, pensamos como vosotros, vivimos como vosotros</span></h3>

                    
                    
                </div>
                
                <div class="spacer-big"></div>
                <ul id="portfolio-filter" class="filter text-light" data-related-grid="portfolio-grid">
                    <li class="active" ><a href="#" data-filter=".all">Todas</a></li>
                    [foreach:categoria_galeria]                        
                    <li><a href="#" data-filter=".dj[id]">[nombre]</a></li>
                    [/foreach]
                </ul>
                <div class="spacer-small"></div>
                <div id="portfolio-grid" class="isotope-grid gallery-container style-column-4 clearfix">
                    [foreach:galeria]
                    <div class="isotope-item dj[categoria_galeria_id] all">
                        <a href="[base_url]img/galeria/[foto]" class="thumb-overlay" data-rel="lightcase:gallery1">
                            <img src="[base_url]img/galeria/[foto]" alt="Galeria [foto]"/>
                        </a>
                    </div>
                    [/foreach]
                </div>
            </div>
        </div>
        
        <div>[contacto]</div>
        <div class="spacer-big"></div>
        <div>[footer]</div>
 	</section>
	<!-- PAGEBODY -->