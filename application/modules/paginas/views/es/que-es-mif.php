<!-- HEADER -->
<header id="header" class="header-transparent transparent-light">
    [menu]
</header>
<!-- HERO  -->
<section id="hero" class="hero-full parallax-section text-light" data-parallax-image="[base_url]theme/theme/files/uploads/hero-about-35.jpg">
    
    <div id="page-title" class="wrapper align-center">
        <h4 class="subtitle-2">Mallorca Island Festival</h4>
        <hr class="small fat colored">
        <h1>¿Qué es MIF?</h1>
        </div> <!-- END #page-title -->
        <a href="#" id="scroll-down"></a>
        
    </section>
    <!-- HERO -->
    <section id="page-body" class="notoppadding">
        
        <div class="column-section boxed-sticky adapt-height clearfix">
            <div class="column one-half has-animation empty-content animated" data-delay="200" style="background: rgba(0, 0, 0, 0) url([base_url]theme/theme/files/uploads/about-team-111.jpg) repeat scroll center center / cover; min-height: 661px;"></div>
            <div class="column one-half bigpadding last-col has-animation tallest animated" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 661px;">
                <div class="col-content" style="margin-top: 0px;">
                    <h3><span>¿Qué es MIF? </span> </h3>
                <hr class="small fat colored">
                
                
                <br><strong>MALLORCA ISLAND FESTIVAL® (MIF)</strong>
                es un festival organizado por el Grupo Finalia para poder ofrecer una alternativa diferente  a los estudiantes de Bachillerato para su fin de curso.
Muchos institutos ya no pueden organizar un viaje de final de estudios a sus alumnos de bachiller por la estrechez del calendario académico, la preparación de la selectividad o el elevado precio de los viajes internacionales.<br>
                <br>Por este motivo nació el <strong>MALLORCA ISLAND FESTIVAL® (MIF)</strong>
                un viaje independiente al instituto y después de la selectividad, dónde año tras año han ido participando más de 40.000 estudiantes.<br><br>
                
                
                </div>
            </div>
        </div>





        <div class="column-section boxed-sticky adapt-height clearfix">            
            <div class="column one-half bigpadding last-col has-animation tallest animated" style="background: rgb(255, 255, 255) none repeat scroll 0% 0%; min-height: 661px;">
                <div class="col-content" style="margin-top: 0px;">
                    <h3><span>¿Por qué asistir al MIF? </span> </h3>
                <hr class="small fat colored">
                
                
                <br><strong>PORQUE</strong>
                es la 10ª Edición del evento y tendrás toda la confianza y garantía de GRUPO FINALIA©.<br><br>
                
                <strong>PORQUE</strong>
                + de 50.000 estudiantes de 2º Bachillerato ya han disfrutado de la experiencia en las ediciones anteriores.<br><br>
                
                <strong>PORQUE</strong>
                este no es un VIAJE cualquiera, es un FESTIVAL organizado para ti y tus amig@s de clase, despide el bachillerato a lo grande e imagina:<br><br>
                
                <strong>• CONOCER</strong>
                 a + de 8.000 estudiantes celebrando el fin de la sele en el resort más grande de Europa (7 edificios de aparta-hoteles).<br><br>
                
                               
                <strong>• DISFRUTAR</strong>
                de las actividades incluidas para todo tu grupo: paintball, pool parties, torneos vóley playa, pool sports, crazy games…<br><br>
                
                <strong>• VIVIR</strong>
                las mejores excursiones: Panorámica Palma, Coves del Drach, Catamarán Beach Party, Sunland Festival 2020…<br><br>
                
                <strong>• PASARLO</strong>
                en grande en las mejores discotecas…<br><br>

                    <strong>•</strong> Y además durante el evento… cartelera de <strong>DJ’s FLAIX FM,</strong> conciertos <strong>FLAIXBAC</strong>, barra libre de cócteles en las pool parties, hinchables, toros mecánicos…<br>
                <br>
                <strong>PORQUE</strong>
                si quieres vivir una experiencia inolvidable, sigue leyendo…<br><br>
                
                </div>
            </div>
            <div class="column one-half has-animation empty-content animated" data-delay="200" style="background: rgba(0, 0, 0, 0) url([base_url]theme/theme/files/uploads/about-team-112.jpg) repeat scroll center center / cover; min-height: 661px;"></div>
        </div>




<div>[contacto]</div>
<div class="spacer-big"></div>
<div>[footer]</div>

</section>