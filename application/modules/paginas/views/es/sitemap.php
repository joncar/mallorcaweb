<!-- HEADER -->
    <header id="header" class="header-transparent transparent-light">               
        [menu]                      
    </header>
	<!-- HERO  -->
<!-- HERO  -->

<!-- HERO  -->
	<section id="hero" class="hero-full parallax-section text-light" data-parallax-image="[base_url]theme/theme/files/uploads/hero-contact-1.jpg">
    
    <div id="page-title" class="wrapper align-center">        
        <h1>Sitemap</h1>
        </div> <!-- END #page-title -->
        <a href="#" id="scroll-down"></a>
        
    </section>
    <!-- HERO -->
    
            
	<!-- PAGEBODY -->
	<section id="page-body">
    
    	<div class="wrapper">
        	
            <div class="column-section clearfix">
            	<div class="column one-four">
                	<h4>Información</h4>
                	<ul>
                    	<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/" title="Mallorca Island Festival">Mallorca Island Festival</a></li>						
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/blog" title="Blog">Blog</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/galeria" title="Galeria">Galeria</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/store" title="Reserva">Reserva</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/mas-informacion" title="Pedir más información">Pedir más información</a></li>						
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/concurso" title="Concurso MIF">Concurso MIF</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/store.html" title="Reserva">Reserva</a></li>
                    </ul>                    
                </div>
                <div class="column one-four">
                	<h4>Nosotros</h4>
                    <ul>
                    	<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/grupo-finalia.html" title="Grupo finalia">Grupo finalia</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/instalaciones.html" title="Instalaciones">Instalaciones</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/nuestro-equipo.html" title="Nuestro equipo">Nuestro equipo</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/trabaja-con-nosotros.html" title="Trabaja con nosotros">Trabaja con nosotros</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/aviso-legal.html" title="Aviso legal">Aviso legal</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/politicas-de-privacidad.html" title="Politicas de privacidad">Politicas de privacidad</a></li>
                    </ul>
                </div>
                <div class="column one-four">
                	<h4>El viaje</h4>
                    <ul>
                    	<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/que-es-mif.html" title="Que es mif">Que es mif</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/que-incluye.html" title="Que incluye">Que incluye</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/el-resort.html" title="El resort">El resort</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/excursiones.html" title="Excursiones">Excursiones</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/actividades.html" title="Actividades">Actividades</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/discotecas.html" title="Discotecas">Discotecas</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/como-reservar.html" title="Reserva este viaje">¿Cómo reservar?</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/zona-padres.html" title="Zona padres">Zona padres</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/faqs.html" title="Faqs">Faqs</a></li>
                    </ul>
                </div>

                <div class="column one-four last-col">
                	<h4>Reserva</h4>
                	<ul>
                    	<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/store/categorias/1-catalunya-br--amp-illes-balears" title="Reserva en catalunya &amp;amp; illes balears">Reserva en catalunya &amp; illes balears</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/store/categorias/2-zona-norte-centro-br--amp-levante" title="Reserva en zona norte, centro &amp;amp; levante">Reserva en zona norte, centro &amp; levante</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/store/categorias/6-zona-sur-amp--br--islas-canarias" title="Reserva en zona sur &amp;amp; islas canarias">Reserva en zona sur &amp; islas canarias</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/store/producto/2-2--plazo-con-seguro-standard-" title="Reserva en catalunya &amp;amp; illes balears 2º Plazo con seguro &quot;STANDARD&quot;">Reserva en catalunya &amp; illes balears 2º Plazo con seguro "STANDARD"</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/store/producto/3-3r-plazo-sin-seguro-de-cancelaci-n" title="Reserva en catalunya &amp;amp; illes balears 3r Plazo sin seguro de cancelación">Reserva en catalunya &amp; illes balears 3r Plazo sin seguro de cancelación</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/store/producto/6-2--plazo-con-seguro-plus-estudiantes-" title="Reserva en catalunya &amp;amp; illes balears 2º Plazo con seguro &quot;PLUS ESTUDIANTES&quot;">Reserva en catalunya &amp; illes balears 2º Plazo con seguro "PLUS ESTUDIANTES"</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/store/producto/7-3r-plazo-con-seguro-de-cancelaci-n" title="Reserva en catalunya &amp;amp; illes balears 3r Plazo con seguro de cancelación">Reserva en catalunya &amp; illes balears 3r Plazo con seguro de cancelación</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/store/producto/8-4--plazo-pack-viaje" title="Reserva en catalunya &amp;amp; illes balears 4º Plazo Pack Viaje">Reserva en catalunya &amp; illes balears 4º Plazo Pack Viaje</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/store/producto/17-1r-plazo-pack-viaje" title="Reserva en catalunya &amp;amp; illes balears 1r Plazo Pack Viaje">Reserva en catalunya &amp; illes balears 1r Plazo Pack Viaje</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/store/producto/4-4--plazo--ltimo-pago" title="Reserva en zona norte, centro &amp;amp; levante 4º Plazo. Último pago">Reserva en zona norte, centro &amp; levante 4º Plazo. Último pago</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/store/producto/5-3r-plazo-sin-seguro-de-cancelaci-n" title="Reserva en zona norte, centro &amp;amp; levante 3r Plazo sin seguro de cancelación">Reserva en zona norte, centro &amp; levante 3r Plazo sin seguro de cancelación</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/store/producto/11-2--plazo-con-seguro-plus-estudiantes-" title="Reserva en zona norte, centro &amp;amp; levante 2º Plazo con seguro &quot;PLUS ESTUDIANTES&quot;">Reserva en zona norte, centro &amp; levante 2º Plazo con seguro "PLUS ESTUDIANTES"</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/store/producto/9-2--plazo-con-seguro-standard-" title="Reserva en zona norte, centro &amp;amp; levante 2º Plazo con seguro &quot;STANDARD&quot;">Reserva en zona norte, centro &amp; levante 2º Plazo con seguro "STANDARD"</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/store/producto/14-3r-plazo-con-seguro-de-cancelaci-n" title="Reserva en zona norte, centro &amp;amp; levante 3r Plazo con seguro de cancelación">Reserva en zona norte, centro &amp; levante 3r Plazo con seguro de cancelación</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/store/producto/18-1r-plazo-pack-viaje" title="Reserva en zona norte, centro &amp;amp; levante 1r Plazo Pack Viaje">Reserva en zona norte, centro &amp; levante 1r Plazo Pack Viaje</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/store/producto/12-2--plazo-con-seguro-plus-estudiantes-" title="Reserva en zona sur &amp;amp; islas canarias 2º Plazo con seguro &quot;PLUS ESTUDIANTES&quot;">Reserva en zona sur &amp; islas canarias 2º Plazo con seguro "PLUS ESTUDIANTES"</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/store/producto/10-2--plazo-con-seguro-standard-" title="Reserva en zona sur &amp;amp; islas canarias 2º Plazo con seguro &quot;STANDARD&quot;">Reserva en zona sur &amp; islas canarias 2º Plazo con seguro "STANDARD"</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/store/producto/13-3r-plazo-sin-seguro-de-cancelaci-n" title="Reserva en zona sur &amp;amp; islas canarias 3r Plazo sin seguro de cancelación">Reserva en zona sur &amp; islas canarias 3r Plazo sin seguro de cancelación</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/store/producto/15-3r-plazo-con-seguro-de-cancelaci-n" title="Reserva en zona sur &amp;amp; islas canarias 3r Plazo con seguro de cancelación">Reserva en zona sur &amp; islas canarias 3r Plazo con seguro de cancelación</a></li>
						<li class="lpage"><a href="https://www.mallorcaislandfestival.com/new/store/producto/16-4--plazo--ltimo-pago" title="Reserva en zona sur &amp;amp; islas canarias 4º Plazo. Último pago">Reserva en zona sur &amp; islas canarias 4º Plazo. Último pago</a></li>
						<li class="lpage last-page"><a href="https://www.mallorcaislandfestival.com/new/store/producto/19-1r-plazo-pack-viaje" title="Reserva en zona sur &amp;amp; islas canarias 1r Plazo Pack Viaje">Reserva en zona sur &amp; islas canarias 1r Plazo Pack Viaje</a></li>
                    </ul>                    
                </div>

            </div>

            <div class="column-section clearfix">
            	
                
            </div>
        </div> <!-- END .wrapper-small -->
                
        <div class="spacer-big"></div>		
		<div>[footer]</div>
 	</section>
	<!-- PAGEBODY -->