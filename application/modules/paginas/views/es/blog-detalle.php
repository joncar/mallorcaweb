<!-- HEADER -->
	<!-- HEADER -->
    <header id="header" class="header-transparent transparent-light">               
        [menu]                      
    </header>
	    
    <!-- HERO  -->
	<section id="hero" class="hero-full parallax-section text-light parallax-pattern" 
		data-parallax-image="[foto]">
    	
        <div id="page-title" class="wrapper align-center">
            <h4 class="subtitle-2">Conoce a mallorca island festival</h4>
            <hr class="small fat colored">
            <h1>Experiencias MIF</h1>            
        </div> <!-- END #page-title -->
        <a href="#" id="scroll-down"></a>
        
    </section>
    <!-- HERO -->
    
            
	<!-- PAGEBODY -->
	<section id="page-body">
    
        <div id="blog-single">
                            
            <div class="post-content">
                
                <div class="wrapper-small">

                    <ul class="blog-meta">
                        <li class="post-date">[fecha]</li>
                        <li class="post-author"><a href="#"><span>[user]</span></a></li>
                    </ul>
                
                    <h4 class="uppercase"><strong>[titulo]</strong></h4>
                    <div class="blog-content">
                       [texto]
                    </div>
                
                </div> <!-- END .wrapper-small -->
                
                <div class="spacer-big"></div>
                

            
            </div> <!-- END .wrapper-small -->
                            
        </div> <!-- END #blog-single -->
        
  
        <div class="spacer-medium"></div>
        
        <div id="single-pagination">
            <ul>
                <li class="prev">[prev]</li>
                <li class="back"><a rel="canonical"  href="[base_url]blog">Regresar al blog</a></li>
                <li class="next">[next]</li>
            </ul>
        </div>  
                    
 	</section>
	<!-- PAGEBODY -->
	<div>[contacto]</div>
    <div class="spacer-big"></div>
    <div>[footer]</div>