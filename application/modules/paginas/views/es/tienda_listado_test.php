<!-- HEADER -->
    <header id="header" class="header-transparent transparent-light">               
        [menu]                      
    </header>
    <!-- HERO  -->
    <!-- HERO  -->
	<section id="hero" class="hero-auto parallax-section text-light" data-parallax-image="[categoria_foto]">
    	
        <div id="page-title" class="wrapper align-center">
            <img src="[base_url]img/fotos_categorias/[icono]">
            <h2 class="textshadow"><strong>[nombre_categoria]</strong></h2>
            <h5 class="subtitle-2">[subtitulo]</h5>
            
        </div> <!-- END #page-title -->
            
    </section>
    <!-- HERO -->

	<!-- PAGEBODY -->
	<section id="page-body">
        
        <div class="wrapper">
                
            <div id="shop-grid" class="isotope-grid shop-container style-column-3 fitrows isotope-spaced">
            
                [foreach:productos]asd
                    <div class="isotope-item shop-item">
                        <div class="product-media">
                            <a href="[link]" class="thumb-overlay overlay-effect-2 text-light">
                                <img src="[base_url]img/fotos_productos/[foto]" alt="[nombre_producto]">
                                <img src="[base_url]img/fotos_productos/[foto]" class="hover-pic" alt="[nombre_producto]">
                                <div class="overlay-caption">
                                    <div class="product-desc align-center">
                                        <h4 class="product-name uppercase textshadow">[nombre_producto]</h4>
                                        <div class="product-price textshadow">
                                        	[precio]€
                                        	<small>[precio_tachado]</small>
                                    	</div>
                                    </div>
                                </div>
                            </a>
                            <div class="add-to-cart-overlay">
                                <a href="javascript:addToCart([id],1)" class="add_to_cart_button">Añadir al carro</a>
                            </div>
                        </div>
                    </div>
                [/foreach]
                
                
            </div> <!-- END #blog-grid .isotope-grid -->
            
            <div class="spacer-small"></div>
            
        </div> <!-- END .wrapper -->
       <div>[contacto]</div>
        <div class="spacer-big"></div>
        <div>[footer]</div>
 	</section>
	<!-- PAGEBODY -->