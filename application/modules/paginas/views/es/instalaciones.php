<!-- HEADER -->
<header id="header" class="header-transparent transparent-light">
    [menu]
</header>
<!-- HERO  -->
<!-- HERO  -->
<section id="hero" class="hero-full parallax-section text-light" data-parallax-image="[base_url]theme/theme/files/uploads/hero-about-32.jpg">
    
    <div id="page-title" class="wrapper align-center">
        <h4 class="subtitle-2">Instalaciones</h4>
        <hr class="small fat colored">
        <h1>Dónde nos vas a encontrar</h1>
        </div> <!-- END #page-title -->
        <a href="#" id="scroll-down"></a>
        
    </section>
    <!-- PAGEBODY -->
    <section id="page-body" class="notoppadding">
        
        <div class="column-section boxed-sticky adapt-height clearfix">
            <div class="column one-half bigpadding has-animation" style="background:#ffffff;">
                <!-- <h5 class="subtitle-2">We are Sudo</h5> -->
                <h3><strong>Grupo Finalia</strong> <hr class="small fat colored"></h3>
                <p>División de Turismo</p>
                <p>Tel. 902 002 068
                </p>
                <p>Horario de Atención al Cliente:</p>
                <p>Lunes a Viernes de 08:00 a 21:00h.</p>
                <p>C/ Girona, 34. 08700 Igualada. Spain</p>
            </div>
            <div  style="min-height:400px">
                <div class="column one-half last-col has-animation" style="height:400px" data-delay="200" id="mapa" data-lat="41.5860735" data-lng="1.6237623" data-icon="<?= base_url('img/map-marker.png') ?>">
            </div>
                
            </div>
            </div> <!-- END .column-section -->
            <h3 class="align-center"><strong>Departamentos</strong> <hr class="zigzag small colored"></h3>
            <div class="spacer-medium"></div>
            
            <div class="column-section boxed-sticky adapt-height vertical-center clearfix">
                <div class="column one-third text-light bigpadding tallest" style="background: rgb(228, 167, 72) none repeat scroll 0% 0%; min-height: 386px;"><div class="col-content" style="margin-top: 0px;">
                    <h4><strong>Departamento Central <br> Información</strong></h4>
                    <p><a style="text-decoration: underline;" class="text-light sobrerosa" href="mailto:info@finalia.es">info@finalia.es</a></p>
                    <!-- <p><a href="#" class="sr-button button-3 button-small">More Info</a></p> -->
                </div></div>
                <div class="column one-third text-light bigpadding" style="background: rgb(228, 96, 116) none repeat scroll 0% 0%; min-height: 386px;"><div class="col-content" style="margin-top: 0.5px;">
                    <h4><strong>Departamento Central <br> Administración</strong></h4>
                    <p><a style="text-decoration: underline;" class="text-light sobrerosa" href="mailto:administracion@finalia.es">administracion@finalia.es</a></p>
                    <!-- <p><a href="#" class="sr-button button-2 button-small">More Info</a></p> -->
                </div></div>
                <div class="column one-third text-light bigpadding last-col" style="background: rgb(228, 110, 228) none repeat scroll 0% 0%; min-height: 386px;"><div class="col-content" style="margin-top: 0.5px;">
                    <h4><strong>Departamento Central <br> Comercial</strong></h4>
                    <p><a style="text-decoration: underline;" class="text-light sobrerosa" href="mailto:comercial@finalia.es">comercial@finalia.es</a></p>
                    <!-- <p><a href="#" class="sr-button button-3 button-small">More Info</a></p> -->
                </div></div>
                
            </div>
            <div class="column-section boxed-sticky adapt-height vertical-center clearfix">
                <div class="column one-third text-light bigpadding tallest" style="background: rgb(129, 175, 228) none repeat scroll 0% 0%; min-height: 386px;"><div class="col-content" style="margin-top: 0px;">
                    <h4><strong>Departamento Central <br> Contabilidad</strong></h4>
                    <p><a style="text-decoration: underline;" class="text-light sobrerosa" href="mailto:contabilidad@finalia.es">contabilidad@finalia.es</a></p>
                    <!-- <p><a href="#" class="sr-button button-3 button-small">More Info</a></p> -->
                </div></div>
                <div class="column one-third text-light bigpadding" style="background: rgb(53, 228, 175) none repeat scroll 0% 0%; min-height: 386px;"><div class="col-content" style="margin-top: 0.5px;">
                    <h4><strong>Departamento Central <br>Reservas</strong></h4>
                    <p><a style="text-decoration: underline;" class="text-light sobrerosa" href="mailto:reservas@finalia.es">reservas@finalia.es</a></p>
                    <!-- <p><a href="#" class="sr-button button-2 button-small">More Info</a></p> -->
                </div></div>
                <div class="column one-third text-light bigpadding last-col" style="background: rgb(228, 219, 112) none repeat scroll 0% 0%; min-height: 386px;"><div class="col-content" style="margin-top: 0.5px;">
                    <h4><strong>Departamento y Sedes <br> Logística</strong></h4>
                    <p><a style="text-decoration: underline;" class="text-light sobrerosa" href="mailto:info@mallorcaislandfestival.com">info@mallorcaislandfestival.com</a></p>
                    <!--  <p><a href="#" class="sr-button button-3 button-small">More Info</a></p> -->
                </div></div>
                
            </div>                        
            <div class="fullwidth-section text-light parallax-section" data-parallax-image="[base_url]theme/theme/files/uploads/parallax1.jpg">
            <div class="spacer-big"></div>
            <div class="wrapper">
                <h3 class="align-center">
                    <strong>Sedes</strong> 
                    <hr class="zigzag small colored">
                </h3>
                <div class="column-section align-center clearfix">
                    <div class="column one-third">
                        <img src="[base_url]theme/theme/files/uploads/zonas/noreste.png">
                        <h5 style="margin-top:20px;" class="uppercase">Noreste
                        <br>
                        <a href="mailto:info@mallorcaislandfestival.com" class="sobrerosa"><small><u>INFO</u></small></a>
                        </h5>
                    </div>
                    
                    <div class="column one-third">
                        <img src="[base_url]theme/theme/files/uploads/zonas/levante.png">
                        <h5 style="margin-top:20px;" class="uppercase">Levante
                        <br>
                        <a href="mailto:info@mallorcaislandfestival.com" class="sobrerosa"><small><u>INFO</u></small></a>
                        </h5>
                    </div>
                    
                    <div class="column one-third last-col">
                        <img src="[base_url]theme/theme/files/uploads/zonas/canarias.png">
                        <h5 style="margin-top:20px;" class="uppercase">Canarias
                        <br>
                        <a href="mailto:info@mallorcaislandfestival.com" class="sobrerosa"><small><u>INFO</u></small></a>
                        </h5>
                    </div>
                </div>
                
                <div class="column-section align-center clearfix">
                    <div class="column one-third">
                        <img src="[base_url]theme/theme/files/uploads/zonas/sur.png">
                        <h5 style="margin-top:20px;" class="uppercase">Andalucia
                        <br>
                        <a href="mailto:info@mallorcaislandfestival.com" class="sobrerosa"><small><u>INFO</u></small></a>
                        </h5>
                    </div>
                    
                    <div class="column one-third">
                        <img src="[base_url]theme/theme/files/uploads/zonas/centro.png">
                        <h5 style="margin-top:20px;" class="uppercase">Centro
                        <br>
                        <a href="mailto:info@mallorcaislandfestival.com" class="sobrerosa"><small><u>INFO</u></small></a>
                        </h5>
                    </div>
                    
                    <div class="column one-third last-col">
                        <img src="[base_url]theme/theme/files/uploads/zonas/noroeste.png">
                        <h5 style="margin-top:20px;" class="uppercase">Nordoseste
                        <br>
                        <a href="mailto:info@mallorcaislandfestival.com" class="sobrerosa"><small><u>INFO</u></small></a>
                        </h5>
                    </div>
                </div>
            </div>      

                
                <div class="spacer-big"></div>
                
            </div>
            <div>[contacto]</div>
            <div class="spacer-big"></div>
            <div>[footer]</div>
            
        </section>
        <!-- PAGEBODY -->