<!-- HEADER -->
    <header id="header" class="header-transparent transparent-light">               
        [menu]                      
    </header>
<!-- HERO  -->
<section id="hero" class="hero-full parallax-section text-light" data-parallax-image="[base_url]theme/theme/files/uploads/hero-about-34.jpg">
    
    <div id="page-title" class="wrapper align-center">
        <h4 class="subtitle-2">Trabaja con nosotros</h4>
        <hr class="small fat colored">
        <h1>Forma parte de nuestro mundo</h1>
    </div> <!-- END #page-title -->
    <a href="#" id="scroll-down"></a>
        
</section>

<!-- PAGEBODY -->
	<section id="page-body">
    
    	<div class="wrapper">
        	
            <div class="column-section clearfix">
            
            	<div class="column one-third">
                	<h3><span>¿Qué ofrecemos?</span></h3>
                    <p></p>
                    <h6 class="uppercase">Desarrollo personal y profesional</h6>
                    
                    <h6 class="uppercase">Un buen ambiente de trabajo</h6>
                	
                    <h6 class="uppercase">Formación por competencias</h6>
                    <h6 class="uppercase">Oportunidades de promoción interna</h6>
                    <h6 class="uppercase">Movilidad laboral</h6>
                    
                </div>
                
                <div class="column two-third last-col">
                	<h3><strong>Mándanos tu Currículum</strong></h3>
                    <p>Si compartes nuestros valores y dinámicas de trabajo y quieres formar parte de nuestro equipo de profesionales puedes enviarnos tú candidatura a nuestro departamento RR.HH. con el fin de poder tenerla en cuenta en futuras ofertas laborales:</p>
                    <div class="spacer-small"></div>
                    [output]










                </div>
            
            </div> <!-- END .column-section -->
            
        </div> <!-- END .wrapper-small -->
                
        <div class="spacer-big"></div>
        <div>[footer]</div>
 	</section>
	<!-- PAGEBODY -->