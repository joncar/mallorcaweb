<!-- HEADER -->
    <header id="header" class="header-transparent transparent-light">               
        [menu]                      
    </header>
	<!-- HERO  -->
<!-- HERO  -->

<!-- HERO  -->
	<section id="hero" class="hero-full parallax-section text-light" data-parallax-image="[base_url]theme/theme/files/uploads/contacto.jpg">
    
    <div id="page-title" class="wrapper align-center">
        <h4 class="subtitle-2">¿Quieres participar?</h4>
        <hr class="small fat colored">
        <h1>Contacto <br/>patrocinador</h1>
        </div> <!-- END #page-title -->
        <a href="#" id="scroll-down"></a>
        
    </section>
    <!-- HERO -->
    
            
	<!-- PAGEBODY -->
	<section id="page-body">
    
    	<div class="wrapper">
        	
            <div class="column-section clearfix">
            
            	<div class="column one-third hidden-xs">
                	<h3><span>Llámanos</span></h3>
                    <p>Llama o contacta con nuestras oficinas y resólveremos todas tus dudas. Siempre a tu disposición!</p>
                    <h6 class="uppercase">ATENCIÓN TELEFÓNICA</h6>
                    <p>de Lunes a Viernes<br>de 08:00 a 21:00 h.<br> 902 002 068</p>
                    <h6 class="uppercase">Chat</h6>
                	<p>de Lunes a Viernes<br>de 08:00 a 21:00 h.</p>
                	<h6 class="uppercase">EMAIL</h6>
                    <p>
                        <a href="mailto:info@mallorcaislandfestival.com">info@mallorcaislandfestival.com</a>
                        <br>
                        <a href="mailto:reservas@mallorcaislandfestival.com">reservas@mallorcaislandfestival.com</a>
                    </p>
                    <h6 class="uppercase">WEB</h6>
                    <p><a href="https://mallorcaislandfestival.com">www.mallorcaislandfestival.com</a></p>
                    <h6 class="uppercase">GRUPO FINALIA</h6>
                    <p><a href="https://goo.gl/maps/F3Q9Tvd25Sw" target="_new">C/Girona, 34 <br> 08700 Igualada, Spain</a></p>
                    <h6 class="uppercase">REDES SOCIALES</h6>
                    <ul class="socialmedia-widget size-small hover-fade-1">
                        <li class="facebook"><a href="https://facebook.com/mallorcaislandfestival" target="_new"></a></li>
                        <li class="twitter"><a href="#" target="_new"></a></li>
                        <li class="youtube"><a href="https://www.youtube.com/channel/UCIp1ihBnekNPNIbNQ_9PG4A " target="_new"></a></li>
                        <li class="soundcloud"><a href="https://www.soundcloud.com/finalia" target="_new"></a></li>
                        <li class="instagram"><a href="https://www.instagram.com/mallorca_island_festival/" target="_new"></a></li>
                    </ul>
                </div>
                
                <div class="column two-third last-col">
                	<h3><strong>Contacta con nosotros</strong></h3>
                
                    <form id="contact-form" class="checkform sendemail" onsubmit="return contacto(this)" method="post">
                        <div class="form-row">
                            <label for="name">Nombre <abbr title="required" class="required">*</abbr></label>
                            <input type="text" name="nombre" id="name" class="name req" value="" />
                        </div>
                        
                        <div class="form-row">
                            <label for="email">Email <abbr title="required" class="required">*</abbr></label>
                            <input type="text" name="email" id="email" class="email req" value="" />
                        </div>

                        <div class="form-row">
                            <label for="email">Teléfono <abbr title="required" class="required">*</abbr></label>
                            <input type="text" name="telefono" id="email" class="req" value="" />
                        </div>                        
                                        
                        <div class="form-row">
                            <label for="message">Mensaje <abbr title="required" class="required">*</abbr></label>
                            <textarea name="message" id="message" class="req" rows="15" cols="50"></textarea>
                        </div>

                        <div class="form-row">
                            <input type="checkbox" name="politicas" value="1"> Acepto las <a href="<?= base_url('politicas-de-privacidad.html') ?>" rel="canonical" target="_new">política de privacidad</a>
                        </div>
                        
                        <div class="form-row">
                            <div class="alert" id="result">
                                
                            </div>
                        </div>
                        
                        <div class="form-row hidden">
                            <input type="text" id="form-check" name="form-check" value="" class="form-check" />
                        </div> <!-- Spam check field -->

                        <div class="form-row">
                            <div class="g-recaptcha" data-sitekey="6LciCHAUAAAAAM5R0xYoqlfiNcMmKwINyX2GHk2D"></div>
                        </div> <!-- Spam check field -->
                        
                        <div class="form-row">
                            <input type="submit" name="submit" class="submit" value="Enviar" />
                        </div>
                        
                        <input type="hidden" name="subject" value="contacto_patrocinador" />
                        <input type="hidden" name="fields" value="name,email,message" />
                    </form>
                </div>

                <div class="column one-third visible-xs">
                    <h3><span>Llámanos</span></h3>
                    <p>Llama o contacta con nuestras oficinas y resólveremos todas tus dudas. Siempre a tu disposición!</p>
                    <h6 class="uppercase">ATENCIÓN TELEFÓNICA</h6>
                    <p>de Lunes a Viernes<br>de 08:00 a 21:00 h.<br> 902 002 068</p>
                    <h6 class="uppercase">Chat</h6>
                    <p>de Lunes a Viernes<br>de 08:00 a 21:00 h.</p>
                    <h6 class="uppercase">EMAIL</h6>
                    <p>
                        <a href="mailto:info@mallorcaislandfestival.com">info@mallorcaislandfestival.com</a>
                        <br>
                        <a href="mailto:reservas@mallorcaislandfestival.com">reservas@mallorcaislandfestival.com</a>
                    </p>
                    <h6 class="uppercase">WEB</h6>
                    <p><a href="https://mallorcaislandfestival.com">www.mallorcaislandfestival.com</a></p>
                    <h6 class="uppercase">GRUPO FINALIA</h6>
                    <p><a href="https://goo.gl/maps/F3Q9Tvd25Sw" target="_new">C/Girona, 34 <br> 08700 Igualada, Spain</a></p>
                    <h6 class="uppercase">REDES SOCIALES</h6>
                    <ul class="socialmedia-widget size-small hover-fade-1">
                        <li class="facebook"><a href="https://facebook.com/mallorcaislandfestival" target="_new"></a></li>
                        <li class="twitter"><a href="#" target="_new"></a></li>
                        <li class="youtube"><a href="https://www.youtube.com/channel/UCIp1ihBnekNPNIbNQ_9PG4A " target="_new"></a></li>
                        <li class="soundcloud"><a href="https://www.soundcloud.com/finalia" target="_new"></a></li>
                        <li class="instagram"><a href="https://www.instagram.com/mallorca_island_festival/" target="_new"></a></li>
                    </ul>
                </div>
            
            </div> <!-- END .column-section -->
            
        </div> <!-- END .wrapper-small -->
                
        <div class="spacer-big"></div>		
		<div>[footer]</div>
 	</section>
	<!-- PAGEBODY -->