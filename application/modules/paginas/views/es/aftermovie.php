<!-- HEADER -->
    <header id="header" class="header-transparent transparent-light">               
        [menu]                      
    </header>
	<!-- HERO  -->
<!-- HERO  -->
    <section id="hero" class="hero-full parallax-section text-light" data-parallax-image="[base_url]theme/theme/files/uploads/hero-about-galeria.jpg">
    
    <div id="page-title" class="wrapper align-center">
        <h4 class="subtitle-2">Nuestros mejores videos</h4>
        <hr class="small fat colored">
        <h1>Aftermovie 2019</h1>
        </div> <!-- END #page-title -->
        <a href="#" id="scroll-down"></a>
        
    </section>
    <!-- HERO -->
	
    <!-- PAGEBODY -->
	<section id="page-body" class="notoppadding">
    

        
        <div class="fullwidth-section text-light videobg-section"
        data-videotype="youtube" 
        data-videoyoutubeid="7S0mOohlnP4"
        data-videoratio="16/9"
        data-videoloop="true"                        
        data-videoposter="<?= base_url() ?>theme/theme/files/uploads/900x600-dark.jpg"
        data-videooverlaycolor="#000000"
        data-videooverlayopacity="0.7">
            <div class="fullwidth-content wrapper align-center">
                
                <h2><strong>After Movie 2019</strong></h2>
                <hr class="small fat colored">
                <h5 class="subtitle-1"></h5>
                <div class="spacer-medium"></div>
                <a class="sr-button small-button button-4 rounded" href="https://www.youtube.com/embed/R-vGu_ieVLU" data-rel="lightcase">Ver Video</a>
                
            </div>
        </div>


        
        <div>[contacto]</div>
        <div class="spacer-big"></div>
        <div>[footer]</div>
 	</section>
	<!-- PAGEBODY -->