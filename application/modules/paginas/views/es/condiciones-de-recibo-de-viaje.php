<!-- HEADER -->
    <header id="header" class="header-transparent transparent-light">               
        [menu]                      
    </header>
	<!-- HERO  -->
<!-- HERO  -->

<!-- HERO  -->
	<section id="hero" class="hero-auto parallax-section text-light" data-parallax-image="[base_url]theme/theme/files/uploads/contacto.jpg">
    	
        <div id="page-title" class="wrapper align-center">
            <h1 class="texthyper">Condiciones Generales</h1>            
        </div> <!-- END #page-title -->
            
    </section>
    <!-- HERO -->
    
            
	<!-- PAGEBODY -->
	<section id="page-body">
    
    	<div class="wrapper">
        	
            <?= $this->db->get_where('ajustes')->row()->condiciones_generales ?>
            
            	 <!-- END .column-section -->
            
        </div> <!-- END .wrapper-small -->
                
        <div class="spacer-big"></div>		
		<div>[footer]</div>
 	</section>
	<!-- PAGEBODY -->