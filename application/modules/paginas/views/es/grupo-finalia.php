<!-- HEADER -->
    <header id="header" class="header-transparent transparent-light">               
        [menu]                      
    </header>
	<!-- HERO  -->

    <!-- HERO  -->
    <section id="hero" class="hero-full parallax-section text-light" data-parallax-image="[base_url]theme/theme/files/uploads/hero-about-3.jpg">
        
        <div id="page-title" class="wrapper align-center">
            <h4 class="subtitle-2">Nosotros</h4>
            <hr class="small fat colored">
            <h1>¿Qué es<br><span class="colored">Grupo Finalia?</span><br></h1>
        </div> <!-- END #page-title -->
        <a href="#" id="scroll-down"></a>
            
    </section>
    <!-- HERO -->

    <!-- PAGEBODY -->
	<section id="page-body" class="notoppadding">
        
        <div class="column-section boxed-sticky adapt-height clearfix">
            <div class="column one-half has-animation" data-delay="200" style="background:url([base_url]theme/theme/files/uploads/about-team-1.jpg) center center;background-size:cover;"></div>
        	<div class="column one-half bigpadding last-col has-animation" style="background:#ffffff;">
            <!-- 	<h5 class="subtitle-2">Our Team</h5> -->
                
                <h3><span>Grupo Finalia</span> <hr class="small fat colored"></h3>
                <p>Finalia Viajes es la agencia de viajes líder en organización de viajes de fin de curso, elegida por excelencia entre los Centros Educativos y Asociaciones de Madres y Padres de Alumnos de toda España, convencidos que la educación que reciben sus hijos e hijas en las aulas y hogares, es complementaria a la formación didáctica y pedagógica en la que se orientan nuestros viajes de fin de curso programados.</p>
                <p><h5>Finalia Viajes des de 1.999</h5>

 

En Finalia Viajes trabajamos des de 1.999. El equipo profesional de Finalia Viajes está especializado en la gestión integral de viajes programados, viajes de fin de curso y excursiones en el ámbito educativo. Aportamos soluciones adecuadas en la organización y desarrollo de las propuesta recibidas por los centros. En Finalia Viajes nos coordinamos con el equipo docente, a efectos de garantizar la satisfacción de un aprendizaje completo. ¿Cómo? Durante las actividades, eventos y visitas planificadas en los viajes de fin de curso de nuestros centros. Queremos gestionar todos los viajes y garantizar la seguridad a nuestros alumnos y profesores. Por eso, des de Finalia Viajes habilitamos coordinadores expertos en organización de actividades. También preparamos viajes programados para que puedan resolver todas las necesidades de nuestros clientes. 

<br>Más de 60.000 alumnos, padres y madres ya han confiado en Finalia Viajes para organizar su viaje de fin de curso. En Finalia Viajes organizamos: viajes de fin de curso para secundaria, viajes de fin de bachillerato, viajes de fin de carrera, y viajes programados para colectivos de estudiantes. Los viajes son hechos a medida para cumplir con las necesidades de cada centro.
                </p>
                <p><h5>Finalia Viajes – Expertos en estudiantes</h5>

 

El Grupo Finalia Viajes desarrolla su actividad en el sector turístico con presencia en diferentes y variadas líneas de negocio. Nuestro amplio abanico de actividades dentro del sector nos permite: estar diversificados y alcanzar importantes sinergias. Además, tenemos la posibilidad de ofrecer a nuestros clientes una gestión global de sus necesidades. 

<br>El Grupo Finalia Viajes está formado por un conjunto de empresas independientes en sus respectivos sectores: estudiantes, estadas lingüísticas, eventos deportivos, turismo on-line, receptivos, aéreos, business travel y continental, con un objetivo común: organizar los mejores viajes, y que vuestro viaje de fin de curso sea inolvidable. Lo que hace diferente al Grupo Finalia Viajes, lo que le da sentido, músculo y estabilidad, es el hecho de que cada empresa respalda a las otras y es, a su vez, respaldada por las demás, haciendo un gran puzzle en el que todas las piezas encajan. 
                </p>
            </div>
        </div> <!-- END .column-section -->
        
        <div class="isotope-grid gallery-grid style-modern clearfix" data-heightratio="1">
        	<div class="isotope-item "><img src="[base_url]theme/theme/files/uploads/about-team-4.jpg" alt="SEO IMG NAME"></div>
        	<div class="isotope-item "><img src="[base_url]theme/theme/files/uploads/about-team-5.jpg" alt="SEO IMG NAME"></div>
        	<div class="isotope-item wide tall"><img src="[base_url]theme/theme/files/uploads/about-team-3.jpg" alt="SEO IMG NAME"></div>
        	<div class="isotope-item wide tall"><img src="[base_url]theme/theme/files/uploads/about-team-2.jpg" alt="SEO IMG NAME"></div>            
            <div class="isotope-item "><img src="[base_url]theme/theme/files/uploads/about-team-44.jpg" alt="SEO IMG NAME"></div>
            <div class="isotope-item "><img src="[base_url]theme/theme/files/uploads/about-team-55.jpg" alt="SEO IMG NAME"></div>
        </div> <!-- END .isotope-grid -->
        

        <div class="fullwidth-section text-light parallax-section" data-parallax-image="[base_url]theme/theme/files/uploads/hero-about-31.jpg">
        	<div class="fullwidth-content">
            
                <div class="wrapper-small align-center">
                    <!-- <h5 class="subtitle-2">Nuestros clientes</h5> -->
                    <hr class="small fat colored">
                    <h2><strong>Estudiantes, Estada Lingüísticas, Eventos Deportivos, Turismo On-Line, Receptivos, Aéreos, Business Travel, Continental</strong></h2>
                </div>

            </div>
        </div> <!-- END .fullwidth-section -->
        <div>[contacto]</div>
        <div class="spacer-big"></div>
        <div>[footer]</div>
 	</section>
	<!-- PAGEBODY -->