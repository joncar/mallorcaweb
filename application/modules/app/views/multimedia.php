<?= $output ?>
<script>
	var videos = $("#video_field_box");
	var fotos = $("#foto_field_box");
	$(document).on('change','#field-tipo',function(){
		switch($(this).val()){
			case '1':videos.hide(); fotos.show(); break;
			case '2':videos.show(); fotos.hide(); break;
			default:videos.hide(); fotos.hide(); break;
		}
	});	
	$("#field-tipo").trigger('change');
</script>