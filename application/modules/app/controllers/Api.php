<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Api extends Main{        
        function __construct() {
            parent::__construct();            
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $this->load->model('elements_app');
             if (isset($_SERVER['HTTP_ORIGIN'])) {
		        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
		        header('Access-Control-Allow-Credentials: true');
		        header('Access-Control-Max-Age: 86400');    // cache for 1 day
		    }
		 
		    // Access-Control headers are received during OPTIONS requests
		    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
		 
		        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])){
		            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
		        }
		 
		        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])){
		            header("Access-Control-Allow-Headers:{$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
		        }
		 
		        exit(0);
		    }

		    $postdata = file_get_contents("php://input");
		    if (isset($postdata)) {
		    	$_POST = (array)json_decode($postdata);
		    }else{
		    	$_POST = array();
		    }

            date_default_timezone_set('Europe/Madrid');
            setlocale(LC_ALL,'Spanish');   
        }

        function printjson($object){
            header('Content-Type: application/json');
            echo json_encode($object);            
        }  

        function login(){
            $response = array('success'=>false);
            if(!empty($_POST)){
                if(!empty($_POST['fecha']) && !empty($_POST['telefono']) && !empty($_POST['password'])){
                    $usuario = $this->db->get_where('app_usuarios',array('telefono'=>$_POST['telefono'],'password'=>md5($_POST['password'])));
                    if($usuario->num_rows()==0){
                        $response['msj'] = 'Usuario o contraseña incorrecta';        
                    }else{
                        $response['success'] = true;
                        $response['data']['id'] = $usuario->row()->id;
                        $response['data']['nombre'] = $usuario->row()->nombre;
                        $response['data']['fecha'] = $usuario->row()->app_fechas_salida_id;
                        $response['data']['numero_grupo'] = $usuario->row()->numero_grupo;
                    }
                }else{
                    $response['msj'] = 'Usuario o contraseña incorrecta';    
                }
            }else{
                $response['msj'] = 'Debe indicar los datos para realizar su conexión';
            }

            echo json_encode($response);
        }

         function recover(){
            $response = array('success'=>false);
            if(!empty($_POST)){
                if(!empty($_POST['email'])){
                    $usuario = $this->db->get_where('app_usuarios',array('email'=>$_POST['email']));
                    if($usuario->num_rows()==0){
                        $response['msj'] = 'El email ingresado no existe';
                    }else{
                        $response['success'] = true;
                        $response['msj'] = 'Los pasos para restauración han sido enviados a su Email';
                        $post = $usuario->row();
                        $post->link = base_url('registro/recuperar_app/'.base64_encode($post->email));
                        $this->enviarcorreo($post,15);
                    }
                }else{
                    $response['msj'] = 'Debe indicar los datos para realizar su conexión';
                }
            }else{
                $response['msj'] = 'Debe indicar los datos para realizar su conexión';
            }

            echo json_encode($response);
        }

        function desactivarAviso($user,$proid){
            $response = false;
            $prod = $this->db->get_where('app_programacion',array('id'=>$proid));
            if(is_numeric($user) && $user>0 && $prod->num_rows()>0){
                $response = true;
                $esta = $this->db->get_where('app_programacion_no_notif',array('app_usuarios_id'=>$user,'app_programacion_id'=>$proid));
                if($esta->num_rows()>0){
                    $this->db->delete('app_programacion_no_notif',array('app_usuarios_id'=>$user,'app_programacion_id'=>$proid));
                }else{
                    $this->db->insert('app_programacion_no_notif',array('app_usuarios_id'=>$user,'app_programacion_id'=>$proid));
                }
            }
            echo $response;
        }

        function programa(){
            $programa = array();
            //$_POST['fecha'] = 2;
            //$_POST['user'] = 6;
            if(!empty($_POST['fecha']) && !empty($_POST['user'])){
                $dia = $_POST['fecha'];
                if(is_numeric($dia)){
                    $this->db->order_by('app_programacion.fecha','ASC');
                    $this->db->select('app_actividades.*,app_programacion.app_lineup_id, app_programacion.duracion,app_programacion.id as proid,app_programacion.fecha as _fecha,DATE(app_programacion.fecha) as fecha,TIME(app_programacion.fecha) as hora');
                    $this->db->join('app_actividades','app_actividades.id = app_programacion.app_actividades_id');
                    $fecha = $this->db->get_where('app_programacion',array('app_fechas_salida_id'=>$dia));
                    if($fecha->num_rows()>0){
                        $d = array();
                        foreach($fecha->result() as $n=>$f){
                            $f->foto = base_url('img/app/'.$f->foto);
                            $f->miniatura = base_url('img/app/'.$f->miniatura);
                            $f->foto_horizontal = base_url('img/app/'.$f->foto_horizontal);
                            $f->mes = strftime('%b',strtotime($f->fecha));
                            $f->hora = date('H:i',strtotime($f->_fecha));
                            $f->hasta = date('H:i',strtotime($f->_fecha.' +'.$f->duracion.' hours'));
                            $f->avisame = $this->db->get_where('app_programacion_no_notif',array('app_usuarios_id'=>$_POST['user'],'app_programacion_id'=>$f->proid))->num_rows()>0?0:1;
                            if(!empty($f->app_lineup_id)){
                                $dj = $this->db->get_where('app_lineup',array('id'=>$f->app_lineup_id));
                                if($dj->num_rows()>0){
                                    $f->dj = $dj->row();
                                }
                            }
                            $diaw = date("w",strtotime($f->fecha));
                            $d[$diaw][] = $f;
                        }
                        $programa = array('fecha'=>$this->db->get_where('app_fechas_salida',array('id'=>$dia))->row()->fecha,'programa'=>$d);
                    }
                }
            }
            $this->printjson($programa);
        } 

        function programacion(){
            $programa = array();
            if(!empty($_POST['fecha']) && !empty($_POST['user'])){
                $dia = $_POST['fecha'];
                if(is_numeric($dia)){
                    $fecha = $this->elements_app->app_programacion(array('app_fechas_salida_id'=>$dia));                    
                    if($fecha->num_rows()>0){
                        foreach($fecha->result() as $f){                           
                            $programa[$f->fecha][] = $f;
                        }                        
                    }
                }
            }
            header('Content-Type: application/json');
            echo json_encode($programa);
        } 

        function programa_sin_leer(){
            //$_POST['user_id'] = 6;
            if(!empty($_POST['user_id'])){
                $this->db->where('app_push_programacion.leido',0);
                $consejos = $this->elements_app->app_push_programacion(array('user'=>$_POST['user_id']));
                $con = array($consejos->num_rows());
                $this->printjson($con);
            }
        }

        function leer_programa(){
            //$_POST['user'] = 6;
            if(!empty($_POST['id'])){
                $this->db->update('app_push_programacion',array('leido'=>1),array('id'=>$_POST['id']));
            }
        }

        function fechas(){
        	
        	$crud = new ajax_grocery_crud();
        	$crud->set_table('app_fechas_salida');
        	$crud->set_subject('bootstrap2');   
        	$crud->set_theme('bootstrap2');     	
        	$crud->required_fields_array();
        	$crud->unset_add()
        		 ->unset_edit()
        		 ->unset_delete()
        		 ->unset_print()
        		 ->unset_export();
            $crud->where('NOW() < DATE_ADD(fecha, INTERVAL 7 day)','ESCAPE',NULL);
        	$crud->columns('id','fecha');
        	$crud->callback_column('fecha',function($val,$row){
        		return ucwords(strftime('%d %B %Y',strtotime(str_replace('/','-',$val))));
        	});
        	$crud->is_json();        	
        	$crud = $crud->render();
        	echo json_encode($crud);
        }     

        function addGCM($action,$id){
            if(is_numeric($id)){
                $this->db->update('app_usuarios',array('gcm'=>$_POST['gcm']),array('id'=>$id));
                echo json_encode(array('success'=>true));
            }
        }

        function app_usuarios(){
        	if(!empty($_POST['telefono'])){
        		$_POST['telefono'] = str_replace(' ','',$_POST['telefono']);
        	}
        	$crud = new ajax_grocery_crud();
        	$crud->set_table('app_usuarios');
        	$crud->set_subject('bootstrap2');
        	$crud->set_theme('bootstrap2');
        	$crud->required_fields_array();
            $crud->callback_column('s3a5cb5d6',function($val,$row){
                return $row->app_fechas_salida_id;
            });
            if(!empty($_POST['user'])){
                $crud->where('app_usuarios.id',$_POST['user']);
            }else{
                $crud->where('app_usuarios.id',-1);
            }
            $crud->unset_delete()->unset_read()->unset_print()->unset_export();
        	$crud->is_json();        	

        	if($crud->getParameters(FALSE)=='insert_validation'){
                $crud->set_rules('telefono','Telefono','required|numeric|is_unique[app_usuarios.telefono]');
                $crud->set_rules('email','Email','required|valid_email|is_unique[app_usuarios.email]');
        		if(!empty($_POST['password']) && !empty($_POST['password2']) && $_POST['password'] != $_POST['password2']){
        			echo json_encode(array('success'=>false,'error_message'=>'Las contraseñas deben ser iguales'));
        			exit();
        		}
        	}
            if($crud->getParameters()=='edit'){
                $crud->fields('nombre','email','numero_grupo','instituto','localidad','password','app_fechas_salida_id');
            }
        	$crud->callback_before_insert(function($post){
        		$post['password'] = md5($post['password']);
        		return $post;
        	});

            $crud->callback_before_update(function($post,$primary){
                if(!empty($primary) && $this->db->get_where('app_usuarios',array('id'=>$primary))->row()->password!=$post['password'] || empty($primary)){
                    $post['password'] = md5($post['password']);                
                }
                return $post;
            });
        	$crud = $crud->render();
        	echo json_encode($crud);
        }

        function actividades(){            
            $influencers = $this->elements_app->app_actividad(array('mostrar_en_actividades'=>1));
            $result = array();
            foreach($influencers->result() as $r){
                $result[] = $r;
            }
            $this->printjson($result);
        }

        function lineup(){
            $crud = $this->elements_app->app_lineup()->result_object;
            $this->printjson($crud);
        }

        function params($name = ''){
            $param = $this->db->get_where('app_texts',array('nombre'=>$name));
            if($param->num_rows()>0){
                $this->printjson($param->row());
            }else{
                $this->printjson(array());
            }
        }

        function influencers(){            
            $influencers = $this->elements_app->app_influencers();
            $result = array();
            foreach($influencers->result() as $r){
                $result[] = $r;
            }
            $this->printjson($result);
        }

        function faqs(){            
            $crud = new ajax_grocery_crud();
            $crud->set_table('app_faqs');
            $crud->set_subject('bootstrap2');
            $crud->set_theme('bootstrap2');  
            $crud->is_json();            
            $crud->unset_add()->unset_edit()->unset_delete()->unset_print()->unset_export()->unset_read();
            $crud = $crud->render();
            echo json_encode($crud);
        }

        function consejos_sin_leer(){
            //$_POST['user'] = 6;
            if(!empty($_POST['user_id'])){
                $this->db->where('app_push.leido',0);
                $consejos = $this->elements_app->app_push(array('user'=>$_POST['user_id']));
                $con = array($consejos->num_rows());
                $this->printjson($con);
            }
        }

        function consejos(){  
            //$_POST['user'] = 6;
            if(!empty($_POST['user'])){
                $consejos = $this->elements_app->app_push(array('user'=>$_POST['user']));
                $this->db->update('app_push',array('leido'=>1),array('app_usuarios_id'=>$_POST['user']));
                $con = array();
                foreach($consejos->result() as $c){
                    $con[] = $c;
                }
                $this->printjson(json_encode($con));
            }
        }        

        function tienda(){
            $products = array();
            $mer = $this->load->database('merchadisign',TRUE);
            $mer->select('id,foto,precio,producto_nombre as titulo,texto_corto as descripcion');
            $pr = $mer->get_where('productos',array('disponible'=>1));
            if($pr->num_rows()>0){
                foreach($pr->result() as $p){
                    $mer->order_by('priority','ASC');
                    $p->foto = base_url('tienda/images/productos/'.$mer->get_where('fotos',array('productos_id'=>$p->id))->row()->foto);
                    $p->precio = moneda($p->precio);
                    $products[] = $p;
                }
            }
            echo json_encode($products);
            
        }

        
        

        function redes(){
            $redes = $this->elements_app->redes();            
            $this->printjson($redes);
        }

        function instagram(){
            if(empty($_SESSION['instagram'])){
                $_SESSION['instagram'] = file_get_contents('https://api.instagram.com/v1/users/self/media/recent?access_token=1736150833.cb78ff6.e7f17a31a5a34361997d3d7a81531583');
            }            
            $redes = array();
            $instagram = $_SESSION['instagram'];            
            $instagram = json_decode($instagram);
            $instagram = $instagram->data;
            foreach($instagram as $d){
                $r = array();
                $r['id'] = $d->id;
                $r['texto'] = $d->caption->text;
                $r['foto'] = $d->images->thumbnail->url;
                $r['link'] = $d->link;
                $r['tipo'] = 'instagram';
                $r['fecha'] = date("d-m-Y H:i",strtotime($d->created_time));
                $r['_fecha'] = $d->created_time;
                $r['color'] = 'bg-blue-dark';
                $redes[] = $r;
            }

            $this->printjson($redes);
        }

        function destacados(){
            //$_POST['fecha'] = '2019-06-14';
            //$_POST['salida'] = 2;
            if(!empty($_POST['fecha']) && !empty($_POST['salida'])){
                $dest = array(
                    'hoy'=>array(),
                    'semana'=>array(),                    
                    'redes'=>array(),
                    'notelopierdas'=>array()                   
                );
                //Diaria
                $res = $this->elements_app->app_programacion(array('app_fechas_salida_id'=>$_POST['salida'],'DATE(fecha)'=>$_POST['fecha']),3);
                foreach($res->result() as $r){                    
                    if(!empty($r->app_lineup_id)){
                        $r->dj = $this->elements_app->app_lineup(array('app_lineup.id'=>$r->app_lineup_id))->row();
                    }
                    $dest['hoy'][] = $r;                    
                }
                //Semanal
                $res = $this->elements_app->app_programacion(array('app_fechas_salida_id'=>$_POST['salida'],'destacado'=>1));
                foreach($res->result() as $r){                    
                    $dest['semana'][] = $r;
                }

                //No te lo pierdas
                $res = $this->elements_app->app_programacion(array('app_fechas_salida_id'=>$_POST['salida'],'no_te_lo_pierdas'=>1));
                foreach($res->result() as $r){                    
                    $dest['notelopierdas'][] = $r;
                }

                //redes
                $dest['redes'] = $this->elements_app->redes(3);
                $this->printjson($dest);
            }   
        }

        function multimedia(){
            $redes = $this->elements_app->app_multimedia();  
            $vid = array();
            foreach($redes->result() as $r){
                $vv = array();
                foreach($r->fotos->result() as $f){
                    $vv[] = $f;
                }
                $r->fotos = $vv;
                $vid[] = $r;
            }          
            $redes = $vid;
            $this->printjson($redes);
        }


        //Notificaciones y recordatorios
        function consejos_cron(){            
            $this->db->select('app_consejos.*,app_usuarios.gcm,app_usuarios.id as userid');
            $this->db->join('app_consejos','app_consejos.id = app_consejos_salidas.app_consejos_id');
            $this->db->join('app_usuarios','app_usuarios.app_fechas_salida_id = app_consejos_salidas.app_fechas_salida_id');
            $this->db->where("DATE_FORMAT(app_consejos.fecha_envio,'%Y-%m-%d %H:%i') <= '".date("Y-m-d H:i")."'",NULL,FALSE);
            $consejos = $this->db->get_where('app_consejos_salidas');
            foreach($consejos->result() as $c){
                if($this->db->get_where('app_push',array('app_consejos_id'=>$c->id,'app_usuarios_id'=>$c->userid))->num_rows()==0){
                    //Insertamos para que no lo vuelva a enviar
                    $this->db->insert('app_push',array('app_consejos_id'=>$c->id,'app_usuarios_id'=>$c->userid,'fecha'=>date("Y-m-d H:i:s")));
                    $this->elements_app->sendPush(array($c->gcm),array('message'=>$c->descripcion));
                }
            }

            //Recordatorios
            $this->db->select('app_actividades.*,app_programacion.fecha, app_usuarios.id as app_usuarios_id, app_programacion.id as app_programacion_id, app_usuarios.gcm');
            $this->db->join('app_fechas_salida','app_programacion.app_fechas_salida_id = app_fechas_salida.id');
            $this->db->join('app_usuarios','app_usuarios.app_fechas_salida_id = app_fechas_salida.id');
            $this->db->join('app_actividades','app_actividades.id = app_programacion.app_actividades_id');
            $this->db->where("DATE_FORMAT(app_programacion.notificar,'%Y-%m-%d %H:%i') <= '".date("Y-m-d H:i")."' AND 
                            CONCAT(app_usuarios.id,',',app_programacion.id) NOT IN (SELECT CONCAT(app_usuarios_id,',',app_programacion_id) FROM app_push_programacion) AND 
                            CONCAT(app_programacion.id,',',app_usuarios.id) NOT IN (SELECT CONCAT(app_programacion_id,',',app_usuarios_id) FROM app_programacion_no_notif)",NULL,FALSE);
            $recordatorios = $this->db->get_where('app_programacion');
            foreach($recordatorios->result() as $r){
                //Insertamos para que no lo vuelva a enviar
                $this->db->insert('app_push_programacion',array('app_usuarios_id'=>$r->app_usuarios_id,'app_programacion_id'=>$r->app_programacion_id));
                $rec = $r->nombre.' '.date("d-m-Y H:i",strtotime($r->fecha));
                $this->elements_app->sendPush(array($r->gcm),array('message'=>$rec));
            }            
        }
        function sendTestPush(){
            $this->elements_app->sendPush(array('f2c67334-85f7-4a61-b557-bb0c93076f54'),array('message'=>'Mensaje de prueba'));
        }
    }