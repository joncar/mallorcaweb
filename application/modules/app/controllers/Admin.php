<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function app_fechas_salida(){
        	$crud = $this->crud_function('',''); 
        	$crud->set_subject('Fecha');           
        	$crud->add_action('Programación','',base_url('app/admin/app_programacion').'/');
            $crud = $crud->render();
            $crud->title = 'Fechas de salida';
            $this->loadView($crud);
        }

        public function app_usuarios(){
        	$crud = $this->crud_function('',''); 
        	$crud->set_subject('Usuario');           
        	$crud->display_as('app_fechas_salida_id','Salida');
            $crud = $crud->render();
            $crud->title = 'Usuarios registrados en la app';
            $this->loadView($crud);
        }

        public function app_consejos(){
        	$crud = $this->crud_function('',''); 
        	$crud->set_subject('Consejo');        	
        	$crud->display_as('app_fechas_salida_id','Salida')
        		 ->display_as('duracion','Hrs. de duración');
        	$crud->set_relation_n_n('Salidas','app_consejos_salidas','app_fechas_salida','app_consejos_id','app_fechas_salida_id','fecha',null,array('status'=>1));
            $crud->callback_after_delete(function($primary){
                $db = get_instance()->db;
                $db->delete('app_consejos_salidas',array('app_consejos_id'=>$primary));
                $db->delete('app_push',array('app_consejos_id'=>$primary));
            });
            $crud->set_clone();
            $crud = $crud->render();      
            $crud->title = 'Consejos';      
            $this->loadView($crud);
        }

        function sendPush(){
            $this->load->model('elements_app');
            $this->elements_app->sendPush(array('fe5ae604-fd8a-485d-9bc6-573051c1302a'),array('message'=>'Mensaje de prueba'));
        }

        function app_texts(){
            $crud = $this->crud_function('','');             
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function app_texts_clientes(){
            $crud = $this->crud_function('','');             
            $crud = $crud->render();
            $this->loadView($crud);
        }

        public function app_actividades(){
        	$crud = $this->crud_function('',''); 
            $crud->columns('foto','nombre');  
        	$crud->field_type('foto','image',array('path'=>'img/app','width'=>'700px','height'=>'263px'))
                 ->field_type('foto_horizontal','image',array('path'=>'img/app','width'=>'400px','height'=>'266px'))
                 ->field_type('miniatura','image',array('path'=>'img/app','width'=>'160px','height'=>'160px'))
        		 ->field_type('lugar','map',array('width'=>'400px','height'=>'400px','lat'=>'39.5707844','lon'=>'2.6520217'))
                 ->field_type('mapa','image',array('path'=>'img/app','width'=>'430px','height'=>'430px'))
                 ->field_type('fotos','gallery',array('path'=>'img/app','width'=>'700px','height'=>'700px'));
        	$crud->set_subject('Actividad')
                 ->set_clone();
            $crud = $crud->render();      
            $crud->title = 'Actividades';      
            $this->loadView($crud);
        }

        public function app_lineup(){
            $crud = $this->crud_function('','');   
            $crud->field_type('foto','image',array('path'=>'img/app','width'=>'600px','height'=>'880px'))
                 ->field_type('miniatura','image',array('path'=>'img/app','width'=>'700px','height'=>'263px'))
                 ->field_type('fotos','gallery',array('path'=>'img/app','width'=>'700px','height'=>'700px'))
                 ->set_order('orden')
                 ->columns('foto','miniatura','nombre','seudonimo');
            $crud->set_subject('LineUp')
                 ->set_clone();
            $crud = $crud->render();      
            $crud->title = 'LineUp';      
            $this->loadView($crud);
        }

        public function app_influencers(){
            $crud = $this->crud_function('','');   
            $crud->field_type('foto','image',array('path'=>'img/app','width'=>'600px','height'=>'880px'))
                 ->field_type('miniatura','image',array('path'=>'img/app','width'=>'160px','height'=>'160px'))
                 ->field_type('fotos','gallery',array('path'=>'img/app','width'=>'700px','height'=>'700px'))
                 ->set_order('orden');
            $crud->set_subject('Influencers')
                 ->set_clone();
            $crud = $crud->render();      
            $crud->title = 'Influencers';      
            $this->loadView($crud);
        }

        public function app_programacion($id){
        	$crud = $this->crud_function('','');    
        	$crud->where('app_fechas_salida_id',$id)
        		 ->field_type('app_fechas_salida_id','hidden',$id)
        		 ->display_as('duracion','Hrs. Duración')
                 ->display_as('app_lineup_id','DJ')
                 ->field_type('color_pulsera','dropdown',array(
                     '1'=>'Sin color','2'=>'Amarillo','3'=>'Verde','4'=>'Azul','5'=>'Rojo','6'=>'Negro' 
                ))
        		 ->unset_columns('app_fechas_salida_id');       	
        	$crud->set_subject('Programación');
            $crud = $crud->render();      
            $crud->header = new ajax_grocery_crud();
            $crud->header->set_table('app_fechas_salida')
            		 	 ->set_subject('Salidas')
            		 	 ->set_theme('header_data')
            		 	 ->where('id',$id);
            $crud->header = $crud->header->render(1)->output;
            $crud->title = 'Programación';      
            $this->loadView($crud);
        }

        public function app_faqs(){
            $crud = $this->crud_function('','');             
            $crud->set_subject('FAQ');
            $crud->set_order('orden');
            $crud = $crud->render();
            $crud->title = 'FAQS';
            $this->loadView($crud);
        }

        public function app_multimedia(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Multimedia');
            $crud->field_type('portada','image',array('path'=>'img/app','width'=>'160px','height'=>'160px'));
            $crud = $crud->render();
            $crud->title = 'Multimedia';
            $this->loadView($crud);
        }

        public function app_multimedia_files(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Multimedia');
            $crud->field_type('foto','image',array('path'=>'img/app','width'=>'480px','height'=>'480px'))
                 ->field_type('tipo','dropdown',array('1'=>'Foto','2'=>'Video'))
                 ->set_field_upload('video','img/app')
                 ->set_relation('app_multimedia_id','app_multimedia','nombre');
            $crud = $crud->render();
            $crud->output = $this->load->view('multimedia',array('output'=>$crud->output),TRUE);
            $crud->title = 'Multimedia';
            $this->loadView($crud);
        }

        public function app_concurso_videos(){
            $crud = $this->crud_function('','');            
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
