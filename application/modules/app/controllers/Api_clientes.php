<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Api_clientes extends Main{ 
        const IDROLUSER = 2;       
        function __construct() {
            parent::__construct();            
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $this->load->model('elements_app');
            $this->load->model('elements_app_clientes');
             if (isset($_SERVER['HTTP_ORIGIN'])) {
		        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
		        header('Access-Control-Allow-Credentials: true');
		        header('Access-Control-Max-Age: 86400');    // cache for 1 day
		    }
		 
		    // Access-Control headers are received during OPTIONS requests
		    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
		 
		        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])){
		            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
		        }
		 
		        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])){
		            header("Access-Control-Allow-Headers:{$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
		        }
		 
		        exit(0);
		    }

		    $postdata = file_get_contents("php://input");
		    if (isset($postdata)) {
		    	$_POST = (array)json_decode($postdata);
		    }else{
		    	$_POST = array();
		    }

            date_default_timezone_set('Europe/Madrid');
            setlocale(LC_ALL,'Spanish');   
        }

        function printjson($object){
            header('Content-Type: application/json');
            echo json_encode($object);            
        }  

        function login(){
            $response = array('success'=>false);            
            if(!empty($_POST)){
                if(!empty($_POST['email']) && !empty($_POST['password'])){
                    $usuario = $this->db->get_where('user',array('email'=>$_POST['email'],'password'=>md5($_POST['password']),'status'=>'1'));
                    if($usuario->num_rows()==0){
                        $response['msj'] = 'Usuario o contraseña incorrecta';        
                    }else{
                        $response['success'] = true;
                        $response['data']['id'] = $usuario->row()->id;
                        $response['data']['nombre'] = $usuario->row()->nombre;
                    }
                }else{
                    $response['msj'] = 'Usuario o contraseña incorrecta';    
                }
            }else{
                $response['msj'] = 'Debe indicar los datos para realizar su conexión';
            }

            echo json_encode($response);
        }

         function recover(){
            $response = array('success'=>false);
            if(!empty($_POST)){
                if(!empty($_POST['email'])){
                    $usuario = $this->db->get_where('user',array('email'=>$_POST['email']));
                    if($usuario->num_rows()==0){
                        $response['msj'] = 'El email ingresado no existe';
                    }else{
                        $response['success'] = true;
                        $response['msj'] = 'Los pasos para restauración han sido enviados a su Email';
                        $post = $usuario->row();
                        $post->link = base_url('registro/recuperar_app_cliente/'.base64_encode($post->email));
                        $this->enviarcorreo($post,15);
                    }
                }else{
                    $response['msj'] = 'Debe indicar los datos para realizar su conexión';
                }
            }else{
                $response['msj'] = 'Debe indicar los datos para realizar su conexión';
            }

            echo json_encode($response);
        }

        function desactivarAviso($user,$proid){
            $response = false;
            $prod = $this->db->get_where('app_programacion',array('id'=>$proid));
            if(is_numeric($user) && $user>0 && $prod->num_rows()>0){
                $response = true;
                $esta = $this->db->get_where('app_programacion_no_notif',array('app_usuarios_id'=>$user,'app_programacion_id'=>$proid));
                if($esta->num_rows()>0){
                    $this->db->delete('app_programacion_no_notif',array('app_usuarios_id'=>$user,'app_programacion_id'=>$proid));
                }else{
                    $this->db->insert('app_programacion_no_notif',array('app_usuarios_id'=>$user,'app_programacion_id'=>$proid));
                }
            }
            echo $response;
        }

        function programa(){
            $programa = array();
            //$_POST['fecha'] = 2;
            //$_POST['user'] = 6;
            if(!empty($_POST['fecha']) && !empty($_POST['user'])){
                $dia = $_POST['fecha'];
                if(is_numeric($dia)){
                    $this->db->order_by('app_programacion.fecha','ASC');
                    $this->db->select('app_actividades.*,app_programacion.app_lineup_id, app_programacion.duracion,app_programacion.id as proid,app_programacion.fecha as _fecha,DATE(app_programacion.fecha) as fecha,TIME(app_programacion.fecha) as hora');
                    $this->db->join('app_actividades','app_actividades.id = app_programacion.app_actividades_id');
                    $fecha = $this->db->get_where('app_programacion',array('app_fechas_salida_id'=>$dia));
                    if($fecha->num_rows()>0){
                        $d = array();
                        foreach($fecha->result() as $n=>$f){
                            $f->foto = base_url('img/app/'.$f->foto);
                            $f->miniatura = base_url('img/app/'.$f->miniatura);
                            $f->foto_horizontal = base_url('img/app/'.$f->foto_horizontal);
                            $f->mes = strftime('%b',strtotime($f->fecha));
                            $f->hora = date('H:i',strtotime($f->_fecha));
                            $f->hasta = date('H:i',strtotime($f->_fecha.' +'.$f->duracion.' hours'));
                            $f->avisame = $this->db->get_where('app_programacion_no_notif',array('app_usuarios_id'=>$_POST['user'],'app_programacion_id'=>$f->proid))->num_rows()>0?0:1;
                            if(!empty($f->app_lineup_id)){
                                $dj = $this->db->get_where('app_lineup',array('id'=>$f->app_lineup_id));
                                if($dj->num_rows()>0){
                                    $f->dj = $dj->row();
                                }
                            }
                            $diaw = date("w",strtotime($f->fecha));
                            $d[$diaw][] = $f;
                        }
                        $programa = array('fecha'=>$this->db->get_where('app_fechas_salida',array('id'=>$dia))->row()->fecha,'programa'=>$d);
                    }
                }
            }
            $this->printjson($programa);
        } 

        function programacion(){
            $programa = array();
            if(!empty($_POST['fecha']) && !empty($_POST['user'])){
                $dia = $_POST['fecha'];
                if(is_numeric($dia)){
                    $fecha = $this->elements_app->app_programacion(array('app_fechas_salida_id'=>$dia));                    
                    if($fecha->num_rows()>0){
                        foreach($fecha->result() as $f){                           
                            $programa[$f->fecha][] = $f;
                        }                        
                    }
                }
            }
            header('Content-Type: application/json');
            echo json_encode($programa);
        } 

        function programa_sin_leer(){
            //$_POST['user_id'] = 6;
            if(!empty($_POST['user_id'])){
                $this->db->where('app_push_programacion.leido',0);
                $consejos = $this->elements_app->app_push_programacion(array('user'=>$_POST['user_id']));
                $con = array($consejos->num_rows());
                $this->printjson($con);
            }
        }

        function leer_programa(){
            //$_POST['user'] = 6;
            if(!empty($_POST['id'])){
                $this->db->update('app_push_programacion',array('leido'=>1),array('id'=>$_POST['id']));
            }
        }

        function fechas(){
        	
        	$crud = new ajax_grocery_crud();
        	$crud->set_table('app_fechas_salida');
        	$crud->set_subject('bootstrap2');   
        	$crud->set_theme('bootstrap2');     	
        	$crud->required_fields_array();
        	$crud->unset_add()
        		 ->unset_edit()
        		 ->unset_delete()
        		 ->unset_print()
        		 ->unset_export();
            $crud->where('NOW() < DATE_ADD(fecha, INTERVAL 7 day)','ESCAPE',NULL);
        	$crud->columns('id','fecha');
        	$crud->callback_column('fecha',function($val,$row){
        		return ucwords(strftime('%d %B %Y',strtotime(str_replace('/','-',$val))));
        	});
        	$crud->is_json();        	
        	$crud = $crud->render();
        	echo json_encode($crud);
        }     

        function addGCM($action,$id){
            if(is_numeric($id)){
                $this->db->update('app_usuarios',array('gcm'=>$_POST['gcm']),array('id'=>$id));
                echo json_encode(array('success'=>true));
            }
        }

        function app_usuarios(){        	
        	$crud = new ajax_grocery_crud();
        	$crud->set_table('user');
        	$crud->set_subject('bootstrap2');
        	$crud->set_theme('bootstrap2');
        	$crud->required_fields_array();  
            $crud->display_as('password','Contraseña nuevo usuario')
                 ->display_as('apellido_paterno','1º Apellido')
                 ->display_as('apellido_2','2º Apellido')
                 ->display_as('nombre','Nombre del pasajero')
                 ->display_as('email','Email de contacto')
                 ->display_as('provincias_id','Provincia')
                 ->display_as('referencia_grupo','Código del grupo')
                 ->display_as('telefono','Teléfono')
                 ->display_as('codigo_postal','Código Postal')
                 ->display_as('direccion','Dirección')
                 ->display_as('fecha_caducidad','Fecha de caducidad del DNI')
                 ->display_as('dni','DNI')
                 ->display_as('telefono','Teléfono Estudiante')
                 ->display_as('telefono_padre','Teléfono Padre/Madre/Tutor legal')
                 ->display_as('dni','DNI');    
            if(!empty($_POST['user'])){
                $crud->where('user.id',$_POST['user']);
            }else{
                $crud->where('user.id',-1);
            }
            $crud->unset_delete()->unset_read()->unset_print()->unset_export();
        	$crud->is_json();        	

        	if($crud->getParameters(FALSE)=='insert_validation'){                
                $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]');
        		if(!empty($_POST['password']) && !empty($_POST['password2']) && $_POST['password'] != $_POST['password2']){
        			echo json_encode(array('success'=>false,'error_message'=>'Las contraseñas deben ser iguales'));
        			exit();
        		}
        	}
            if($crud->getParameters()=='edit'){
                $crud->fields('nombre','apellido_paterno','apellido_materno','apellido_2','dni','email','telefono','telefono_padre','referencia_grupo');
                $crud->set_rules('referencia_grupo','Referencia de grupo','required|callback_validarReferenciaGrupo');
            }
        	$crud->callback_before_insert(function($post){
        		$post['password'] = md5($post['password']);
                $post['admin'] = 0;
                $post['status'] = 1;
                $post['fecha_registro'] = date("Y-m-d H:i:s");   
                $post['registrado_desde'] = 'APP';
        		return $post;
        	});

            $crud->callback_before_update(function($post,$primary){
                if(!empty($primary) && $this->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'] || empty($primary)){
                    $post['password'] = md5($post['password']);                
                }
                return $post;
            });

            $crud->callback_after_insert(function($post,$primary){
                get_instance()->db->insert('user_group',array('user'=>$primary,'grupo'=>self::IDROLUSER));
                get_instance()->enviarcorreo((object)$post,1);
            });
        	$crud = $crud->render();
        	echo json_encode($crud);
        }

        function validarReferenciaGrupo(){
            if(empty($_POST)){
                $this->form_validation->set_message('validarReferenciaGrupo','Debe enviar los datos solicitados');
                return false;
            }
            if(!isset($_POST['referencia_grupo'])){
                $this->form_validation->set_message('validarReferenciaGrupo','Debe enviar los datos solicitados');   
                return false;
            }
            if($_POST['referencia_grupo']=='0'){
                $this->form_validation->set_message('validarReferenciaGrupo','Debe indicar un numero de grupo válido');
                return false;
            }            
            return true;
        }

        function actividades(){            
            $influencers = $this->elements_app->app_actividad(array('mostrar_en_actividades'=>1));
            $result = array();
            foreach($influencers->result() as $r){
                $result[] = $r;
            }
            $this->printjson($result);
        }

        function lineup(){
            $crud = $this->elements_app->app_lineup()->result_object;
            $this->printjson($crud);
        }

        function concurso(){
            $_POST['user'] = empty($_POST['user'])?'':$_POST['user'];
            $crud = $this->elements_app_clientes->app_concurso_videos(array(),$_POST['user'])->result_object;
            $this->printjson($crud);
        }

        function concurso_votar(){
            $response = array('success'=>false);            
            $this->form_validation->set_rules('user','Usuario','required');
            $this->form_validation->set_rules('video','Video','required');
            if($this->form_validation->run()){
                $votos = $this->db->get_where('app_concurso_votos',array('user_id'=>$_POST['user']));
                if($votos->num_rows()<3){
                    $yavoto = $this->db->get_where('app_concurso_votos',array('user_id'=>$_POST['user'],'app_concurso_videos_id'=>$_POST['video']));
                    if($yavoto->num_rows()==0){
                        $cupos = 3-($votos->num_rows()+1);
                        if($cupos>0){
                            $response = array('success'=>true,'msj'=>'Has votado por un vídeo te quedan '.$cupos.' votos');
                        }else{
                            $response = array('success'=>true,'msj'=>'Ya has votado, gracias por participar');
                        }
                        $this->elements_app_clientes->votar($_POST['user'],$_POST['video']);
                    }else{
                        $response['msj'] = 'Ya has votado este vídeo';      
                    }

                }else{
                    $response['msj'] = 'Has excedido el limite máximo de votos';                    
                }
            }else{
                $response['msj'] = $this->form_validation->error_string();                
            }
            echo $this->printjson($response);
        }

        function params($name = ''){
            $param = $this->db->get_where('app_texts_clientes',array('nombre'=>$name));
            if($param->num_rows()>0){
                $this->printjson($param->row());
            }else{
                $this->printjson(array());
            }
        }

        function influencers(){            
            $influencers = $this->elements_app->app_influencers();
            $result = array();
            foreach($influencers->result() as $r){
                $result[] = $r;
            }
            $this->printjson($result);
        }

        function faqs(){            
            $crud = new ajax_grocery_crud();
            $crud->set_table('app_faqs');
            $crud->set_subject('bootstrap2');
            $crud->set_theme('bootstrap2');  
            $crud->is_json();            
            $crud->unset_add()->unset_edit()->unset_delete()->unset_print()->unset_export()->unset_read();
            $crud = $crud->render();
            echo json_encode($crud);
        }

        function consejos_sin_leer(){
            //$_POST['user'] = 6;
            if(!empty($_POST['user_id'])){
                $this->db->where('app_push.leido',0);
                $consejos = $this->elements_app->app_push(array('user'=>$_POST['user_id']));
                $con = array($consejos->num_rows());
                $this->printjson($con);
            }
        }

        function consejos(){  
            //$_POST['user'] = 6;
            if(!empty($_POST['user'])){
                $consejos = $this->elements_app->app_push(array('user'=>$_POST['user']));
                $this->db->update('app_push',array('leido'=>1),array('app_usuarios_id'=>$_POST['user']));
                $con = array();
                foreach($consejos->result() as $c){
                    $con[] = $c;
                }
                $this->printjson(json_encode($con));
            }
        }        

        function tienda(){
            $products = array();
            $mer = $this->load->database('merchadisign',TRUE);
            $mer->select('id,foto,precio,producto_nombre as titulo,texto_corto as descripcion');
            $pr = $mer->get_where('productos',array('disponible'=>1));
            if($pr->num_rows()>0){
                foreach($pr->result() as $p){
                    $mer->order_by('priority','ASC');
                    $p->foto = base_url('tienda/images/productos/'.$mer->get_where('fotos',array('productos_id'=>$p->id))->row()->foto);
                    $p->precio = moneda($p->precio);
                    $products[] = $p;
                }
            }
            echo json_encode($products);
            
        }

        
        

        function redes(){
            $redes = $this->elements_app->redes();            
            $this->printjson($redes);
        }

        function instagram(){
            if(empty($_SESSION['instagram'])){
                $_SESSION['instagram'] = file_get_contents('https://api.instagram.com/v1/users/self/media/recent?access_token=1736150833.cb78ff6.e7f17a31a5a34361997d3d7a81531583');
            }            
            $redes = array();
            $instagram = $_SESSION['instagram'];            
            $instagram = json_decode($instagram);
            $instagram = $instagram->data;
            foreach($instagram as $d){
                $r = array();
                $r['id'] = $d->id;
                $r['texto'] = $d->caption->text;
                $r['foto'] = $d->images->thumbnail->url;
                $r['link'] = $d->link;
                $r['tipo'] = 'instagram';
                $r['fecha'] = date("d-m-Y H:i",strtotime($d->created_time));
                $r['_fecha'] = $d->created_time;
                $r['color'] = 'bg-blue-dark';
                $redes[] = $r;
            }

            $this->printjson($redes);
        }

        function destacados(){
            //$_POST['fecha'] = '2019-06-14';
            //$_POST['salida'] = 2;
            if(!empty($_POST['fecha'])){
                $dest = array(              
                    'redes'=>array()
                );
                //redes
                $dest['redes'] = $this->elements_app->redes(3);
                $this->printjson($dest);
            }   
        }

        function multimedia(){
            $redes = $this->elements_app_clientes->app_multimedia();  
            $vid = array();
            foreach($redes->result() as $r){
                $vv = array();
                $vid[] = $r;
            }          
            $redes = $vid;
            $this->printjson($redes);
        }

        function productos(){
            $productos = array();
            if(!empty($_POST['cat'])){
                $productos = $this->elements_app_clientes->app_productos(array('categorias_id'=>$_POST['cat']));  
                $vid = array();
                foreach($productos->result() as $r){
                    $vv = array();
                    $vid[] = $r;
                }          
                $productos = $vid;
            }
            $this->printjson($productos);
        }


        //Notificaciones y recordatorios
        function consejos_cron(){            
            $this->db->select('app_consejos.*,app_usuarios.gcm,app_usuarios.id as userid');
            $this->db->join('app_consejos','app_consejos.id = app_consejos_salidas.app_consejos_id');
            $this->db->join('app_usuarios','app_usuarios.app_fechas_salida_id = app_consejos_salidas.app_fechas_salida_id');
            $this->db->where("DATE_FORMAT(app_consejos.fecha_envio,'%Y-%m-%d %H:%i') <= '".date("Y-m-d H:i")."'",NULL,FALSE);
            $consejos = $this->db->get_where('app_consejos_salidas');
            foreach($consejos->result() as $c){
                if($this->db->get_where('app_push',array('app_consejos_id'=>$c->id,'app_usuarios_id'=>$c->userid))->num_rows()==0){
                    //Insertamos para que no lo vuelva a enviar
                    $this->db->insert('app_push',array('app_consejos_id'=>$c->id,'app_usuarios_id'=>$c->userid,'fecha'=>date("Y-m-d H:i:s")));
                    $this->elements_app->sendPush(array($c->gcm),array('message'=>$c->descripcion));
                }
            }

            //Recordatorios
            $this->db->select('app_actividades.*,app_programacion.fecha, app_usuarios.id as app_usuarios_id, app_programacion.id as app_programacion_id, app_usuarios.gcm');
            $this->db->join('app_fechas_salida','app_programacion.app_fechas_salida_id = app_fechas_salida.id');
            $this->db->join('app_usuarios','app_usuarios.app_fechas_salida_id = app_fechas_salida.id');
            $this->db->join('app_actividades','app_actividades.id = app_programacion.app_actividades_id');
            $this->db->where("DATE_FORMAT(app_programacion.notificar,'%Y-%m-%d %H:%i') <= '".date("Y-m-d H:i")."' AND 
                            CONCAT(app_usuarios.id,',',app_programacion.id) NOT IN (SELECT CONCAT(app_usuarios_id,',',app_programacion_id) FROM app_push_programacion) AND 
                            CONCAT(app_programacion.id,',',app_usuarios.id) NOT IN (SELECT CONCAT(app_programacion_id,',',app_usuarios_id) FROM app_programacion_no_notif)",NULL,FALSE);
            $recordatorios = $this->db->get_where('app_programacion');
            foreach($recordatorios->result() as $r){
                //Insertamos para que no lo vuelva a enviar
                $this->db->insert('app_push_programacion',array('app_usuarios_id'=>$r->app_usuarios_id,'app_programacion_id'=>$r->app_programacion_id));
                $rec = $r->nombre.' '.date("d-m-Y H:i",strtotime($r->fecha));
                $this->elements_app->sendPush(array($r->gcm),array('message'=>$rec));
            }            
        }
        function sendTestPush(){
            $this->elements_app->sendPush(array('f2c67334-85f7-4a61-b557-bb0c93076f54'),array('message'=>'Mensaje de prueba'));
        }



        function app_carritoparams(){
            $response = array('success'=>false,'msj'=>'Acción post requerida');
            if(!empty($_POST)){
                $this->db->insert('app_carritoparams',array('params'=>$_POST['params'],'user'=>json_encode($_POST['user'])));
                $response = array('success'=>true,'msj'=>$this->db->insert_id());
            }
            $this->printjson($response);
        }
    }